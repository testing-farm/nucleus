import logging
import os
import random
import time
from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import Dict, List

from dynaconf import settings
from prometheus_client import Gauge, Histogram, start_http_server
from sqlalchemy.orm import Session

from tft.nucleus.api.core.database import Request, RequestStateType, Token, get_db

# defaults
DEFAULT_NUCLEUS_METRICS_PORT = 8000
DEFAULT_NUCLEUS_METRICS_TICK = 30
RANCHES = ['public', 'redhat']

# metrics
requests_total = Gauge('tf_requests_total', 'Total number of requests')
requests_user = Gauge('tf_requests_user', 'Total number of requests by user', ['user'])
requests_ranch = Gauge('tf_requests_ranch', 'Total number of requests by ranch', ['ranch'])

requests_new_total = Gauge('tf_requests_new_total', 'Total number of requests ended in new state')
requests_new_user = Gauge('tf_requests_new_user', 'Total number of requests ended in new state', ['user'])
requests_new_ranch = Gauge('tf_requests_new_ranch', 'Total number of requests ended in new state', ['ranch'])

requests_queued_total = Gauge('tf_requests_queued_total', 'Total number of requests ended in queued state')
requests_queued_user = Gauge('tf_requests_queued_user', 'Total number of requests ended in queued state', ['user'])
requests_queued_ranch = Gauge('tf_requests_queued_ranch', 'Total number of requests ended in queued state', ['ranch'])

requests_running_total = Gauge('tf_requests_running_total', 'Total number of requests ended in running state')
requests_running_user = Gauge('tf_requests_running_user', 'Total number of requests ended in running state', ['user'])
requests_running_ranch = Gauge(
    'tf_requests_running_ranch', 'Total number of requests ended in running state', ['ranch']
)

requests_complete_total = Gauge('tf_requests_complete_total', 'Total number of requests ended in complete state')
requests_complete_user = Gauge(
    'tf_requests_complete_user', 'Total number of requests ended in complete state', ['user']
)
requests_complete_ranch = Gauge(
    'tf_requests_complete_ranch', 'Total number of requests ended in complete state', ['ranch']
)

requests_error_total = Gauge('tf_requests_error_total', 'Total number of requests ended in error state')
requests_error_user = Gauge('tf_requests_error_user', 'Total number of requests ended in error state', ['user'])
requests_error_ranch = Gauge('tf_requests_error_ranch', 'Total number of requests ended in error state', ['ranch'])

requests_canceled_total = Gauge('tf_requests_canceled_total', 'Total number of requests ended in canceled state')
requests_canceled_user = Gauge(
    'tf_requests_canceled_user', 'Total number of requests ended in canceled state', ['user']
)
requests_canceled_ranch = Gauge(
    'tf_requests_canceled_ranch', 'Total number of requests ended in canceled state', ['ranch']
)

requests_cancel_requested_total = Gauge(
    'tf_requests_cancel_requested_total', 'Total number of requests ended in cancel_requested state'
)
requests_cancel_requested_user = Gauge(
    'tf_requests_cancel_requested_user', 'Total number of requests ended in cancel_requested state', ['user']
)
requests_cancel_requested_ranch = Gauge(
    'tf_requests_cancel_requested_ranch', 'Total number of requests ended in cancel_requested state', ['ranch']
)


requests_run_time = Histogram(
    'tf_requests_run_time_seconds',
    'Histogram of running time of requests',
    ['ranch'],
    buckets=[
        5,
        10,
        15,
        30,
        60,
        120,
        180,
        240,
        300,
        600,
        1200,
        1800,
        3600,
        7200,
        10800,
        14400,
        18000,
        21600,
        43200,
        86400,
        172800,
        259200,
        '+Inf',
    ],
)
requests_queued_time = Histogram(
    'tf_requests_queued_time_seconds',
    'Histogram of queued time of requests',
    ['ranch'],
    buckets=[
        5,
        10,
        15,
        30,
        60,
        120,
        180,
        240,
        300,
        600,
        1200,
        1800,
        3600,
        7200,
        10800,
        14400,
        18000,
        21600,
        43200,
        '+Inf',
    ],
)

requests_run_time_user = Histogram(
    'tf_requests_run_time_user_seconds',
    'Histogram of running time of requests of a user',
    ['user'],
    buckets=[
        5,
        10,
        15,
        30,
        60,
        120,
        180,
        240,
        300,
        600,
        1200,
        1800,
        3600,
        7200,
        10800,
        14400,
        18000,
        21600,
        43200,
        86400,
        172800,
        259200,
        '+Inf',
    ],
)

requests_queued_time_user = Histogram(
    'tf_requests_queued_time_user_seconds',
    'Histogram of queued time of requests of a user',
    ['user'],
    buckets=[
        5,
        10,
        15,
        30,
        60,
        120,
        180,
        240,
        300,
        600,
        1200,
        1800,
        3600,
        7200,
        10800,
        14400,
        18000,
        21600,
        43200,
        '+Inf',
    ],
)


@dataclass
class ProcessedRequest:
    created: datetime
    id: str


run_time_processed: List[ProcessedRequest] = []
queued_time_processed: List[ProcessedRequest] = []
run_time_processed_user: Dict[str, List[ProcessedRequest]] = {}
queued_time_processed_user: Dict[str, List[ProcessedRequest]] = {}


def count(session: Session) -> None:
    # count totals
    print('Counting totals')
    requests_total.set(session.query(Request).count())
    requests_new_total.set(session.query(Request).filter(Request.state == RequestStateType.NEW).count())
    requests_queued_total.set(session.query(Request).filter(Request.state == RequestStateType.QUEUED).count())
    requests_running_total.set(session.query(Request).filter(Request.state == RequestStateType.RUNNING).count())
    requests_complete_total.set(session.query(Request).filter(Request.state == RequestStateType.COMPLETE).count())
    requests_error_total.set(session.query(Request).filter(Request.state == RequestStateType.ERROR).count())
    requests_canceled_total.set(session.query(Request).filter(Request.state == RequestStateType.CANCELED).count())
    requests_cancel_requested_total.set(
        session.query(Request).filter(Request.state == RequestStateType.CANCEL_REQUESTED).count()
    )

    # count totals per ranch
    for ranch in RANCHES:
        print('Counting totals per ranch {}'.format(ranch))
        token_ids = [token.id for token in session.query(Token).all() if token.ranch == ranch]
        requests_ranch.labels(ranch).set(session.query(Request).filter(Request.token_id.in_(token_ids)).count())
        requests_new_ranch.labels(ranch).set(
            session.query(Request)
            .filter(Request.token_id.in_(token_ids), Request.state == RequestStateType.NEW)
            .count()
        )
        requests_queued_ranch.labels(ranch).set(
            session.query(Request)
            .filter(Request.token_id.in_(token_ids), Request.state == RequestStateType.QUEUED)
            .count()
        )
        requests_running_ranch.labels(ranch).set(
            session.query(Request)
            .filter(Request.token_id.in_(token_ids), Request.state == RequestStateType.RUNNING)
            .count()
        )
        requests_complete_ranch.labels(ranch).set(
            session.query(Request)
            .filter(Request.token_id.in_(token_ids), Request.state == RequestStateType.COMPLETE)
            .count()
        )
        requests_error_ranch.labels(ranch).set(
            session.query(Request)
            .filter(Request.token_id.in_(token_ids), Request.state == RequestStateType.ERROR)
            .count()
        )
        requests_canceled_ranch.labels(ranch).set(
            session.query(Request)
            .filter(Request.token_id.in_(token_ids), Request.state == RequestStateType.CANCELED)
            .count()
        )
        requests_cancel_requested_ranch.labels(ranch).set(
            session.query(Request)
            .filter(Request.token_id.in_(token_ids), Request.state == RequestStateType.CANCEL_REQUESTED)
            .count()
        )

    # get list of tokens
    tokens = session.query(Token).filter(Token.name.in_(settings.PER_USER_METRICS)).all()

    # count per user
    for token in tokens:
        requests = (
            session.query(Request)
            .filter(Request.token_id == token.id)
            .with_entities(Request.token_id, Request.state, Request.environments_requested)
            .all()
        )

        print('Processing {} requests for token {}'.format(len(requests), token.name))

        token_stats = {}

        for request in requests:
            token_name = token.name
            if request.environments_requested and len(request.environments_requested) > 0:
                token_name = (
                    ((request.environments_requested[0].get('settings') or {}).get('provisioning') or {}).get('tags')
                    or {}
                ).get('BusinessUnit') or token.name

            if token_name not in token_stats:
                token_stats[token_name] = {
                    RequestStateType.NEW: 0,
                    RequestStateType.QUEUED: 0,
                    RequestStateType.RUNNING: 0,
                    RequestStateType.COMPLETE: 0,
                    RequestStateType.ERROR: 0,
                    RequestStateType.CANCEL_REQUESTED: 0,
                    RequestStateType.CANCELED: 0,
                }

            token_stats[token_name][request.state] += 1

        if not requests:
            continue

        for token_name in token_stats.keys():
            requests_user.labels(token_name).set(sum(token_stats[token_name].values()))

            requests_new_user.labels(token_name).set(token_stats[token_name][RequestStateType.NEW])

            requests_queued_user.labels(token_name).set(token_stats[token_name][RequestStateType.QUEUED])

            requests_running_user.labels(token_name).set(token_stats[token_name][RequestStateType.RUNNING])

            requests_complete_user.labels(token_name).set(token_stats[token_name][RequestStateType.COMPLETE])

            requests_error_user.labels(token_name).set(token_stats[token_name][RequestStateType.ERROR])

            requests_canceled_user.labels(token_name).set(token_stats[token_name][RequestStateType.CANCELED])

            requests_cancel_requested_user.labels(token_name).set(
                token_stats[token_name][RequestStateType.CANCEL_REQUESTED]
            )


def observe(
    session: Session,
    check_time: datetime,
) -> None:
    """
    Until metrics is part of the API, we implement it here, but it is harder to do, as we do not want to process any
    request multiple times, that would spoil our metrics.

    So to do that:

    * store the processed request in a dataclass
    * look back max 7 days in the history
    * purge older processed requests according to their creation time
    * do this for both, run time and queued time

    In case of restart, we start with the current startup time, what might make us loose some requests, but as this
    application is not expected to be restarted that often, let's assume that is ok for now.
    """

    print(
        'Observing requests since {}, processed {} items for run time, processed {} items for queued time'.format(
            check_time,
            len(run_time_processed),
            len(queued_time_processed),
        )
    )

    # cleanup old processed requests
    for processed_request in queued_time_processed[:]:
        if processed_request.created < check_time:
            print(
                "Purging queued time processed_request '{}' which was created '{}'".format(
                    processed_request.id, processed_request.created
                )
            )
            queued_time_processed.remove(processed_request)

    for processed_request in run_time_processed[:]:
        if processed_request.created < check_time:
            print(
                "Purging run time processed_request '{}' which was created '{}'".format(
                    processed_request.id, processed_request.created
                )
            )
            run_time_processed.remove(processed_request)

    for processed_user in queued_time_processed_user.keys():
        for processed_request in queued_time_processed_user[processed_user][:]:
            if processed_request.created < check_time:
                print(
                    "Purging queued time processed_request '{}' which was created '{}' for user '{}'".format(
                        processed_request.id, processed_request.created, processed_user
                    )
                )
                queued_time_processed_user[processed_user].remove(processed_request)

    for processed_user in run_time_processed_user.keys():
        for processed_request in run_time_processed_user[processed_user][:]:
            if processed_request.created < check_time:
                print(
                    "Purging run time processed_request '{}' which was created '{}' for user '{}'".format(
                        processed_request.id, processed_request.created, processed_user
                    )
                )
                run_time_processed_user[processed_user].remove(processed_request)

    # get all requests created after STARTUP_TIME
    for ranch in RANCHES:
        token_ids = [token.id for token in session.query(Token).all() if token.ranch == ranch]
        requests = session.query(Request).filter(Request.token_id.in_(token_ids), Request.created >= check_time).all()

        print(f"Processing {len(requests)} requests for ranch {ranch}")

        for request in requests:
            # process queued time
            if (
                ProcessedRequest(id=request.id, created=request.created) not in queued_time_processed
                and request.queued_time
            ):
                queued_time_processed.append(ProcessedRequest(id=request.id, created=request.created))
                requests_queued_time.labels(ranch).observe(request.queued_time.total_seconds())

            # process run time
            if ProcessedRequest(id=request.id, created=request.created) not in run_time_processed and request.run_time:
                run_time_processed.append(ProcessedRequest(id=request.id, created=request.created))
                requests_run_time.labels(ranch).observe(request.run_time.total_seconds())

    # get list of users
    tokens = session.query(Token).filter(Token.name.in_(settings.PER_USER_METRICS)).all()

    # count per user
    for token in tokens:
        requests = session.query(Request).filter(Request.token_id == token.id, Request.created >= check_time).all()

        print(f"Processing {len(requests)} requests for user {token.name}")

        for request in requests:
            token_name = token.name
            if request.environments_requested and len(request.environments_requested) > 0:
                token_name = (
                    ((request.environments_requested[0].get('settings') or {}).get('provisioning') or {}).get('tags')
                    or {}
                ).get('BusinessUnit') or token.name

            if token_name not in queued_time_processed_user:
                queued_time_processed_user[token_name] = []

            if token_name not in run_time_processed_user:
                run_time_processed_user[token_name] = []

            # process queued time
            if (
                ProcessedRequest(id=request.id, created=request.created) not in queued_time_processed_user[token_name]
                and request.queued_time
            ):
                queued_time_processed_user[token_name].append(ProcessedRequest(id=request.id, created=request.created))
                requests_queued_time_user.labels(token_name).observe(request.queued_time.total_seconds())

            # process run time
            if (
                ProcessedRequest(id=request.id, created=request.created) not in run_time_processed_user[token_name]
                and request.run_time
            ):
                run_time_processed_user[token_name].append(ProcessedRequest(id=request.id, created=request.created))
                requests_run_time_user.labels(token_name).observe(request.run_time.total_seconds())


if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(int(os.environ.get('NUCLEUS_METRICS_PORT', DEFAULT_NUCLEUS_METRICS_PORT)))

    startup_time = datetime.utcnow()

    while True:
        # check maximum 7 days in the past
        check_time = startup_time
        last_7_days = datetime.utcnow() - timedelta(days=7)
        if check_time < last_7_days:
            check_time = last_7_days

        session: Session = next(get_db())
        observe(session, check_time)
        session.close()

        session: Session = next(get_db())
        count(session)
        session.close()

        print(r'All done \o/')

        time.sleep(int(os.environ.get('NUCLEUS_METRICS_TICK', DEFAULT_NUCLEUS_METRICS_TICK)))
