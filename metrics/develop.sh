#!/bin/bash -x

COCKROACH_IMAGE="docker.io/cockroachdb/cockroach:v22.2.9"

kill_all() {
    info "Stopping cockroachdb"
    podman kill cockroachdb-metrics
    podman rm -f cockroachdb-metrics
    exit
}

info() {
    echo -e "\033[0;32m[+] $@\033[0m"
}

error() {
    echo -e "\033[0;31mError: $@\033[0m"
    exit 1
}

podman kill cockroachdb-metrics &> /dev/null
podman rm -f cockroachdb-metrics &> /dev/null

command -v podman &>/dev/null || error "No 'podman' command found, please install it"
command -v gunzip &>/dev/null || error "No 'gunzip' command found, please install it"
[ ! -e dump.sql.gz ] && error "Did not find dump.sql.gz in the current directory"

trap kill_all TERM INT

info "Starting CockroachDB"
podman run -d --name=cockroachdb-metrics --network=host -p 26257:26257 -p 8080:8080 $COCKROACH_IMAGE start-single-node --insecure

info "Waiting for CockroachDB to start"
while ! podman run --rm --network=host $COCKROACH_IMAGE sql --insecure -e "SHOW DATABASES;"; do sleep 1; done

info "Creating Database"
podman run --rm --network=host $COCKROACH_IMAGE sql --insecure -e 'create database api;'

info "Restoring Backup (this might take a while)"
gunzip --stdout dump.sql.gz | podman run -i --rm --network=host $COCKROACH_IMAGE sql --insecure -d api &>/dev/null

info "Press CTRL+C to stop the development environment"
sleep infinity

wait $COCKROACH_PID
