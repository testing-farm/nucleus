"""add repo_key_pairs table

Revision ID: 57b6637067e2
Revises: 770cb8d13d5b
Create Date: 2024-09-09 12:38:47.864908

"""

import sqlalchemy as sa
import sqlalchemy_utils.types
from sqlalchemy.dialects import postgresql

from alembic import op

# revision identifiers, used by Alembic.
revision = '57b6637067e2'
down_revision = '770cb8d13d5b'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'repo_key_pairs',
        sa.Column('created', sa.DateTime(), nullable=False),
        sa.Column('updated', sa.DateTime(), nullable=False),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('url', sa.String(), nullable=False),
        sa.Column('token_id', postgresql.UUID(), nullable=False),
        sa.Column('public_key', sa.String(), nullable=False),
        sa.Column('private_key', sqlalchemy_utils.types.encrypted.encrypted_type.EncryptedType(), nullable=False),
        sa.ForeignKeyConstraint(
            ['token_id'],
            ['tokens.id'],
        ),
        sa.PrimaryKeyConstraint('id'),
    )


def downgrade():
    op.drop_table('repo_key_pairs')
