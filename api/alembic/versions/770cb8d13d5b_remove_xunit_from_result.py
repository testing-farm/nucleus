"""Remove xunit from result

Revision ID: 770cb8d13d5b
Revises: f472d7e106a9
Create Date: 2024-09-09 10:45:57.382981

"""

import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = '770cb8d13d5b'
down_revision = 'f472d7e106a9'
branch_labels = None
depends_on = None


def upgrade():

    # Remove the 'xunit' field from the 'result' JSON column
    op.execute(
        """
        UPDATE requests
        SET result = result - 'xunit'
        WHERE result ? 'xunit'
    """
    )

    # Update the 'xunit_url' with the modified 'artifacts' field from the 'run' JSON column
    op.execute(
        """
        UPDATE requests
        SET result = jsonb_set(
                result,
                '{xunit_url}',
                to_jsonb((run->>'artifacts') || '/result.xml'),
                true
            )
        WHERE run IS NOT NULL
        AND run->>'artifacts' IS NOT NULL
        AND result IS NOT NULL
        AND result != 'null'
        AND result->>'xunit_url' IS NULL
    """
    )


def downgrade():
    # Add the 'xunit' key back with a null value
    op.execute(
        """
        UPDATE requests
        SET result = jsonb_set(result, '{xunit}', 'null'::jsonb)
        WHERE result ? 'xunit' = false
    """
    )
