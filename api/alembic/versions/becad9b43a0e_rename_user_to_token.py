"""rename user to token

Revision ID: becad9b43a0e
Revises: 320b4191c1f4
Create Date: 2024-05-31 11:57:37.929313

"""

import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = 'becad9b43a0e'
down_revision = '320b4191c1f4'
branch_labels = None
depends_on = None


def upgrade():
    op.rename_table('users', 'tokens')
    op.alter_column(
        'requests',
        'user_id',
        new_column_name='token_id',
    )

    # Make sure we remove the < v20 default constraint name for the migration
    # Our production database was created on old version of cockrachdb
    # https://github.com/cockroachdb/docs/issues/11965
    results = op.get_bind().execute("SHOW CONSTRAINTS from requests").fetchall()
    if any(True for result in results if 'fk_user_id_ref_users' in result[1]):
        op.drop_constraint('fk_user_id_ref_users', 'requests', type_='foreignkey')

    results = op.get_bind().execute("SHOW CONSTRAINTS from requests").fetchall()
    if any(True for result in results if 'requests_user_id_fkey' in result[1]):
        op.drop_constraint('requests_user_id_fkey', 'requests', type_='foreignkey')

    op.create_foreign_key('requests_token_id_fkey', 'requests', 'tokens', ['token_id'], ['id'])


def downgrade():
    op.rename_table('tokens', 'users')
    op.alter_column(
        'requests',
        'token_id',
        new_column_name='user_id',
    )

    # NOTE: do not bother adding back the old foreign key
    op.drop_constraint('requests_token_id_fkey', 'requests', type_='foreignkey')
    op.create_foreign_key('requests_user_id_fkey', 'requests', 'users', ['user_id'], ['id'])
