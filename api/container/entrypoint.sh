#!/bin/sh

#
# Helpers functions
#

terminate() {
    # optional terminate command, can be used to wait for specific conditions when the container should finish
    if [ -n "TERMINATE_COMMAND" ]; then
        $TERMINATE_COMMAND
    fi

    # no pid means we failed too early
    if [ -z "$PID" ]; then
        echo "Skipping PID termination, entrypoint failed too early"
        exit 1
    fi

    echo "Terminating application"
    kill -TERM $PID

    # wait for the application to terminate
    wait $PID
}

# trap is needed to correctly propagate SIGTERM from container
trap terminate TERM INT

# helpers
error() { echo "Error: $@"; exit 1; }
psql_cmd() { psql -h $TF_API_DATABASE_HOST -p $TF_API_DATABASE_PORT -U $TF_API_DATABASE_USER -c "$*;"; }

#
# Wait for database to start
#

# Default value are used for testing and local development
SETTINGS_FILE=${SETTINGS_FILE:-settings.yaml}
TF_API_DATABASE_HOST=${TF_API_DATABASE_HOST:-$(yq eval '.default.DATABASE_HOST' $SETTINGS_FILE)}
TF_API_DATABASE_PORT=${TF_API_DATABASE_PORT:-$(yq eval '.default.DATABASE_PORT' $SETTINGS_FILE)}
TF_API_DATABASE_USER=${TF_API_DATABASE_USER:-$(yq eval '.default.DATABASE_USER' $SETTINGS_FILE)}
TF_API_DATABASE_NAME=${TF_API_DATABASE_NAME:-$(yq eval '.default.DATABASE_NAME' $SETTINGS_FILE)}
TF_API_PORT=${TF_API_PORT:-$(yq eval '.default.PORT' $SETTINGS_FILE)}
WAIT_TIMEOUT=${WAIT_TIMEOUT:-60}
WAIT_TICK=${WAIT_TICK:-1}

check_cockroachdb() {
    echo "Checking database connection"
    pg_isready -h $TF_API_DATABASE_HOST -p $TF_API_DATABASE_PORT -U $TF_API_DATABASE_USER
    return $?
}

wait_cockroachdb() {
    time=0
    until check_cockroachdb; do
        time=$((time + WAIT_TICK))
        [ $time -ge $WAIT_TIMEOUT ] && error "Failed to wait for database to start"
        sleep $WAIT_TICK
    done
}

#
# Main script
#

# a name of the api to start
APP=$1

# shift the arguments so we can easily process the rest in loop
shift

[ -z "$APP" ] && { error "No api to run passed to entrypoint script"; exit 1; }

case $APP in
    public)
        wait_cockroachdb
        COMMAND="uvicorn --host 0.0.0.0 --port $TF_API_PORT tft.nucleus.api.public:api"
        ;;
    internal)
        wait_cockroachdb
        COMMAND="uvicorn --host 0.0.0.0 --port $TF_API_PORT tft.nucleus.api.internal:api"
        ;;
    initdb)
        wait_cockroachdb
        # Create database if doesn't exists
        if ! psql_cmd "USE $TF_API_DATABASE_NAME"; then
            psql_cmd "CREATE DATABASE $TF_API_DATABASE_NAME"
        fi
        # Initialize or upgrade the database to the latest version
        alembic upgrade head || { echo "failed to upgrade DB Schema"; exit 1; }
        # Initialize records from server.yml
        exit 0
        ;;
    restoredb)
        wait_cockroachdb

        # Restore only if the target database does not exist
        if psql_cmd "USE $TF_API_DATABASE_NAME"; then
            echo "Database $TF_API_DATABASE_NAME already exists, skipping restoration"
            exit 0
        else
            psql_cmd "CREATE DATABASE $TF_API_DATABASE_NAME"
        fi

        for ARG in "$@"; do
            if [ -z "$ARG" ]; then
                error "No download URL to the XZ dump specified."
            fi
            # Check if remote file is an XZ archive
            mime_type="$(curl -Ls --range 0-5 "$ARG" | file - -b --mime-type)"
            if [ "$mime_type" != "application/x-xz" ]; then
                error "The URL does not contain an XZ archive, detected '$mime_type'."
            fi
            # Restore from given dump
            dump=$(mktemp)
            curl -Lo "$dump" "$ARG"
            pv "$dump" | xzcat | psql -h $TF_API_DATABASE_HOST -p $TF_API_DATABASE_PORT -U $TF_API_DATABASE_USER -d $TF_API_DATABASE_NAME
            rm -f "$dump"
        done

        # Upgrade the database to the latest version
        alembic upgrade head || { echo "failed to upgrade DB Schema"; exit 1; }
        ;;
    waitdb)
        wait_cockroachdb
        ;;
    *)
        echo "Unknown app '$APP'"
        exit 1
        ;;
esac

# We run the command in background to get his PID which is used to properly
# terminate it with SIGTERM signal.

$COMMAND &
PID=$!

wait $PID
