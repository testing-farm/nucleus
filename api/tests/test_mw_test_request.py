import logging
from datetime import datetime
from unittest.mock import MagicMock, call
from uuid import UUID

import pytest

from tft.nucleus.api.core import access_control, errors
from tft.nucleus.api.core.database import Request, RequestStateType
from tft.nucleus.api.core.schemes import test_request
from tft.nucleus.api.middleware import test_request as mw_test_request


@pytest.fixture
def access_logger(monkeypatch):
    access_log_mock = MagicMock()
    access_log_mock.info = MagicMock()
    monkeypatch.setattr(access_control, 'access_logger', access_log_mock)
    return access_log_mock


def test_get_test_request(monkeypatch, session, access_logger, fastapi_request, user1_admin_role_token):
    monkeypatch.setattr(
        mw_test_request.crud_test_request,
        'get_test_request',
        MagicMock(
            return_value=test_request.RequestGetUpdateOut(
                id=UUID('11111111-1111-1111-1111-111111111111'),
                user_id=UUID('22222222-2222-2222-2222-222222222222'),
                token_id=UUID('22222222-2222-2222-2222-222222222222'),
                state='new',
                environments_requested=[],
                test={'fmf': {'url': 'https://example.com'}},
                run={'artifacts': 'http://example.com/artifacts'},
                queued_time=None,
                run_time=None,
                created=datetime.utcnow(),
                updated=datetime.utcnow(),
            )
        ),
    )
    result = mw_test_request.get_test_request(fastapi_request, session, UUID('11111111-1111-1111-1111-111111111111'))

    assert isinstance(result, test_request.RequestGetUpdateOut)
    assert result.id == UUID('11111111-1111-1111-1111-111111111111')
    assert result.state == 'new'
    assert str(result.test.fmf.url) == 'https://example.com'
    assert access_logger.info.call_args_list == [
        call(
            'Access attempt',
            extra={
                'client_ip': 'localhost',
                'object_id': UUID('11111111-1111-1111-1111-111111111111'),
                'what': 'access_control',
                'role': 'anonymous',
                'object': 'request',
                'action': 'get',
                'access': True,
            },
        )
    ]


def test_get_test_requests(monkeypatch, session, access_logger, fastapi_request):
    monkeypatch.setattr(
        mw_test_request.crud_test_request,
        'get_test_requests',
        MagicMock(
            return_value=[
                test_request.RequestGetUpdateOut(
                    id=UUID('11111111-1111-1111-1111-111111111111'),
                    user_id=UUID('22222222-2222-2222-2222-222222222222'),
                    token_id=UUID('22222222-2222-2222-2222-222222222222'),
                    state='new',
                    environments_requested=[],
                    test={'fmf': {'url': 'https://example.com'}},
                    run={'artifacts': 'http://example.com/artifacts'},
                    queued_time=None,
                    run_time=None,
                    created=datetime.utcnow(),
                    updated=datetime.utcnow(),
                ),
                test_request.RequestGetUpdateOut(
                    id=UUID('22222222-2222-2222-2222-222222222222'),
                    user_id=UUID('22222222-2222-2222-2222-222222222222'),
                    token_id=UUID('22222222-2222-2222-2222-222222222222'),
                    state='new',
                    environments_requested=[],
                    test={'fmf': {'url': 'https://example.com'}},
                    run={'artifacts': 'http://example.com/artifacts'},
                    queued_time=None,
                    run_time=None,
                    created=datetime.utcnow(),
                    updated=datetime.utcnow(),
                ),
            ]
        ),
    )

    result = mw_test_request.get_test_requests(fastapi_request, session, 'new', None, None, None, None)

    assert isinstance(result, list)
    assert len(result) == 2
    assert all(isinstance(r, test_request.RequestGetUpdateOut) for r in result)
    assert access_logger.info.call_args_list == [
        call(
            'Access attempt',
            extra={
                'client_ip': 'localhost',
                'what': 'access_control',
                'role': 'anonymous',
                'object': 'requests_list',
                'action': 'get',
                'access': True,
            },
        )
    ]


def test_get_test_request_authenticated(
    monkeypatch, session, access_logger, fastapi_request, user1, user1_admin_role_token
):
    monkeypatch.setattr(
        mw_test_request.crud_test_request,
        'get_test_request_authenticated',
        MagicMock(
            return_value=test_request.RequestGetAuthenticatedOut(
                id=UUID('11111111-1111-1111-1111-111111111111'),
                user_id=UUID('22222222-2222-2222-2222-222222222222'),
                token_id=UUID('22222222-2222-2222-2222-222222222222'),
                state='new',
                environments_requested=[],
                test={'fmf': {'url': 'https://example.com'}},
                notification={'webhook': {'url': 'http://example.com/webhook'}},
                run={'artifacts': 'http://example.com/artifacts'},
                queued_time=None,
                run_time=None,
                created=datetime.utcnow(),
                updated=datetime.utcnow(),
            )
        ),
    )

    monkeypatch.setattr(mw_test_request, 'is_test_request_owned_by_token', MagicMock(return_value=True))

    result = mw_test_request.get_test_request_authenticated(
        fastapi_request, session, UUID('11111111-1111-1111-1111-111111111111'), user1_admin_role_token.api_key, None
    )

    assert isinstance(result, test_request.RequestGetAuthenticatedOut)
    assert result.id == UUID('11111111-1111-1111-1111-111111111111')
    assert str(result.notification.webhook.url) == 'http://example.com/webhook'
    assert access_logger.info.call_args_list == [
        call(
            'Access attempt',
            extra={
                'client_ip': 'localhost',
                'object_id': UUID('11111111-1111-1111-1111-111111111111'),
                'token_id': access_logger.info.call_args_list[0].kwargs['extra']['token_id'],
                'token_name': 'test-name',
                'token_enabled': True,
                'user_id': user1.id,
                'user_name': 'user1',
                'what': 'access_control',
                'role': 'admin',
                'object': 'request_owned',
                'action': 'get_secrets',
                'access': True,
            },
        )
    ]


def test_create_test_request(monkeypatch, session, access_logger, fastapi_request, user1, user1_admin_role_token):
    monkeypatch.setattr(
        mw_test_request.crud_test_request,
        'create_test_request',
        MagicMock(
            return_value=test_request.RequestCreateOut(
                id=UUID('11111111-1111-1111-1111-111111111111'),
                state='new',
                environments=[],
                test={'fmf': {'url': 'https://example.com'}},
                created=datetime.utcnow(),
                updated=datetime.utcnow(),
            )
        ),
    )

    test_request_in = test_request.RequestCreateIn(
        api_key=user1_admin_role_token.api_key, test={'fmf': {'url': 'https://example.com'}}, environments=[]
    )

    result = mw_test_request.create_test_request(
        fastapi_request, session, test_request_in, user1_admin_role_token.api_key
    )

    assert isinstance(result, test_request.RequestCreateOut)
    assert result.id == UUID('11111111-1111-1111-1111-111111111111')
    assert result.state == 'new'
    assert access_logger.info.call_args_list == [
        call(
            'Access attempt',
            extra={
                'client_ip': 'localhost',
                'token_id': access_logger.info.call_args_list[0].kwargs['extra']['token_id'],
                'token_name': 'test-name',
                'token_enabled': True,
                'user_id': user1.id,
                'user_name': 'user1',
                'what': 'access_control',
                'role': 'admin',
                'object': 'request',
                'action': 'create',
                'access': True,
            },
        )
    ]


def test_delete_test_request(monkeypatch, session, access_logger, fastapi_request, user1, user1_admin_role_token):
    monkeypatch.setattr(
        mw_test_request.crud_test_request,
        'get_test_request_db',
        MagicMock(return_value=MagicMock(state=RequestStateType.NEW)),
    )
    monkeypatch.setattr(mw_test_request.crud_test_request, 'delete_test_request', MagicMock())
    monkeypatch.setattr(
        mw_test_request.crud_test_request,
        'get_test_request',
        MagicMock(
            return_value=test_request.RequestGetUpdateOut(
                id=UUID('11111111-1111-1111-1111-111111111111'),
                user_id=UUID('22222222-2222-2222-2222-222222222222'),
                token_id=UUID('22222222-2222-2222-2222-222222222222'),
                state='cancel-requested',
                environments_requested=[],
                test={'fmf': {'url': 'https://example.com'}},
                run={'artifacts': 'http://example.com/artifacts'},
                queued_time=None,
                run_time=None,
                created=datetime.utcnow(),
                updated=datetime.utcnow(),
            )
        ),
    )

    status_code, result = mw_test_request.delete_test_request(
        fastapi_request, session, UUID('11111111-1111-1111-1111-111111111111'), None, user1_admin_role_token.api_key
    )

    assert status_code == 200
    assert isinstance(result, test_request.RequestGetUpdateOut)
    assert result.state == 'cancel-requested'
    assert access_logger.info.call_args_list == [
        call(
            'Access attempt',
            extra={
                'client_ip': 'localhost',
                'object_id': UUID('11111111-1111-1111-1111-111111111111'),
                'token_id': access_logger.info.call_args_list[0].kwargs['extra']['token_id'],
                'token_name': 'test-name',
                'token_enabled': True,
                'user_id': user1.id,
                'user_name': 'user1',
                'what': 'access_control',
                'role': 'admin',
                'object': 'request',
                'action': 'delete',
                'access': True,
            },
        )
    ]
