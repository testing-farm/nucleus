import time
from unittest.mock import MagicMock
from uuid import uuid4

import psycopg2.errors
import pytest
import sqlalchemy.exc
from fastapi.testclient import TestClient
from ruamel.yaml import YAML
from sqlalchemy_utils import create_database, database_exists

from alembic import command
from alembic.config import Config
from tft.nucleus.api.core import errors
from tft.nucleus.api.core.database import (
    RepoKeyPair,
    Request,
    SessionLocal,
    Token,
    User,
    engine,
)
from tft.nucleus.api.crud.token import get_token_db
from tft.nucleus.api.crud.user import get_user_by_auth_id_and_auth_method
from tft.nucleus.api.internal import api as internal_api
from tft.nucleus.api.login import create_jwt
from tft.nucleus.api.public import api as public_api

public_client = TestClient(public_api)
internal_client = TestClient(internal_api)

# Path to assets relative to tox location
ASSERTS_PATH = "tests/assets.yml"


def get_assets():
    yaml = YAML(typ='safe')
    with open(ASSERTS_PATH, 'r') as assets_file:
        assets = yaml.load(assets_file)
    return assets


assets = get_assets()


@pytest.fixture
def session():
    return SessionLocal()


@pytest.fixture
def fastapi_request():
    return MagicMock(client=MagicMock(host='localhost'))


@pytest.fixture(scope="session", autouse=True)
def create_nucleus_database() -> None:
    print(f'Using database {str(engine.url)}')

    # NOTE: The database creation used to be under `if sqlalchemy_utils.database_exists(engine.url)` check, but the
    # `database_exists` function always returns True since Cockroach v23.
    try:
        create_database(engine.url)
    except psycopg2.errors.DuplicateDatabase:
        pass
    alembic_cfg = Config('alembic.ini')
    alembic_cfg.set_main_option('sqlalchemy.url', str(engine.url))
    # Sometimes when tests run in parallel we hit this problem:
    # https://www.cockroachlabs.com/docs/v21.1/transaction-retry-error-reference#retry_serializable
    # Add a retry with a small sleep to workaround.
    try:
        command.upgrade(alembic_cfg, 'head')
    except sqlalchemy.exc.OperationalError:
        print(f'Retrying alembic upgrade')
        time.sleep(1)
        command.upgrade(alembic_cfg, 'head')


# We need to have at least one token and user to use the API
@pytest.fixture
def user1() -> User:
    return add_user(auth_id='developer', auth_method='login', auth_name='user1')


@pytest.fixture
def user2() -> User:
    return add_user(auth_id='developer2', auth_method='unit-test', auth_name='user2')


@pytest.fixture
def user1_user_role_token(user1) -> Token:
    return add_token(
        user_id=user1.id,
        name='user-role',
        api_key=str(uuid4()),
        enabled=True,
        ranch='public',
        role='user',
    )


@pytest.fixture
def user1_user_role_token_redhat(user1) -> Token:
    return add_token(
        user_id=user1.id,
        name='user-role-redhat',
        api_key=str(uuid4()),
        enabled=True,
        ranch='redhat',
        role='user',
    )


@pytest.fixture
def user1_poweruser_role_token(user1) -> Token:
    return add_token(
        user_id=user1.id,
        name='poweruser-role',
        api_key=str(uuid4()),
        enabled=True,
        ranch='public',
        role='poweruser',
    )


@pytest.fixture
def user1_worker_role_token(user1) -> Token:
    return add_token(
        user_id=user1.id,
        name='worker-role',
        api_key=str(uuid4()),
        enabled=True,
        ranch='public',
        role='worker',
    )


@pytest.fixture
def user1_admin_role_token(user1) -> Token:
    return add_token(
        user_id=user1.id,
        name='test-name',
        api_key=str(uuid4()),
        enabled=True,
        ranch='public',
        role='admin',
    )


@pytest.fixture
def user1_banned_role_token(user1) -> Token:
    return add_token(
        user_id=user1.id,
        name='banned-role',
        api_key=str(uuid4()),
        enabled=True,
        ranch='public',
        role='banned',
    )


@pytest.fixture
def user1_user_role_token_disabled(user1) -> Token:
    return add_token(
        user_id=user1.id,
        name='user-role',
        api_key=str(uuid4()),
        enabled=False,
        ranch='public',
        role='user',
    )


@pytest.fixture
def user1_admin_role_token_disabled(user1) -> Token:
    return add_token(
        user_id=user1.id,
        name='admin-role',
        api_key=str(uuid4()),
        enabled=False,
        ranch='public',
        role='admin',
    )


@pytest.fixture
def user1_fedora_admin_role_token(user1) -> Token:
    return add_token(
        user_id=user1.id,
        name='fedora-admin-role',
        api_key=str(uuid4()),
        enabled=True,
        ranch='public',
        role='fedora_admin',
    )


@pytest.fixture
def user2_user_role_token(user2) -> Token:
    return add_token(
        user_id=user2.id,
        name='user-role',
        api_key=str(uuid4()),
        enabled=True,
        ranch='public',
        role='user',
    )


@pytest.fixture
def user2_poweruser_role_token(user2) -> Token:
    return add_token(
        user_id=user2.id,
        name='poweruser-role',
        api_key=str(uuid4()),
        enabled=True,
        ranch='public',
        role='poweruser',
    )


@pytest.fixture
def user2_worker_role_token(user2) -> Token:
    return add_token(
        user_id=user2.id,
        name='worker-role',
        api_key=str(uuid4()),
        enabled=True,
        ranch='public',
        role='worker',
    )


@pytest.fixture
def user2_admin_role_token(user2) -> Token:
    return add_token(
        user_id=user2.id,
        name='test-name',
        api_key=str(uuid4()),
        enabled=True,
        ranch='public',
        role='admin',
    )


@pytest.fixture
def user2_banned_role_token(user2) -> Token:
    return add_token(
        user_id=user2.id,
        name='dddddddd-dddd-dddd-dddd-dddddddddddd',
        api_key=str(uuid4()),
        enabled=True,
        ranch='public',
        role='banned',
    )


@pytest.fixture
def user2_fedora_admin_role_token(user2) -> Token:
    return add_token(
        user_id=user2.id,
        name='fedora-admin-role',
        api_key=str(uuid4()),
        enabled=True,
        ranch='public',
        role='fedora_admin',
    )


def add_user(auth_id: str, auth_method: str, auth_name: str) -> User:
    session = SessionLocal()
    try:
        user = get_user_by_auth_id_and_auth_method(session, auth_id, auth_method)
    except errors.NoSuchEntityError:
        user = User(id=str(uuid4()), auth_id=auth_id, auth_method=auth_method, auth_name=auth_name, enabled=True)
        session.add(user)
        session.commit()
        session.refresh(user)
    session.close()
    return user


def add_token(user_id: str, name: str, api_key: str, enabled: bool, ranch: str, role: str) -> Token:
    session = SessionLocal()
    token = Token(id=str(uuid4()), user_id=user_id, name=name, api_key=api_key, enabled=enabled, ranch=ranch, role=role)
    session.add(token)
    session.commit()
    session.refresh(token)
    session.close()
    return token


def add_request(request: Request) -> None:
    session = SessionLocal()
    session.add(request)
    session.commit()
    session.refresh(request)
    session.close()


@pytest.fixture
def delete_all_requests():
    session = SessionLocal()
    session.query(Request).delete()
    session.commit()
    session.close()


@pytest.fixture
def delete_all_repo_key_pairs():
    session = SessionLocal()
    session.query(RepoKeyPair).delete()
    session.commit()
    session.close()


@pytest.fixture
def delete_all_tokens(delete_all_requests, delete_all_repo_key_pairs):
    session = SessionLocal()
    session.query(Token).delete()
    session.commit()
    session.close()


@pytest.fixture
def delete_all_users(delete_all_tokens):
    session = SessionLocal()
    session.query(User).delete()
    session.commit()
    session.close()


@pytest.fixture()
def jwt_user1_public_redhat(user1):
    return create_jwt(user1.auth_id, user1.auth_method, user1.auth_name, 'user', ['public', 'redhat'])


@pytest.fixture()
def jwt_user2_public(user2):
    return create_jwt(user2.auth_id, user2.auth_method, user2.auth_name, 'user', ['public'])


@pytest.fixture()
def jwt_admin1_public_redhat(user1):
    return create_jwt(user1.auth_id, user1.auth_method, user1.auth_name, 'admin', ['public', 'redhat'])


@pytest.fixture()
def jwt_admin2_public(user2):
    return create_jwt(user2.auth_id, user2.auth_method, user2.auth_name, 'admin', ['public'])
