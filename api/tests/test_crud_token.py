from uuid import UUID

import pytest

from tft.nucleus.api.core import errors
from tft.nucleus.api.core.database import Token
from tft.nucleus.api.core.schemes import token
from tft.nucleus.api.crud import token as crud_token


def test_create_token(session, user1):
    token_in = token.TokenCreateIn(name="Test Token", ranch="public", role="user")
    result = crud_token.create_token(session, token_in, user1.id)

    assert isinstance(result, token.TokenCreateOut)
    assert result.name == "Test Token"
    assert result.ranch == "public"
    assert result.role == "user"
    assert result.user_id == str(user1.id)


def test_get_token_db_by_api_key(session, user1_admin_role_token):
    result = crud_token.get_token_db(session, api_key=user1_admin_role_token.api_key)

    assert isinstance(result, Token)
    assert result.api_key == user1_admin_role_token.api_key


def test_get_token_db_by_token_id(session, user1_admin_role_token):
    result = crud_token.get_token_db(session, token_id=user1_admin_role_token.id)

    assert isinstance(result, Token)
    assert str(result.id) == str(user1_admin_role_token.id)


def test_get_token_db_not_found(session):
    with pytest.raises(errors.NotAuthorizedError):
        crud_token.get_token_db(session, api_key="non_existent_key")


def test_get_token(session, user1_admin_role_token):
    result = crud_token.get_token(session, api_key=user1_admin_role_token.api_key)

    assert isinstance(result, token.TokenGetUpdateOut)
    assert result.enabled == True


def test_get_tokens(session, user1_admin_role_token, user1_user_role_token):
    results = crud_token.get_tokens(session)

    assert isinstance(results, list)
    assert len(results) >= 2
    assert all(isinstance(t, token.TokenGetUpdateOut) for t in results)


def test_update_token(session, user1_admin_role_token):
    token_update = token.TokenUpdateIn(name="Updated Token Name")
    result = crud_token.update_token(session, user1_admin_role_token.id, token_update)

    assert isinstance(result, token.TokenGetUpdateOut)
    assert result.name == "Updated Token Name"


def test_disable_token(session, user1_admin_role_token):
    result = crud_token.disable_token(session, user1_admin_role_token.id)

    assert isinstance(result, token.TokenGetUpdateOut)
    assert result.enabled == False


def test_get_token_db_disabled_not_allowed(session, user1_admin_role_token):
    crud_token.disable_token(session, user1_admin_role_token.id)

    with pytest.raises(errors.NotAuthorizedError):
        crud_token.get_token_db(session, token_id=user1_admin_role_token.id, allow_disabled=False)
