import pytest

from tft.nucleus.api.config import settings
from tft.nucleus.api.core.schemes.compose import Compose, SupportedComposesOut
from tft.nucleus.api.crud import composes


def test_get_composes_from_file(tmp_path):
    # Create a temporary file with test composes
    test_composes = ["Fedora-36", "RHEL-8.6.0", "^(regex-compose.*)(?:aarch64|x86_64)$"]
    test_file = tmp_path / "test_composes.txt"
    test_file.write_text("\n".join(test_composes))

    result = composes.get_composes_from_file(str(test_file))

    assert len(result) == 3
    assert all(isinstance(compose, Compose) for compose in result)
    assert [compose.name for compose in result] == test_composes


def test_get_composes_from_file_empty_lines(tmp_path):
    test_composes = ["Fedora-36", "", "RHEL-8.6.0", "", "^(regex-compose.*)(?:aarch64|x86_64)$"]
    test_file = tmp_path / "test_composes_with_empty.txt"
    test_file.write_text("\n".join(test_composes))

    result = composes.get_composes_from_file(str(test_file))

    assert len(result) == 3
    assert all(isinstance(compose, Compose) for compose in result)
    assert [compose.name for compose in result] == [line for line in test_composes if line]


@pytest.mark.parametrize("ranch", ["public", "redhat"])
def test_get_ranch_composes(ranch, monkeypatch):
    test_composes = [Compose(name="TestCompose-1"), Compose(name="TestCompose-2")]
    monkeypatch.setattr(composes, "get_composes_from_file", lambda _: test_composes)

    result = composes.get_ranch_composes(ranch)

    assert isinstance(result, SupportedComposesOut)
    assert len(result.composes) == 2
    assert all(isinstance(compose, Compose) for compose in result.composes)
    assert [compose.name for compose in result.composes] == ["TestCompose-1", "TestCompose-2"]


def test_get_ranch_composes_uses_correct_file():
    original_composes = settings.COMPOSES
    settings.COMPOSES = {"PUBLIC": "public_composes.txt", "REDHAT": "redhat_composes.txt"}

    with pytest.raises(FileNotFoundError):
        composes.get_ranch_composes("public")

    settings.COMPOSES = original_composes
