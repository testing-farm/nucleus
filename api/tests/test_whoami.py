from unittest.mock import MagicMock

import pkg_resources
import pytest

from tft.nucleus.api.core.database import SessionLocal, User

from .conftest import public_client


@pytest.mark.parametrize(
    'token_selector, expected_status_code',
    [
        ('user1_user_role_token', 200),
        ('user2_admin_role_token', 200),
        ('user1_banned_role_token', 403),
        ('user1_user_role_token_disabled', 403),
        ('foo', 401),
        (None, 401),
    ],
)
def test_whoami(
    user1_user_role_token,
    user2_admin_role_token,
    user1_banned_role_token,
    user1_user_role_token_disabled,
    token_selector,
    expected_status_code,
    user1,
    user2,
):
    """
    Test the whoami endpoint.
    """

    token, user = {
        'user1_user_role_token': (user1_user_role_token, user1),
        'user2_admin_role_token': (user2_admin_role_token, user2),
        'user1_banned_role_token': (user1_banned_role_token, user1),
        'user1_user_role_token_disabled': (user1_user_role_token_disabled, user1),
        'foo': (MagicMock(api_key='foo'), None),
        None: (MagicMock(api_key=None), None),
    }.get(token_selector, (None, None))

    response = public_client.get(
        '/v0.1/whoami',
        headers={'Authorization': f'Bearer {token.api_key}'},
    )
    assert response.status_code == expected_status_code

    if expected_status_code == 200:
        assert response.json() == {
            'token': {
                'created': token.created.strftime("%Y-%m-%dT%H:%M:%S.%f"),
                'enabled': token.enabled,
                'id': token.id,
                'name': token.name,
                'ranch': token.ranch,
                'role': token.role,
                'updated': token.updated.strftime("%Y-%m-%dT%H:%M:%S.%f"),
                'user_id': token.user_id,
            },
            'user': {
                'auth_id': user.auth_id,
                'auth_method': user.auth_method,
                'auth_name': user.auth_name,
                'created': user.created.strftime("%Y-%m-%dT%H:%M:%S.%f"),
                'enabled': user.enabled,
                'id': user.id,
                'updated': user.updated.strftime("%Y-%m-%dT%H:%M:%S.%f"),
            },
        }
