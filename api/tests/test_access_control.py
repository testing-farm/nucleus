import json
from copy import deepcopy
from unittest.mock import MagicMock

import pytest

from tft.nucleus.api.core.database import SessionLocal, Token

from .conftest import add_user, assets, internal_client, public_client

#
# Test access to request operations
#


@pytest.mark.parametrize(
    'token_selector, expected_status_code',
    [
        ('user1_user_role_token', 200),
        ('user1_poweruser_role_token', 200),
        ('user1_worker_role_token', 200),
        ('user1_admin_role_token', 200),
        ('user1_banned_role_token', 403),
        ('user1_user_role_token_disabled', 403),
        ('user1_fedora_admin_role_token', 403),
        ('foo', 401),
        (None, 422),
    ],
)
def test_access_create_test_request(
    delete_all_tokens,
    user1_user_role_token,
    user1_poweruser_role_token,
    user1_worker_role_token,
    user1_admin_role_token,
    user1_banned_role_token,
    user1_user_role_token_disabled,
    user1_fedora_admin_role_token,
    token_selector,
    expected_status_code,
):
    """
    Test access to the create test request endpoint.
    """
    token = {
        'user1_user_role_token': user1_user_role_token,
        'user1_poweruser_role_token': user1_poweruser_role_token,
        'user1_worker_role_token': user1_worker_role_token,
        'user1_admin_role_token': user1_admin_role_token,
        'user1_banned_role_token': user1_banned_role_token,
        'user1_user_role_token_disabled': user1_user_role_token_disabled,
        'user1_fedora_admin_role_token': user1_fedora_admin_role_token,
        'foo': MagicMock(api_key='foo'),
        None: MagicMock(api_key=None),
    }.get(token_selector)

    request = deepcopy(assets['test_request_example'])

    # token in header
    del request['api_key']
    response = public_client.post(
        '/v0.1/requests',
        json=request,
        headers={'Authorization': 'Bearer {}'.format(token.api_key)} if token.api_key else {},
    )
    assert response.status_code == expected_status_code

    # token in json
    request['api_key'] = token.api_key
    response = public_client.post('/v0.1/requests', json=request)
    assert response.status_code == expected_status_code


@pytest.mark.parametrize(
    'token_selector, expected_status_code',
    [
        ('user1_user_role_token', 403),
        ('user1_poweruser_role_token', 403),
        ('user1_worker_role_token', 200),
        ('user1_admin_role_token', 200),
        ('user1_banned_role_token', 403),
        ('user1_user_role_token_disabled', 403),
        ('user1_fedora_admin_role_token', 403),
        ('foo', 401),
        (None, 422),
        # TODO: check access between tokens of the same user
    ],
)
def test_access_get_test_request_internal(
    delete_all_tokens,
    user1_poweruser_role_token,
    user1_user_role_token,
    user1_worker_role_token,
    user1_admin_role_token,
    user1_banned_role_token,
    user1_user_role_token_disabled,
    user1_fedora_admin_role_token,
    user2_user_role_token,
    token_selector,
    expected_status_code,
):
    """
    Test access to the get test request endpoint.
    """
    token = {
        'user1_user_role_token': user1_user_role_token,
        'user1_poweruser_role_token': user1_poweruser_role_token,
        'user1_worker_role_token': user1_worker_role_token,
        'user1_admin_role_token': user1_admin_role_token,
        'user1_banned_role_token': user1_banned_role_token,
        'user1_user_role_token_disabled': user1_user_role_token_disabled,
        'user1_fedora_admin_role_token': user1_fedora_admin_role_token,
        'foo': MagicMock(api_key='foo'),
        None: MagicMock(api_key=None),
    }.get(token_selector)

    request = deepcopy(assets['test_request_example'])
    request['api_key'] = user2_user_role_token.api_key
    response = internal_client.post('/v0.1/requests', json=request)
    assert response.status_code == 200

    request_id = json.loads(response.content)['id']

    # token in header
    response = internal_client.get(
        '/v0.1/requests/{}'.format(request_id),
        headers={'Authorization': 'Bearer {}'.format(token.api_key)} if token.api_key else {},
    )
    assert response.status_code == expected_status_code

    # token in url
    response = internal_client.get(
        '/v0.1/requests/{}{}'.format(request_id, '?api_key={}'.format(token.api_key) if token.api_key else '')
    )
    assert response.status_code == expected_status_code


def test_access_get_test_request_internal_disabled_token(delete_all_tokens, user1_user_role_token):
    """
    Test access to the get test request endpoint after disabling the token.
    """
    request = deepcopy(assets['test_request_example'])
    request['api_key'] = user1_user_role_token.api_key
    response = internal_client.post('/v0.1/requests', json=request)
    assert response.status_code == 200

    session = SessionLocal()
    session.query(Token).filter(Token.api_key == user1_user_role_token.api_key).update({'enabled': False})
    session.commit()

    request_id = json.loads(response.content)['id']

    # token in header
    response = internal_client.get(
        '/v0.1/requests/{}'.format(request_id),
        headers={'Authorization': 'Bearer {}'.format(user1_user_role_token.api_key)},
    )
    assert response.status_code == 403

    # token in url
    response = internal_client.get('/v0.1/requests/{}?api_key={}'.format(request_id, user1_user_role_token.api_key))
    assert response.status_code == 403


@pytest.mark.parametrize(
    'token_selector, expected_status_code',
    [
        ('user1_user_role_token', 403),
        ('user1_poweruser_role_token', 403),
        ('user1_worker_role_token', 200),
        ('user1_admin_role_token', 200),
        ('user1_banned_role_token', 403),
        ('user1_user_role_token_disabled', 403),
        ('user1_fedora_admin_role_token', 403),
        ('foo', 401),
        (None, 422),
        # TODO: check access between tokens of the same user
    ],
)
def test_access_update_test_request(
    delete_all_tokens,
    user1_user_role_token,
    user1_poweruser_role_token,
    user1_worker_role_token,
    user1_admin_role_token,
    user1_banned_role_token,
    user1_user_role_token_disabled,
    user1_fedora_admin_role_token,
    user2_user_role_token,
    token_selector,
    expected_status_code,
):
    """
    Test access to the update test request endpoint.
    """
    token = {
        'user1_user_role_token': user1_user_role_token,
        'user1_poweruser_role_token': user1_poweruser_role_token,
        'user1_worker_role_token': user1_worker_role_token,
        'user1_admin_role_token': user1_admin_role_token,
        'user1_banned_role_token': user1_banned_role_token,
        'user1_user_role_token_disabled': user1_user_role_token_disabled,
        'user1_fedora_admin_role_token': user1_fedora_admin_role_token,
        'foo': MagicMock(api_key='foo'),
        None: MagicMock(api_key=None),
    }.get(token_selector)

    request = deepcopy(assets['test_request_example'])
    request['api_key'] = user2_user_role_token.api_key
    response = internal_client.post('/v0.1/requests', json=request)
    assert response.status_code == 200

    request_id = json.loads(response.content)['id']

    # token in header
    response = internal_client.put(
        '/v0.1/requests/{}'.format(request_id),
        json={'notes': [{'level': 'info', 'message': 'test'}]},
        headers={'Authorization': 'Bearer {}'.format(token.api_key)} if token.api_key else {},
    )
    assert response.status_code == expected_status_code

    # token in json
    response = internal_client.put(
        '/v0.1/requests/{}'.format(request_id),
        json={'api_key': token.api_key, 'notes': [{'level': 'info', 'message': 'test'}]},
    )
    assert response.status_code == expected_status_code


@pytest.mark.parametrize(
    'token_selector, expected_status_code',
    [
        ('user1_user_role_token', 403),
        ('user1_poweruser_role_token', 200),
        ('user1_worker_role_token', 200),
        ('user1_admin_role_token', 200),
        ('user1_banned_role_token', 403),
        ('user1_user_role_token_disabled', 403),
        ('user1_fedora_admin_role_token', 403),
        ('foo', 401),
        (None, 422),
        # TODO: check access between tokens of the same user
    ],
)
def test_access_delete_test_request(
    delete_all_tokens,
    user1_user_role_token,
    user1_poweruser_role_token,
    user1_worker_role_token,
    user1_admin_role_token,
    user1_banned_role_token,
    user1_user_role_token_disabled,
    user1_fedora_admin_role_token,
    user2_user_role_token,
    token_selector,
    expected_status_code,
):
    """
    Test access to the delete test request endpoint which cancels the request.
    """
    token = {
        'user1_user_role_token': user1_user_role_token,
        'user1_poweruser_role_token': user1_poweruser_role_token,
        'user1_worker_role_token': user1_worker_role_token,
        'user1_admin_role_token': user1_admin_role_token,
        'user1_banned_role_token': user1_banned_role_token,
        'user1_user_role_token_disabled': user1_user_role_token_disabled,
        'user1_fedora_admin_role_token': user1_fedora_admin_role_token,
        'foo': MagicMock(api_key='foo'),
        None: MagicMock(api_key=None),
    }.get(token_selector)

    request = deepcopy(assets['test_request_example'])
    request['api_key'] = user2_user_role_token.api_key

    # 1st request - token in header
    response = internal_client.post('/v0.1/requests', json=request)
    assert response.status_code == 200

    request_id = json.loads(response.content)['id']

    response = public_client.request(
        method='DELETE',
        url='/v0.1/requests/{}'.format(request_id),
        headers={'Authorization': 'Bearer {}'.format(token.api_key)} if token.api_key else {},
    )
    assert response.status_code == expected_status_code

    # 2nd request - token in json
    request['api_key'] = user2_user_role_token.api_key
    response = internal_client.post('/v0.1/requests', json=request)
    assert response.status_code == 200

    request_id = json.loads(response.content)['id']

    response = public_client.request(
        method='DELETE',
        url='/v0.1/requests/{}'.format(request_id),
        json={'api_key': token.api_key},
    )
    assert response.status_code == expected_status_code


@pytest.mark.parametrize(
    'token_selector, expected_status_code',
    [
        ('user1_user_role_token', 200),
        ('user1_poweruser_role_token', 200),
        ('user1_worker_role_token', 200),
        ('user1_admin_role_token', 200),
        ('user1_user_role_token_disabled', 403),
        # TODO: check access between tokens of the same user
    ],
)
def test_access_delete_owned_test_request(
    delete_all_tokens,
    user1_user_role_token,
    user1_poweruser_role_token,
    user1_worker_role_token,
    user1_admin_role_token,
    user1_banned_role_token,
    user1_user_role_token_disabled,
    token_selector,
    expected_status_code,
):
    """
    Deleting owned request should pass for user and stronger roles
    """
    token = {
        'user1_user_role_token': user1_user_role_token,
        'user1_poweruser_role_token': user1_poweruser_role_token,
        'user1_worker_role_token': user1_worker_role_token,
        'user1_admin_role_token': user1_admin_role_token,
        'user1_banned_role_token': user1_banned_role_token,
        'user1_user_role_token_disabled': user1_user_role_token_disabled,
        'foo': MagicMock(api_key='foo', enabled=True),
        None: MagicMock(api_key=None, enabled=True),
    }.get(token_selector)

    request = deepcopy(assets['test_request_example'])
    request['api_key'] = token.api_key

    # 1st request - token in header
    if not token.enabled:
        # Temporarily enable the token - we want to test disable endpoint
        session = SessionLocal()
        session.query(Token).filter(Token.api_key == token.api_key).update({'enabled': True})
        session.commit()

        response = internal_client.post('/v0.1/requests', json=request)

        session.query(Token).filter(Token.api_key == token.api_key).update({'enabled': False})
        session.commit()
        session.close()
    else:
        response = internal_client.post('/v0.1/requests', json=request)
    assert response.status_code == 200

    request_id = json.loads(response.content)['id']

    response = public_client.request(
        method='DELETE',
        url='/v0.1/requests/{}'.format(request_id),
        headers={'Authorization': 'Bearer {}'.format(token.api_key)} if token.api_key else {},
    )
    assert response.status_code == expected_status_code

    # 2nd request - token in json
    if not token.enabled:
        # Temporarily enable the token - we want to test disable endpoint
        session = SessionLocal()
        session.query(Token).filter(Token.api_key == token.api_key).update({'enabled': True})
        session.commit()

        response = internal_client.post('/v0.1/requests', json=request)

        session.query(Token).filter(Token.api_key == token.api_key).update({'enabled': False})
        session.commit()
        session.close()
    else:
        response = internal_client.post('/v0.1/requests', json=request)
    assert response.status_code == 200

    request_id = json.loads(response.content)['id']

    response = public_client.request(
        method='DELETE',
        url='/v0.1/requests/{}'.format(request_id),
        json={'api_key': token.api_key},
    )
    assert response.status_code == expected_status_code


#
# Token operations
#
@pytest.mark.parametrize(
    'auth_token_selector, token_role, token_ranch, expected_status_code',
    [
        # JWTs
        ('jwt_user1_public_redhat', 'user', 'public', 200),
        ('jwt_user1_public_redhat', 'admin', 'public', 403),
        ('jwt_user2_public', 'user', 'public', 200),
        ('jwt_user2_public', 'user', 'redhat', 403),
        ('jwt_user2_public', 'worker', 'public', 403),
        ('jwt_admin1_public_redhat', 'user', 'public', 200),
        ('jwt_admin1_public_redhat', 'worker', 'redhat', 200),
        ('jwt_admin1_public_redhat', 'admin', 'public', 200),
        ('jwt_admin2_public', 'user', 'redhat', 403),
        # API tokens
        ('user1_admin_role_token', 'admin', 'redhat', 200),
        ('user1_admin_role_token_disabled', 'admin', 'redhat', 403),
        ('user1_user_role_token', 'user', 'public', 403),
        ('user1_poweruser_role_token', 'user', 'public', 403),
        ('user1_poweruser_role_token', 'poweruser', 'public', 403),
        ('user1_worker_role_token', 'user', 'public', 403),
        ('user1_banned_role_token', 'user', 'redhat', 403),
        ('user1_fedora_admin_role_token', 'fedora_admin', 'public', 403),
        # Invalid token
        (None, 'user', 'public', 401),
    ],
)
def test_access_create_token(
    delete_all_tokens,
    jwt_user1_public_redhat,
    jwt_user2_public,
    jwt_admin1_public_redhat,
    jwt_admin2_public,
    user1_admin_role_token,
    user1_admin_role_token_disabled,
    user1_user_role_token,
    user1_poweruser_role_token,
    user1_worker_role_token,
    user1_banned_role_token,
    user1_fedora_admin_role_token,
    auth_token_selector,
    token_role,
    token_ranch,
    expected_status_code,
):
    """
    Test access to the create token endpoint.
    """

    auth_token = {
        'jwt_user1_public_redhat': jwt_user1_public_redhat,
        'jwt_user2_public': jwt_user2_public,
        'jwt_admin1_public_redhat': jwt_admin1_public_redhat,
        'jwt_admin2_public': jwt_admin2_public,
        'user1_admin_role_token': user1_admin_role_token.api_key,
        'user1_user_role_token': user1_user_role_token.api_key,
        'user1_poweruser_role_token': user1_poweruser_role_token.api_key,
        'user1_worker_role_token': user1_worker_role_token.api_key,
        'user1_banned_role_token': user1_banned_role_token.api_key,
        'user1_admin_role_token_disabled': user1_admin_role_token_disabled.api_key,
        'user1_fedora_admin_role_token': user1_fedora_admin_role_token.api_key,
    }.get(auth_token_selector)

    response = public_client.post(
        '/v0.1/tokens',
        json={'name': 'token-name', 'role': token_role, 'ranch': token_ranch},
        headers={'Authorization': f'Bearer {auth_token}'},
    )

    assert response.status_code == expected_status_code


@pytest.mark.parametrize(
    'auth_token_selector, get_token_selector, expected_status_code',
    [
        # JWTs
        ('jwt_user1_public_redhat', 'user1_user_role_token', 200),
        ('jwt_user1_public_redhat', 'user1_admin_role_token', 200),
        ('jwt_user1_public_redhat', 'user2_user_role_token', 403),
        ('jwt_admin1_public_redhat', 'user2_user_role_token', 200),
        ('jwt_user1_public_redhat', None, 400),
        # API tokens
        ('user1_admin_role_token', 'user2_admin_role_token', 200),
        ('user1_admin_role_token_disabled', 'user1_admin_role_token_disabled', 404),
        ('user1_user_role_token', 'user1_user_role_token', 200),
        ('user1_poweruser_role_token', 'user1_poweruser_role_token', 200),
        ('user1_user_role_token', 'user2_user_role_token', 403),
        ('user1_poweruser_role_token', 'user2_user_role_token', 403),
        ('user1_worker_role_token', 'user2_user_role_token', 200),
        ('user1_banned_role_token', 'user1_user_role_token', 403),
        ('user1_fedora_admin_role_token', 'user1_user_role_token', 403),
        # Invalid token
        (None, 'user1_user_role_token', 401),
    ],
)
def test_access_get_token(
    delete_all_tokens,
    user1_admin_role_token,
    user1_admin_role_token_disabled,
    user1_worker_role_token,
    user1_banned_role_token,
    user1_user_role_token,
    user1_poweruser_role_token,
    user1_fedora_admin_role_token,
    user2_user_role_token,
    user2_admin_role_token,
    jwt_user1_public_redhat,
    jwt_user2_public,
    jwt_admin1_public_redhat,
    jwt_admin2_public,
    auth_token_selector,
    get_token_selector,
    expected_status_code,
):
    """
    Test access to the get token endpoint.
    """
    tokens = {
        'jwt_user1_public_redhat': jwt_user1_public_redhat,
        'jwt_user2_public': jwt_user2_public,
        'jwt_admin1_public_redhat': jwt_admin1_public_redhat,
        'jwt_admin2_public': jwt_admin2_public,
        'user1_admin_role_token': user1_admin_role_token,
        'user1_admin_role_token_disabled': user1_admin_role_token_disabled,
        'user1_worker_role_token': user1_worker_role_token,
        'user1_banned_role_token': user1_banned_role_token,
        'user1_user_role_token': user1_user_role_token,
        'user1_poweruser_role_token': user1_poweruser_role_token,
        'user2_admin_role_token': user2_admin_role_token,
        'user2_user_role_token': user2_user_role_token,
        'user1_fedora_admin_role_token': user1_fedora_admin_role_token,
        None: MagicMock(id='foo', api_key='foo'),
    }
    auth_token = (
        tokens.get(auth_token_selector).api_key
        if isinstance(tokens.get(auth_token_selector), Token)
        else tokens.get(auth_token_selector)
    )
    get_token = tokens.get(get_token_selector).id

    response = public_client.get(f'/v0.1/tokens/{get_token}', headers={'Authorization': f'Bearer {auth_token}'})
    assert response.status_code == expected_status_code


@pytest.mark.parametrize(
    'auth_token_selector, expected_tokens_selector, expected_status_code',
    [
        # JWTs
        (
            'jwt_user1_public_redhat',
            [
                'user1_admin_role_token',
                'user1_user_role_token',
                'user1_poweruser_role_token',
                'user1_worker_role_token',
                'user1_banned_role_token',
                'user1_fedora_admin_role_token',
            ],
            200,
        ),
        (
            'jwt_admin1_public_redhat',
            [
                'user1_admin_role_token',
                'user1_user_role_token',
                'user1_poweruser_role_token',
                'user1_worker_role_token',
                'user1_banned_role_token',
                'user1_fedora_admin_role_token',
            ],
            200,
        ),
        ('jwt_user2_public', ['user2_user_role_token', 'user2_admin_role_token'], 200),
        # API tokens
        (
            'user1_admin_role_token',
            [
                'user1_admin_role_token',
                'user1_admin_role_token_disabled',
                'user1_user_role_token',
                'user1_poweruser_role_token',
                'user1_worker_role_token',
                'user2_user_role_token',
                'user2_admin_role_token',
                'user1_banned_role_token',
                'user1_fedora_admin_role_token',
            ],
            200,
        ),
        ('user1_admin_role_token_disabled', [], 403),
        ('user1_worker_role_token', [], 403),
        ('user1_user_role_token', [], 403),
        ('user1_poweruser_role_token', [], 403),
        ('user1_banned_role_token', [], 403),
        ('user1_fedora_admin_role_token', [], 403),
        # Invalid token
        (None, [], 401),
    ],
)
def test_access_get_tokens(
    delete_all_tokens,
    user1_admin_role_token,
    user1_admin_role_token_disabled,
    user1_user_role_token,
    user1_poweruser_role_token,
    user1_worker_role_token,
    user1_banned_role_token,
    user1_fedora_admin_role_token,
    user2_user_role_token,
    user2_admin_role_token,
    jwt_user1_public_redhat,
    jwt_admin1_public_redhat,
    jwt_user2_public,
    auth_token_selector,
    expected_tokens_selector,
    expected_status_code,
):
    """
    Test access to the get tokens endpoint.
    """

    auth_token = {
        'jwt_user1_public_redhat': jwt_user1_public_redhat,
        'jwt_user2_public': jwt_user2_public,
        'jwt_admin1_public_redhat': jwt_admin1_public_redhat,
        'user1_admin_role_token': user1_admin_role_token.api_key,
        'user1_admin_role_token_disabled': user1_admin_role_token_disabled.api_key,
        'user1_worker_role_token': user1_worker_role_token.api_key,
        'user1_user_role_token': user1_user_role_token.api_key,
        'user1_poweruser_role_token': user1_poweruser_role_token.api_key,
        'user1_banned_role_token': user1_banned_role_token.api_key,
        'user1_fedora_admin_role_token': user1_fedora_admin_role_token.api_key,
    }.get(auth_token_selector)

    expected_tokens = [
        {
            'user1_banned_role_token': user1_banned_role_token,
            'user1_user_role_token': user1_user_role_token,
            'user1_worker_role_token': user1_worker_role_token,
            'user1_admin_role_token': user1_admin_role_token,
            'user1_admin_role_token_disabled': user1_admin_role_token_disabled,
            'user2_user_role_token': user2_user_role_token,
            'user1_poweruser_role_token': user1_poweruser_role_token,
            'user2_admin_role_token': user2_admin_role_token,
            'user1_fedora_admin_role_token': user1_fedora_admin_role_token,
        }.get(token)
        for token in expected_tokens_selector
    ]

    response = public_client.get('/v0.1/tokens', headers={'Authorization': f'Bearer {auth_token}'})

    assert response.status_code == expected_status_code

    if expected_status_code == 200:
        response_json = json.loads(response.content)
        assert len(response_json) == len(expected_tokens)
        assert {token['id'] for token in response_json} == {token.id for token in expected_tokens}


@pytest.mark.parametrize(
    'auth_token_selector, token_selector, expected_status_code',
    [
        # JWTs
        ('jwt_user1_public_redhat', 'user1_admin_role_token', 200),
        ('jwt_admin1_public_redhat', 'user1_user_role_token', 200),
        ('jwt_user2_public', 'user2_user_role_token', 200),
        # At the moment even admin cannot update other users' tokens
        ('jwt_admin1_public_redhat', 'user2_user_role_token', 403),
        # API tokens
        ('user1_admin_role_token', 'user2_admin_role_token', 200),
        ('user1_admin_role_token_disabled', 'user1_admin_role_token', 403),
        ('user1_worker_role_token', 'user2_user_role_token', 403),
        ('user1_user_role_token', 'user1_user_role_token', 403),
        ('user1_user_role_token', 'user2_user_role_token', 403),
        ('user1_poweruser_role_token', 'user2_user_role_token', 403),
        ('user1_banned_role_token', 'user1_banned_role_token', 403),
        ('user1_fedora_admin_role_token', 'user1_fedora_admin_role_token', 403),
        ('user1_fedora_admin_role_token', 'user2_user_role_token', 403),
        # Invalid tokens
        (None, 'user1_user_role_token', 401),
        ('jwt_user1_public_redhat', None, 400),
    ],
)
def test_access_update_token(
    delete_all_tokens,
    user1_admin_role_token,
    user1_admin_role_token_disabled,
    user1_worker_role_token,
    user1_user_role_token,
    user1_poweruser_role_token,
    user1_banned_role_token,
    user1_fedora_admin_role_token,
    user2_admin_role_token,
    user2_user_role_token,
    jwt_user1_public_redhat,
    jwt_user2_public,
    jwt_admin1_public_redhat,
    auth_token_selector,
    token_selector,
    expected_status_code,
):
    """
    Test access to the update users endpoint.
    """
    auth_token = {
        'jwt_user1_public_redhat': jwt_user1_public_redhat,
        'jwt_user2_public': jwt_user2_public,
        'jwt_admin1_public_redhat': jwt_admin1_public_redhat,
        'user1_admin_role_token': user1_admin_role_token.api_key,
        'user1_admin_role_token_disabled': user1_admin_role_token_disabled.api_key,
        'user1_worker_role_token': user1_worker_role_token.api_key,
        'user1_user_role_token': user1_user_role_token.api_key,
        'user1_poweruser_role_token': user1_poweruser_role_token.api_key,
        'user1_banned_role_token': user1_banned_role_token.api_key,
        'user1_fedora_admin_role_token': user1_fedora_admin_role_token.api_key,
    }.get(auth_token_selector)
    token = {
        'user1_banned_role_token': user1_banned_role_token,
        'user1_user_role_token': user1_user_role_token,
        'user1_poweruser_role_token': user1_poweruser_role_token,
        'user1_admin_role_token': user1_admin_role_token,
        'user2_admin_role_token': user2_admin_role_token,
        'user2_user_role_token': user2_user_role_token,
        'user1_fedora_admin_role_token': user1_fedora_admin_role_token,
        None: MagicMock(id='foo'),
    }.get(token_selector)

    response = public_client.put(
        f'/v0.1/tokens/{token.id}', headers={'Authorization': f'Bearer {auth_token}'}, json={'name': 'renamed-token'}
    )

    assert response.status_code == expected_status_code


@pytest.mark.parametrize(
    'auth_token_selector, token_selector, expected_status_code',
    [
        # JWTs
        ('jwt_user1_public_redhat', 'user1_admin_role_token', 200),
        ('jwt_admin1_public_redhat', 'user1_user_role_token', 200),
        ('jwt_user2_public', 'user2_user_role_token', 200),
        # At the moment even admin cannot update other users' tokens
        ('jwt_admin1_public_redhat', 'user2_user_role_token', 403),
        # API tokens
        ('user1_admin_role_token', 'user2_admin_role_token', 200),
        ('user1_admin_role_token_disabled', 'user1_admin_role_token', 403),
        ('user1_worker_role_token', 'user2_user_role_token', 403),
        ('user1_user_role_token', 'user1_user_role_token', 403),
        ('user1_user_role_token', 'user2_user_role_token', 403),
        ('user1_poweruser_role_token', 'user1_user_role_token', 403),
        ('user1_banned_role_token', 'user1_banned_role_token', 403),
        ('user1_fedora_admin_role_token', 'user1_fedora_admin_role_token', 403),
        ('user1_fedora_admin_role_token', 'user2_user_role_token', 403),
        # Invalid tokens
        (None, 'user1_user_role_token', 401),
        ('jwt_user1_public_redhat', None, 400),
    ],
)
def test_access_delete_token(
    delete_all_tokens,
    user1_admin_role_token,
    user1_admin_role_token_disabled,
    user1_worker_role_token,
    user1_user_role_token,
    user1_poweruser_role_token,
    user1_banned_role_token,
    user1_fedora_admin_role_token,
    user2_admin_role_token,
    user2_user_role_token,
    jwt_user1_public_redhat,
    jwt_user2_public,
    jwt_admin1_public_redhat,
    auth_token_selector,
    token_selector,
    expected_status_code,
):
    """
    Test access to the delete token endpoint.
    """

    auth_token = {
        'jwt_user1_public_redhat': jwt_user1_public_redhat,
        'jwt_user2_public': jwt_user2_public,
        'jwt_admin1_public_redhat': jwt_admin1_public_redhat,
        'user1_admin_role_token': user1_admin_role_token.api_key,
        'user1_admin_role_token_disabled': user1_admin_role_token_disabled.api_key,
        'user1_worker_role_token': user1_worker_role_token.api_key,
        'user1_user_role_token': user1_user_role_token.api_key,
        'user1_poweruser_role_token': user1_poweruser_role_token.api_key,
        'user1_banned_role_token': user1_banned_role_token.api_key,
        'user1_fedora_admin_role_token': user1_fedora_admin_role_token.api_key,
    }.get(auth_token_selector)
    token = {
        'user1_banned_role_token': user1_banned_role_token,
        'user1_user_role_token': user1_user_role_token,
        'user1_poweruser_role_token': user1_poweruser_role_token,
        'user1_admin_role_token': user1_admin_role_token,
        'user2_admin_role_token': user2_admin_role_token,
        'user2_user_role_token': user2_user_role_token,
        'user1_fedora_admin_role_token': user1_fedora_admin_role_token,
        None: MagicMock(id='foo'),
    }.get(token_selector)

    response = public_client.delete(f'/v0.1/tokens/{token.id}', headers={'Authorization': f'Bearer {auth_token}'})

    assert response.status_code == expected_status_code


#
# User operations
#


@pytest.mark.parametrize(
    'token_selector, expected_status_code',
    [
        ('user1_user_role_token', 403),
        ('user1_poweruser_role_token', 403),
        ('user1_worker_role_token', 403),
        ('user1_admin_role_token', 200),
        ('user1_banned_role_token', 403),
        ('user1_user_role_token_disabled', 403),
        ('user1_fedora_admin_role_token', 200),
        ('foo', 401),
        (None, 422),
    ],
)
def test_access_delete_user(
    delete_all_tokens,
    delete_all_users,
    user1_user_role_token,
    user1_poweruser_role_token,
    user1_worker_role_token,
    user1_admin_role_token,
    user1_banned_role_token,
    user1_user_role_token_disabled,
    user1_fedora_admin_role_token,
    user2,
    token_selector,
    expected_status_code,
):
    """
    Test access to the delete test request endpoint which cancels the request.
    """
    token = {
        'user1_user_role_token': user1_user_role_token,
        'user1_poweruser_role_token': user1_user_role_token,
        'user1_worker_role_token': user1_worker_role_token,
        'user1_admin_role_token': user1_admin_role_token,
        'user1_banned_role_token': user1_banned_role_token,
        'user1_user_role_token_disabled': user1_user_role_token_disabled,
        'user1_fedora_admin_role_token': user1_fedora_admin_role_token,
        'foo': MagicMock(api_key='foo'),
        None: MagicMock(api_key=None),
    }.get(token_selector)

    # 1st request - token in header
    response = public_client.request(
        method='DELETE',
        url='/v0.1/users/{}'.format(user2.id),
        headers={'Authorization': 'Bearer {}'.format(token.api_key)} if token.api_key else {},
    )
    assert response.status_code == expected_status_code

    # 2nd request - token in json
    user2 = add_user(auth_id='developer3', auth_method='unit-test', auth_name='user3')

    if token.api_key is not None:
        response = public_client.request(
            method='DELETE',
            url='/v0.1/users/{}?api_key={}'.format(user2.id, token.api_key),
        )
        assert response.status_code == expected_status_code
