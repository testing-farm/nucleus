from unittest.mock import MagicMock

from tft.nucleus.api.core.schemes.compose import Compose, SupportedComposesOut
from tft.nucleus.api.middleware import composes


def test_get_ranch_composes(monkeypatch):
    mock_composes = SupportedComposesOut(composes=[Compose(name="TestCompose-1"), Compose(name="TestCompose-2")])
    monkeypatch.setattr(composes.composes, "get_ranch_composes", MagicMock(return_value=mock_composes))

    result = composes.get_ranch_composes("public")

    assert isinstance(result, SupportedComposesOut)
    assert len(result.composes) == 2
    assert result.composes[0].name == "TestCompose-1"
    assert result.composes[1].name == "TestCompose-2"


def test_get_composes(monkeypatch):
    mock_composes = SupportedComposesOut(composes=[Compose(name="TestCompose-3"), Compose(name="TestCompose-4")])
    monkeypatch.setattr(composes.composes, "get_ranch_composes", MagicMock(return_value=mock_composes))

    result = composes.get_composes()

    assert isinstance(result, SupportedComposesOut)
    assert len(result.composes) == 2
    assert result.composes[0].name == "TestCompose-3"
    assert result.composes[1].name == "TestCompose-4"


def test_get_ranch_composes_calls_crud_function(monkeypatch):
    monkeypatch.setattr(composes.composes, "get_ranch_composes", MagicMock())

    composes.get_ranch_composes("redhat")

    composes.composes.get_ranch_composes.assert_called_once_with("redhat")


def test_get_composes_calls_crud_function_with_public(monkeypatch):
    monkeypatch.setattr(composes.composes, "get_ranch_composes", MagicMock())

    composes.get_composes()

    composes.composes.get_ranch_composes.assert_called_once_with("public")
