import pytest

from tft.nucleus.api.core.database import RepoKeyPair, SessionLocal
from tft.nucleus.api.middleware.secrets import normalize_git_url

from .conftest import internal_client, public_client


def test_public_key_endpoint_not_authentized():
    response = public_client.post(
        '/v0.1/secrets/public-key',
        json={'url': 'https://gitlab.com/testing-farm/nucleus'},
    )
    assert response.status_code == 422
    assert response.json() == {'message': 'API key is required'}


def test_public_key_endpoint_disabled_token(user1_user_role_token_disabled):
    response = public_client.post(
        '/v0.1/secrets/public-key',
        json={'url': 'https://gitlab.com/testing-farm/nucleus'},
        headers={'Authorization': f'Bearer {user1_user_role_token_disabled.api_key}'},
    )
    assert response.status_code == 403
    assert response.json() == {'message': 'Not authorized to perform this action'}


def test_public_key_endpoint_user(user1_user_role_token):
    session = SessionLocal()
    assert len(session.query(RepoKeyPair).all()) == 0

    # Creates a new key pair
    response1 = public_client.post(
        '/v0.1/secrets/public-key',
        json={'url': 'https://gitlab.com/testing-farm/nucleus'},
        headers={'Authorization': f'Bearer {user1_user_role_token.api_key}'},
    )
    assert response1.status_code == 200

    # Calling again fetches the same key pair
    response2 = public_client.post(
        '/v0.1/secrets/public-key',
        json={'url': 'https://gitlab.com/testing-farm/nucleus'},
        headers={'Authorization': f'Bearer {user1_user_role_token.api_key}'},
    )
    assert response2.status_code == 200
    assert response1.text == response2.text
    assert response1.json().startswith('-----BEGIN PUBLIC KEY-----\n')
    assert response1.json().endswith('-----END PUBLIC KEY-----\n')

    session = SessionLocal()
    assert len(session.query(RepoKeyPair).all()) == 1


def test_encrypt_endpoint_not_authentized():
    response = public_client.post(
        '/v0.1/secrets/encrypt',
        json={'url': 'https://gitlab.com/testing-farm/nucleus', 'message': 'top secret'},
    )
    assert response.status_code == 422
    assert response.json() == {'message': 'API key is required'}


def test_encrypt_endpoint_disabled_token(user1_user_role_token_disabled):
    response = public_client.post(
        '/v0.1/secrets/encrypt',
        json={'url': 'https://gitlab.com/testing-farm/nucleus', 'message': 'top secret'},
        headers={'Authorization': f'Bearer {user1_user_role_token_disabled.api_key}'},
    )
    assert response.status_code == 403
    assert response.json() == {'message': 'Not authorized to perform this action'}


def test_decrypt_endpoint_not_authentized():
    response = internal_client.post(
        '/v0.1/secrets/decrypt',
        json={'url': 'https://gitlab.com/testing-farm/nucleus', 'message': 'top secret'},
    )
    assert response.status_code == 422
    assert response.json() == {'message': 'API key is required'}


def test_decrypt_endpoint_disabled_token(user1_admin_role_token_disabled):
    response = internal_client.post(
        '/v0.1/secrets/decrypt',
        json={'url': 'https://gitlab.com/testing-farm/nucleus', 'message': 'encrypted'},
        headers={'Authorization': f'Bearer {user1_admin_role_token_disabled.api_key}'},
    )
    assert response.status_code == 403
    assert response.json() == {'message': 'Not authorized to perform this action'}


def test_decrypt_endpoint_user(user1_user_role_token):
    response = internal_client.post(
        '/v0.1/secrets/decrypt',
        json={'url': 'https://gitlab.com/testing-farm/nucleus', 'message': 'encrypted'},
        headers={'Authorization': f'Bearer {user1_user_role_token.api_key}'},
    )
    assert response.status_code == 403
    assert response.json() == {'message': 'Not authorized to perform this action'}


def test_encrypt_decrypt_endpoints(user1_user_role_token, user2_worker_role_token):
    # Encrypt a message as a user
    response1 = public_client.post(
        '/v0.1/secrets/encrypt',
        json={'url': 'https://gitlab.com/testing-farm/nucleus', 'message': 'top secret'},
        headers={'Authorization': f'Bearer {user1_user_role_token.api_key}'},
    )
    assert response1.status_code == 200

    # Try to decrypt the message as a user
    response2 = internal_client.post(
        '/v0.1/secrets/decrypt',
        json={'url': 'https://gitlab.com/testing-farm/nucleus', 'message': response1.json()},
        headers={'Authorization': f'Bearer {user1_user_role_token.api_key}'},
    )
    assert response2.status_code == 403
    assert response2.json() == {'message': 'Not authorized to perform this action'}

    # Decrypt the message as a worker
    response3 = internal_client.post(
        '/v0.1/secrets/decrypt',
        json={'url': 'https://gitlab.com/testing-farm/nucleus', 'message': response1.json()},
        headers={'Authorization': f'Bearer {user2_worker_role_token.api_key}'},
    )
    assert response3.status_code == 200
    assert response3.json() == 'top secret'


def test_encrypt_decrypt_endpoints_tamper(user1_user_role_token, user2_user_role_token, user2_worker_role_token):
    # Encrypt a message as a user
    response1 = public_client.post(
        '/v0.1/secrets/encrypt',
        json={'url': 'https://gitlab.com/testing-farm/nucleus', 'message': 'top secret'},
        headers={'Authorization': f'Bearer {user1_user_role_token.api_key}'},
    )
    assert response1.status_code == 200
    token_id, encrypted_message = response1.json().split(',', 1)

    # Modify the token - invalid token
    encrypted_message_tampered = f'foo,{encrypted_message}'

    # Try to decrypt the message
    response2 = internal_client.post(
        '/v0.1/secrets/decrypt',
        json={'url': 'https://gitlab.com/testing-farm/nucleus', 'message': encrypted_message_tampered},
        headers={'Authorization': f'Bearer {user2_worker_role_token.api_key}'},
    )
    assert response2.status_code == 400
    assert response2.json() == {'message': 'Token is not a valid UUID string.'}

    # Modify the token - nonexistent token
    encrypted_message_tampered = f'12345678-abcd-abcd-abcd-12345678abcd,{encrypted_message}'

    # Try to decrypt the message, encrypted for `user1_user_role_token`, trying to decrypt as nonexistent token
    response3 = internal_client.post(
        '/v0.1/secrets/decrypt',
        json={'url': 'https://gitlab.com/testing-farm/nucleus', 'message': encrypted_message_tampered},
        headers={'Authorization': f'Bearer {user2_worker_role_token.api_key}'},
    )
    assert response3.status_code == 404

    # Modify the token - different token than encrypted
    encrypted_message_tampered = f'{user2_user_role_token.id},{encrypted_message}'

    # Try to decrypt the message, encrypted for `user1_user_role_token`, trying to decrypt as `user2_user_role_token`.
    # Ensure `user2_user_role_token` has a key pair.
    response4 = public_client.post(
        '/v0.1/secrets/public-key',
        json={'url': 'https://gitlab.com/testing-farm/nucleus'},
        headers={'Authorization': f'Bearer {user2_user_role_token.api_key}'},
    )
    assert response4.status_code == 200

    response5 = internal_client.post(
        '/v0.1/secrets/decrypt',
        json={'url': 'https://gitlab.com/testing-farm/nucleus', 'message': encrypted_message_tampered},
        headers={'Authorization': f'Bearer {user2_worker_role_token.api_key}'},
    )
    assert response5.status_code == 400
    assert response5.json() == {'message': 'Unable to decrypt message.'}

    # Modify the token - invalid base64
    encrypted_message_tampered = f'{token_id},{encrypted_message[1:]}'

    # Try to decrypt the message
    response6 = internal_client.post(
        '/v0.1/secrets/decrypt',
        json={'url': 'https://gitlab.com/testing-farm/nucleus', 'message': encrypted_message_tampered},
        headers={'Authorization': f'Bearer {user2_worker_role_token.api_key}'},
    )
    assert response6.status_code == 400
    assert response6.json() == {'message': 'Encrypted message does not have a valid base64 format.'}


@pytest.mark.parametrize(
    'url, expected',
    [
        ('ssh://git@github.com:testing-farm/nucleus.git', 'https://github.com/testing-farm/nucleus'),
        ('git@gitlab.com:testin.git-farm/cli.git', 'https://gitlab.com/testin.git-farm/cli'),
        ('http://gitssh://lab.com/testinghttp://farm/cli', 'https://gitssh://lab.com/testinghttp://farm/cli'),
    ],
)
def test_normalize_git_url(url, expected):
    assert normalize_git_url(url) == expected
