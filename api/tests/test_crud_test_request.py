from uuid import UUID

import pytest

from tft.nucleus.api.core import errors
from tft.nucleus.api.core.database import Request, RequestStateType, Token
from tft.nucleus.api.core.schemes import test_request
from tft.nucleus.api.crud import test_request as crud_test_request

from .conftest import assets, session


def test_create_test_request(user1_admin_role_token, session):
    test_request_in = test_request.RequestCreateIn(**assets['test_request_example'])

    test_request_in.test.fmf = test_request_in.test.tmt
    test_request_in.test.tmt = None

    test_request_out_tests = test_request.Test(fmf=test_request_in.test.fmf)

    result = crud_test_request.create_test_request(session, test_request_in, user1_admin_role_token)

    assert isinstance(result, test_request.RequestCreateOut)
    assert result.id is not None
    assert result.state == 'new'
    assert result.test == test_request_out_tests
    assert result.environments == test_request_in.environments


def test_get_test_request_db(user1_admin_role_token, session):
    test_request_in = test_request.RequestCreateIn(**assets['test_request_example'])
    created_request = crud_test_request.create_test_request(session, test_request_in, user1_admin_role_token)

    result = crud_test_request.get_test_request_db(session, created_request.id)

    assert isinstance(result, Request)
    assert UUID(result.id) == created_request.id


def test_get_test_request_db_not_found(session):
    with pytest.raises(errors.NoSuchEntityError):
        crud_test_request.get_test_request_db(session, UUID('00000000-0000-0000-0000-000000000000'))


def test_get_test_request(user1_admin_role_token, session):
    test_request_in = test_request.RequestCreateIn(**assets['test_request_example'])
    created_request = crud_test_request.create_test_request(session, test_request_in, user1_admin_role_token)

    result = crud_test_request.get_test_request(session, created_request.id)

    assert isinstance(result, test_request.RequestGetUpdateOut)
    assert result.id == created_request.id
    assert result.state == 'new'


def test_get_test_request_authenticated(user1_admin_role_token, session):
    test_request_in = test_request.RequestCreateIn(**assets['test_request_example'])
    created_request = crud_test_request.create_test_request(session, test_request_in, user1_admin_role_token)

    result = crud_test_request.get_test_request_authenticated(session, created_request.id)

    assert isinstance(result, test_request.RequestGetAuthenticatedOut)
    assert result.id == created_request.id
    assert result.state == 'new'
    assert result.notification == created_request.notification


def test_get_test_requests(user1_admin_role_token, session):
    test_request_in = test_request.RequestCreateIn(**assets['test_request_example'])
    crud_test_request.create_test_request(session, test_request_in, user1_admin_role_token)

    result = crud_test_request.get_test_requests(session, [Request.state == RequestStateType.NEW])

    assert isinstance(result, list)
    assert len(result) > 0
    assert all(isinstance(r, test_request.RequestGetUpdateOut) for r in result)


def test_get_test_requests_token_filter(user1_admin_role_token, session):
    test_request_in = test_request.RequestCreateIn(**assets['test_request_example'])
    crud_test_request.create_test_request(session, test_request_in, user1_admin_role_token)

    result = crud_test_request.get_test_requests(
        session, [Token.ranch == user1_admin_role_token.ranch], join_token=True
    )

    assert isinstance(result, list)
    assert len(result) > 0
    assert all(isinstance(r, test_request.RequestGetUpdateOut) for r in result)


def test_update_test_request(user1_admin_role_token, session):
    test_request_in = test_request.RequestCreateIn(**assets['test_request_example'])
    created_request = crud_test_request.create_test_request(session, test_request_in, user1_admin_role_token)

    update_data = test_request.RequestUpdateIn(notes=[{"level": "info", "message": "Updated notes"}])
    result = crud_test_request.update_test_request(session, created_request.id, update_data)

    assert isinstance(result, test_request.RequestGetUpdateOut)
    assert result.notes[0].message == "Updated notes"


def test_delete_test_request(user1_admin_role_token, session):
    test_request_in = test_request.RequestCreateIn(**assets['test_request_example'])
    created_request = crud_test_request.create_test_request(session, test_request_in, user1_admin_role_token)

    crud_test_request.delete_test_request(session, created_request.id)

    updated_request = crud_test_request.get_test_request(session, created_request.id)
    assert updated_request.state == 'cancel-requested'
