import pkg_resources
import pytest

from tft.nucleus.api.core.database import SessionLocal, User

from .conftest import public_client


def test_delete_user(user1_fedora_admin_role_token, user2):
    """
    Test the delete user endpoint.
    """

    response = public_client.delete(
        f'/v0.1/users/{user2.id}', headers={'Authorization': f'Bearer {user1_fedora_admin_role_token.api_key}'}
    )

    assert response.status_code == 200

    session = SessionLocal()
    user2 = session.query(User).filter(User.id == user2.id).first()

    assert user2.enabled is False
    assert user2.auth_name == f'deleted-user-{user2.id}'
    assert user2.auth_method == 'deleted'
