from unittest.mock import MagicMock

import pytest

from tft.nucleus.api.core.database import SessionLocal, User
from tft.nucleus.api.login import AuthorizedUser, get_authorized_user
from tft.nucleus.api.middleware import login

from .conftest import public_client


@pytest.mark.parametrize(
    'orgs_response, expected_user',
    [
        (
            MagicMock(status_code=200, json=lambda: {'role': 'member'}),
            AuthorizedUser(
                auth_id='123', auth_method='github', auth_name='mocked username', role='user', ranch=['public']
            ),
        ),
        (
            MagicMock(status_code=200, json=lambda: {'role': 'admin'}),
            AuthorizedUser(
                auth_id='123',
                auth_method='github',
                auth_name='mocked username',
                role='admin',
                ranch=['public', 'redhat'],
            ),
        ),
    ],
)
def test_login_github_callback(monkeypatch, delete_all_users, orgs_response, expected_user):
    mock_requests = MagicMock()
    mock_requests.post.side_effect = [
        MagicMock(status_code=200, json=lambda: {'access_token': 'mocked token'})  # github access_token endpoint mock
    ]
    mock_requests.get.side_effect = [
        MagicMock(status_code=200, json=lambda: {'login': 'mocked username', 'id': 123}),  # github user endpoint mock
        orgs_response,  # github orgs endpoint mock
    ]
    monkeypatch.setattr(login, 'requests', mock_requests)

    session = SessionLocal()
    users_db = session.query(User).all()
    assert len(users_db) == 0

    response = public_client.get('/v0.1/login/github/callback?code=123456')
    assert response.status_code == 200
    jwt = response.text

    user_authorized = get_authorized_user(jwt)

    assert user_authorized == expected_user

    session = SessionLocal()
    users_db = session.query(User).all()
    assert len(users_db) == 1
    assert users_db[0].auth_id == '123'
    assert users_db[0].auth_method == 'github'
    assert users_db[0].auth_name == 'mocked username'


@pytest.mark.parametrize(
    'access_token_response, orgs_response, expected_response',
    [
        (
            MagicMock(status_code=200, json=lambda: {}),
            None,
            MagicMock(status_code=500, text='{"message":"Error: Failed to retrieve access token."}'),
        ),
        (
            MagicMock(status_code=200, json=lambda: {'access_token': 'mocked token'}),
            MagicMock(status_code=404),
            MagicMock(status_code=403, text='{"message":"Error: User is not a member of the org-name organization."}'),
        ),
        (
            MagicMock(status_code=200, json=lambda: {'access_token': 'mocked token'}),
            MagicMock(status_code=200, json=lambda: {'role': 'unkown'}),
            MagicMock(
                status_code=403,
                text='{"message":"Error: User does not have expected role in the org-name organization."}',
            ),
        ),
    ],
)
def test_login_github_callback_unauthorized(
    monkeypatch, delete_all_users, access_token_response, orgs_response, expected_response
):
    mock_requests = MagicMock()
    mock_requests.post.side_effect = [access_token_response]
    mock_requests.get.side_effect = [
        MagicMock(status_code=200, json=lambda: {'login': 'mocked username', 'id': 123}),  # github user endpoint mock
        orgs_response,  # github orgs endpoint mock
    ]
    monkeypatch.setattr(login, 'requests', mock_requests)

    response = public_client.get('/v0.1/login/github/callback?code=123456')

    assert response.status_code == expected_response.status_code
    assert response.text == expected_response.text


def test_login_github_callback_multiple(monkeypatch, delete_all_users):
    mock_requests = MagicMock()
    mock_requests.post.side_effect = [
        MagicMock(  # github access_token endpoint mock 1st login
            status_code=200, json=lambda: {'access_token': 'mocked token'}
        ),
        MagicMock(  # github access_token endpoint mock 2nd login
            status_code=200, json=lambda: {'access_token': 'mocked token'}
        ),
    ]
    mock_requests.get.side_effect = [
        MagicMock(  # github user endpoint mock 1st login
            status_code=200, json=lambda: {'login': 'mocked username', 'id': 123}
        ),
        MagicMock(status_code=200, json=lambda: {'role': 'member'}),  # github orgs endpoint mock 1st login
        MagicMock(status_code=200, json=lambda: {'login': 'changed', 'id': 123}),  # github user endpoint mock 2nd login
        MagicMock(status_code=200, json=lambda: {'role': 'member'}),  # github orgs endpoint mock 2nd login
    ]
    monkeypatch.setattr(login, 'requests', mock_requests)

    # Before 1st login - 0 users
    session = SessionLocal()
    users_db = session.query(User).all()
    assert len(users_db) == 0

    # 1st login
    response = public_client.get('/v0.1/login/github/callback?code=123456')
    assert response.status_code == 200
    jwt = response.text

    user_authorized = get_authorized_user(jwt)

    assert user_authorized == AuthorizedUser(
        auth_id='123', auth_method='github', auth_name='mocked username', role='user', ranch=['public']
    )

    session = SessionLocal()
    users_db = session.query(User).all()
    assert len(users_db) == 1
    assert users_db[0].auth_id == '123'
    assert users_db[0].auth_method == 'github'
    assert users_db[0].auth_name == 'mocked username'

    # 2nd login - same user, different username
    response = public_client.get('/v0.1/login/github/callback?code=123456')
    assert response.status_code == 200
    jwt = response.text

    user_authorized = get_authorized_user(jwt)

    assert user_authorized == AuthorizedUser(
        auth_id='123', auth_method='github', auth_name='changed', role='user', ranch=['public']
    )

    session = SessionLocal()
    users_db = session.query(User).all()
    assert len(users_db) == 1
    assert users_db[0].auth_id == '123'
    assert users_db[0].auth_method == 'github'
    assert users_db[0].auth_name == 'changed'


def test_login_fedora_callback(monkeypatch, delete_all_users):
    mock_requests = MagicMock()
    mock_requests.post.side_effect = [
        MagicMock(  # fedora access_token endpoint mock 1st login
            status_code=200, json=lambda: {'access_token': 'mocked token'}
        ),
        MagicMock(  # fedora access_token endpoint mock 2nd login
            status_code=200, json=lambda: {'access_token': 'mocked token'}
        ),
    ]
    mock_requests.get.side_effect = [
        MagicMock(  # fedora userinfo endpoint mock 1st login
            status_code=200, json=lambda: {'sub': 'username', 'groups': ['testing-farm']}
        ),
        MagicMock(  # fedora userinfo endpoint mock 2nd login
            status_code=200, json=lambda: {'sub': 'username', 'groups': ['testing-farm']}
        ),
    ]
    monkeypatch.setattr(login, 'requests', mock_requests)

    # Before 1st login - 0 users
    session = SessionLocal()
    users_db = session.query(User).all()
    assert len(users_db) == 0

    # 1st login
    response = public_client.get('/v0.1/login/fedora/callback?code=123456')
    assert response.status_code == 200
    jwt = response.text

    user_authorized = get_authorized_user(jwt)

    assert user_authorized == AuthorizedUser(
        auth_id='username', auth_method='fedora', auth_name='username', role='user', ranch=['public']
    )

    session = SessionLocal()
    users_db = session.query(User).all()
    assert len(users_db) == 1
    assert users_db[0].auth_id == 'username'
    assert users_db[0].auth_method == 'fedora'
    assert users_db[0].auth_name == 'username'

    # 2nd login - same user with same username
    response = public_client.get('/v0.1/login/fedora/callback?code=123456')
    assert response.status_code == 200
    jwt = response.text

    user_authorized = get_authorized_user(jwt)

    assert user_authorized == AuthorizedUser(
        auth_id='username', auth_method='fedora', auth_name='username', role='user', ranch=['public']
    )

    session = SessionLocal()
    users_db = session.query(User).all()
    assert len(users_db) == 1
    assert users_db[0].auth_id == 'username'
    assert users_db[0].auth_method == 'fedora'
    assert users_db[0].auth_name == 'username'


@pytest.mark.parametrize(
    'access_token_response, userid_response, expected_response',
    [
        (
            MagicMock(status_code=400),
            None,
            MagicMock(status_code=500, text='{"message":"Error: Failed to retrieve access token."}'),
        ),
        (
            MagicMock(status_code=200, json=lambda: {}),
            None,
            MagicMock(status_code=500, text='{"message":"Error: Failed to retrieve access token."}'),
        ),
        (
            MagicMock(status_code=200, json=lambda: {'access_token': 'mocked token'}),
            MagicMock(status_code=200, json=lambda: {'sub': 'username', 'groups': ['nothing']}),
            MagicMock(status_code=403, text='{"message":"Error: User is not a member of the organization."}'),
        ),
    ],
)
def test_login_fedora_callback_unauthorized(
    monkeypatch, delete_all_users, access_token_response, userid_response, expected_response
):
    mock_requests = MagicMock()
    mock_requests.post.side_effect = [access_token_response]
    mock_requests.get.side_effect = [userid_response]
    monkeypatch.setattr(login, 'requests', mock_requests)

    response = public_client.get('/v0.1/login/fedora/callback?code=123456')

    assert response.status_code == expected_response.status_code
    assert response.text == expected_response.text


def test_login_redhat_callback(monkeypatch, delete_all_users):
    mock_requests = MagicMock()
    mock_requests.post.side_effect = [
        MagicMock(  # redhat access_token endpoint mock 1st login
            status_code=200, json=lambda: {'access_token': 'mocked token'}
        ),
        MagicMock(  # redhat access_token endpoint mock 2nd login
            status_code=200, json=lambda: {'access_token': 'mocked token'}
        ),
    ]
    mock_requests.get.side_effect = [
        MagicMock(  # redhat userinfo endpoint mock 1st login
            status_code=200, json=lambda: {'sub': 'userid', 'preferred_username': 'username'}
        ),
        MagicMock(  # redhat userinfo endpoint mock 2nd login
            status_code=200, json=lambda: {'sub': 'userid', 'preferred_username': 'changed'}
        ),
    ]
    monkeypatch.setattr(login, 'requests', mock_requests)

    # Before 1st login - 0 users
    session = SessionLocal()
    users_db = session.query(User).all()
    assert len(users_db) == 0

    # 1st login
    response = public_client.get('/v0.1/login/redhat/callback?code=123456')
    assert response.status_code == 200
    jwt = response.text

    user_authorized = get_authorized_user(jwt)

    assert user_authorized == AuthorizedUser(
        auth_id='userid', auth_method='redhat', auth_name='username', role='user', ranch=['public', 'redhat']
    )

    session = SessionLocal()
    users_db = session.query(User).all()
    assert len(users_db) == 1
    assert users_db[0].auth_id == 'userid'
    assert users_db[0].auth_method == 'redhat'
    assert users_db[0].auth_name == 'username'

    # 2nd login - same user with changed username
    response = public_client.get('/v0.1/login/redhat/callback?code=123456')
    assert response.status_code == 200
    jwt = response.text

    user_authorized = get_authorized_user(jwt)

    assert user_authorized == AuthorizedUser(
        auth_id='userid', auth_method='redhat', auth_name='changed', role='user', ranch=['public', 'redhat']
    )

    session = SessionLocal()
    users_db = session.query(User).all()
    assert len(users_db) == 1
    assert users_db[0].auth_id == 'userid'
    assert users_db[0].auth_method == 'redhat'
    assert users_db[0].auth_name == 'changed'


@pytest.mark.parametrize(
    'access_token_response, userid_response, expected_response',
    [
        (
            MagicMock(status_code=400),
            None,
            MagicMock(status_code=500, text='{"message":"Error: Failed to retrieve access token."}'),
        ),
        (
            MagicMock(status_code=200, json=lambda: {}),
            None,
            MagicMock(status_code=500, text='{"message":"Error: Failed to retrieve access token."}'),
        ),
        (
            MagicMock(status_code=200, json=lambda: {'access_token': 'mocked token'}),
            MagicMock(status_code=400),
            MagicMock(
                status_code=500,
                text='{"message":"Error: Failed to retrieve data from https://auth.stage.redhat.com/auth/realms/EmployeeIDP/protocol/openid-connect/userinfo."}',
            ),
        ),
    ],
)
def test_login_redhat_callback_unauthorized(
    monkeypatch, delete_all_users, access_token_response, userid_response, expected_response
):
    mock_requests = MagicMock()
    mock_requests.post.side_effect = [access_token_response]
    mock_requests.get.side_effect = [userid_response]
    monkeypatch.setattr(login, 'requests', mock_requests)

    response = public_client.get('/v0.1/login/redhat/callback?code=123456')

    assert response.status_code == expected_response.status_code
    assert response.text == expected_response.text
