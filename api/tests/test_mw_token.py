from unittest.mock import MagicMock, call
from uuid import UUID

import pytest

from tft.nucleus.api.core.schemes import token
from tft.nucleus.api.login import AuthorizedUser
from tft.nucleus.api.middleware import token as mw_token


@pytest.fixture
def access_logger(monkeypatch):
    access_log_mock = MagicMock()
    access_log_mock.info = MagicMock()
    monkeypatch.setattr(mw_token, 'access_logger', access_log_mock)
    return access_log_mock


def test_create_token(monkeypatch, session, fastapi_request, access_logger, user1):
    token_in = token.TokenCreateIn(name="test_token", role="user", ranch="public")
    auth = AuthorizedUser(
        auth_id=user1.auth_id,
        auth_method=user1.auth_method,
        auth_name=user1.auth_name,
        role="user",
        ranch=["public", "redhat"],
    )

    mock_crud_create_token = MagicMock(
        return_value=token.TokenCreateOut(
            id=UUID('11111111-1111-1111-1111-111111111111'),
            user_id='11111111-1111-1111-1111-111111111111',
            api_key='11111111-1111-1111-1111-111111111111',
            name="test_token",
            role="user",
            ranch="public",
            enabled=True,
            created="2023-05-01T00:00:00",
            updated="2023-05-01T00:00:00",
        )
    )
    monkeypatch.setattr(mw_token.crud_token, "create_token", mock_crud_create_token)

    result = mw_token.create_token(fastapi_request, session, token_in, auth)

    assert isinstance(result, token.TokenCreateOut)
    assert result.name == "test_token"
    assert result.role == "user"
    assert result.ranch == "public"
    mock_crud_create_token.assert_called_once()
    assert access_logger.info.call_args_list == [
        call(
            'Access attempt',
            extra={
                'client_ip': 'localhost',
                'user_id': user1.id,
                'user_name': 'user1',
                'what': 'access_control',
                'role': None,
                'object': 'token',
                'action': 'create',
                'access': True,
            },
        )
    ]


def test_get_token(monkeypatch, session, fastapi_request, access_logger, user1):
    token_id = UUID('11111111-1111-1111-1111-111111111111')
    auth = AuthorizedUser(
        auth_id=user1.auth_id,
        auth_method=user1.auth_method,
        auth_name=user1.auth_name,
        role="user",
        ranch=["public", "redhat"],
    )

    mock_crud_get_token = MagicMock(
        return_value=token.TokenGetUpdateOut(
            id=token_id,
            user_id=user1.id,
            name="test_token",
            role="user",
            ranch="public",
            enabled=True,
            created="2023-05-01T00:00:00",
            updated="2023-05-01T00:00:00",
        )
    )
    monkeypatch.setattr(mw_token.crud_token, "get_token", mock_crud_get_token)

    result = mw_token.get_token(fastapi_request, session, str(token_id), auth)

    assert isinstance(result, token.TokenGetUpdateOut)
    assert result.id == token_id
    assert result.name == "test_token"
    assert result.role == "user"
    assert result.ranch == "public"
    mock_crud_get_token.assert_called_once_with(session, token_id=token_id, allow_disabled=False)
    assert access_logger.info.call_args_list == [
        call(
            'Access attempt',
            extra={
                'client_ip': 'localhost',
                'object_id': '11111111-1111-1111-1111-111111111111',
                'user_id': user1.id,
                'user_name': 'user1',
                'what': 'access_control',
                'role': None,
                'object': 'token_self',
                'action': 'get',
                'access': True,
            },
        )
    ]


def test_get_tokens(monkeypatch, session, fastapi_request, access_logger, user1):
    auth = AuthorizedUser(
        auth_id=user1.auth_id,
        auth_method=user1.auth_method,
        auth_name=user1.auth_name,
        role="user",
        ranch=["public", "redhat"],
    )

    mock_crud_get_tokens = MagicMock(
        return_value=[
            token.TokenGetUpdateOut(
                id=UUID('11111111-1111-1111-1111-111111111111'),
                user_id='11111111-1111-1111-1111-111111111111',
                name="test_token1",
                role="user",
                ranch="public",
                enabled=True,
                created="2023-05-01T00:00:00",
                updated="2023-05-01T00:00:00",
            ),
            token.TokenGetUpdateOut(
                id=UUID('22222222-2222-2222-2222-222222222222'),
                user_id='11111111-1111-1111-1111-111111111111',
                name="test_token2",
                role="user",
                ranch="redhat",
                enabled=True,
                created="2023-05-02T00:00:00",
                updated="2023-05-02T00:00:00",
            ),
        ]
    )
    monkeypatch.setattr(mw_token.crud_token, "get_tokens", mock_crud_get_tokens)

    result = mw_token.get_tokens(fastapi_request, session, auth)

    assert isinstance(result, list)
    assert len(result) == 2
    assert all(isinstance(item, token.TokenGetUpdateOut) for item in result)
    mock_crud_get_tokens.assert_called_once()
    assert access_logger.info.call_args_list == [
        call(
            'Access attempt',
            extra={
                'client_ip': 'localhost',
                'user_id': user1.id,
                'user_name': 'user1',
                'what': 'access_control',
                'role': None,
                'object': 'tokens_list',
                'action': 'get',
                'access': True,
            },
        )
    ]


def test_update_token(monkeypatch, session, fastapi_request, access_logger, user1):
    token_id = UUID('11111111-1111-1111-1111-111111111111')
    token_update = token.TokenUpdateIn(name="updated_token")
    auth = AuthorizedUser(
        auth_id=user1.auth_id,
        auth_method=user1.auth_method,
        auth_name=user1.auth_name,
        role="user",
        ranch=["public", "redhat"],
    )

    mock_crud_update_token = MagicMock(
        return_value=token.TokenGetUpdateOut(
            id=token_id,
            user_id='11111111-1111-1111-1111-111111111111',
            name="updated_token",
            role="admin",
            ranch="redhat",
            enabled=True,
            created="2023-05-01T00:00:00",
            updated="2023-05-03T00:00:00",
        )
    )
    monkeypatch.setattr(mw_token.crud_token, "update_token", mock_crud_update_token)

    mock_crud_get_token_db = MagicMock(return_value=MagicMock(user_id=user1.id))
    monkeypatch.setattr(mw_token.crud_token, "get_token_db", mock_crud_get_token_db)

    result = mw_token.update_token(fastapi_request, session, str(token_id), token_update, auth)

    assert isinstance(result, token.TokenGetUpdateOut)
    assert result.id == token_id
    assert result.name == "updated_token"
    mock_crud_update_token.assert_called_once_with(session, token_id, token_update)
    assert access_logger.info.call_args_list == [
        call(
            'Access attempt',
            extra={
                'client_ip': 'localhost',
                'object_id': '11111111-1111-1111-1111-111111111111',
                'user_id': user1.id,
                'user_name': 'user1',
                'what': 'access_control',
                'role': None,
                'object': 'token_self',
                'action': 'update',
                'access': True,
            },
        )
    ]


def test_disable_token(monkeypatch, session, fastapi_request, access_logger, user1):
    token_id = UUID('11111111-1111-1111-1111-111111111111')
    auth = AuthorizedUser(
        auth_id=user1.auth_id,
        auth_method=user1.auth_method,
        auth_name=user1.auth_name,
        role="user",
        ranch=["public", "redhat"],
    )

    mock_crud_disable_token = MagicMock(return_value=True)
    monkeypatch.setattr(mw_token.crud_token, "disable_token", mock_crud_disable_token)

    mock_crud_get_token_db = MagicMock(return_value=MagicMock(user_id=user1.id))
    monkeypatch.setattr(mw_token.crud_token, "get_token_db", mock_crud_get_token_db)

    result = mw_token.disable_token(fastapi_request, session, str(token_id), auth)

    assert result == True
    mock_crud_disable_token.assert_called_once_with(session, token_id)
    assert access_logger.info.call_args_list == [
        call(
            'Access attempt',
            extra={
                'client_ip': 'localhost',
                'object_id': '11111111-1111-1111-1111-111111111111',
                'user_id': user1.id,
                'user_name': 'user1',
                'what': 'access_control',
                'role': None,
                'object': 'token_self',
                'action': 'delete',
                'access': True,
            },
        )
    ]
