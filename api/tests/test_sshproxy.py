import pytest

from tft.nucleus.api.config import settings

from .conftest import public_client


def test_get_sshproxy_public(
    user1,
    user1_user_role_token,
):
    response = public_client.get(f"/v0.1/sshproxy?api_key={user1_user_role_token.api_key}")

    assert response.status_code == 200
    assert response.json() == {
        'ssh_proxy': settings.get('SSH_PROXY', {})['PUBLIC'],
        'ssh_private_key_base_64': settings.get('SSH_PRIVATE_KEY_BASE_64', {})['PUBLIC'],
    }


def test_get_sshproxy_redhat(user1, user1_user_role_token_redhat):
    response = public_client.get(f"/v0.1/sshproxy?api_key={user1_user_role_token_redhat.api_key}")

    assert response.status_code == 200
    assert response.json() == {
        'ssh_proxy': settings.get('SSH_PROXY', {})['REDHAT'],
        'ssh_private_key_base_64': settings.get('SSH_PRIVATE_KEY_BASE_64', {})['REDHAT'],
    }


def test_get_sshproxy_disabled_token(user1, user1_user_role_token_disabled):
    response = public_client.get(f"/v0.1/sshproxy?api_key={user1_user_role_token_disabled.api_key}")

    assert response.status_code == 403


def test_get_sshproxy_invalid_api_key():
    response = public_client.get(f"/v0.1/sshproxy?api_key=bad_key")

    assert response.status_code == 401
