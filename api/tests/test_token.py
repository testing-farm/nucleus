import json
from copy import deepcopy
from uuid import UUID

from .conftest import assets, public_client


def test_create_token(jwt_user1_public_redhat):
    """
    Test the create token endpoint.
    """
    response = public_client.post(
        '/v0.1/tokens',
        json=assets['create_token_example'],
        headers={'Authorization': f'Bearer {jwt_user1_public_redhat}'},
    )
    content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['create_token_response_example'].copy()
    expected_response['id'] = content['id']
    expected_response['created'] = content['created']
    expected_response['updated'] = content['updated']
    expected_response['user_id'] = content['user_id']
    expected_response['api_key'] = content['api_key']
    UUID(content['api_key'], version=4)

    assert content == expected_response


def test_get_token(jwt_user1_public_redhat):
    """
    Test the get token endpoint.
    """
    response = public_client.post(
        '/v0.1/tokens',
        json=assets['create_token_example'],
        headers={'Authorization': f'Bearer {jwt_user1_public_redhat}'},
    )
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    response = public_client.get(
        '/v0.1/tokens/{}'.format(create_response_content['id']),
        headers={'Authorization': f'Bearer {jwt_user1_public_redhat}'},
    )
    content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['create_token_response_example'].copy()
    expected_response['id'] = create_response_content['id']
    expected_response['created'] = create_response_content['created']
    expected_response['updated'] = create_response_content['updated']
    expected_response['user_id'] = content['user_id']
    assert content == expected_response


def test_get_tokens(delete_all_tokens, user1_admin_role_token, jwt_user1_public_redhat):
    """
    Test the get tokens endpoint.
    """
    expected_responses = []

    # First of all, get expected response for already created token
    # and in meanwhile test get_tokens with only one token
    response = public_client.get('/v0.1/tokens', headers={'Authorization': f'Bearer {jwt_user1_public_redhat}'})
    content = json.loads(response.content)

    assert response.status_code == 200

    expected_responses.append(content[0])

    for _ in range(3):
        # Create 3 tokens, one is already created by the fixture
        request = deepcopy(assets['create_token_example'])
        response = public_client.post(
            '/v0.1/tokens', json=request, headers={'Authorization': f'Bearer {jwt_user1_public_redhat}'}
        )
        content = json.loads(response.content)

        assert response.status_code == 200

        expected_response = assets['create_token_response_example'].copy()
        expected_response['id'] = content['id']
        expected_response['created'] = content['created']
        expected_response['updated'] = content['updated']
        expected_response['user_id'] = content['user_id']
        expected_responses.append(expected_response)
        assert content == {**expected_response, 'api_key': content['api_key']}

    response = public_client.get('/v0.1/tokens', headers={'Authorization': f'Bearer {jwt_user1_public_redhat}'})
    content = json.loads(response.content)

    assert response.status_code == 200

    assert sorted(content, key=lambda x: x['id']) == sorted(expected_responses, key=lambda x: x['id'])


def test_update_token(user1, jwt_user1_public_redhat):
    """
    Test the update token endpoint.
    """
    response = public_client.post(
        '/v0.1/tokens',
        json=assets['create_token_example'],
        headers={'Authorization': f'Bearer {jwt_user1_public_redhat}'},
    )
    create_response_content = json.loads(response.content)
    assert response.status_code == 200

    response = public_client.put(
        '/v0.1/tokens/{}'.format(create_response_content['id']),
        json=assets['update_token_example'],
        headers={'Authorization': f'Bearer {jwt_user1_public_redhat}'},
    )
    content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['update_token_response_example'].copy()
    expected_response['id'] = content['id']
    expected_response['created'] = content['created']
    expected_response['updated'] = content['updated']
    expected_response['user_id'] = content['user_id']
    assert content == expected_response


def test_delete_token(delete_all_tokens, user1, jwt_user1_public_redhat):
    """
    Test the delete token endpoint.
    """
    response = public_client.post(
        '/v0.1/tokens',
        json=assets['create_token_example'],
        headers={'Authorization': f'Bearer {jwt_user1_public_redhat}'},
    )
    create_response_content = json.loads(response.content)
    print(create_response_content)

    assert response.status_code == 200

    response = public_client.delete(
        '/v0.1/tokens/{}'.format(create_response_content['id']),
        headers={'Authorization': f'Bearer {jwt_user1_public_redhat}'},
    )

    assert response.status_code == 200

    response = public_client.get(
        '/v0.1/tokens/{}'.format(create_response_content['id']),
        headers={'Authorization': f'Bearer {jwt_user1_public_redhat}'},
    )
    content = json.loads(response.content)

    assert response.status_code == 404

    assert content['message'] == 'No such entity'
