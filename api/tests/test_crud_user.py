from uuid import UUID

import pytest

from tft.nucleus.api.core import errors
from tft.nucleus.api.core.database import User
from tft.nucleus.api.core.schemes import user
from tft.nucleus.api.crud import user as crud_user


def test_create_user(session):
    result = crud_user.create_user(session, "test_auth_id", "test_auth_method", "Test User")

    assert isinstance(result, User)
    assert result.auth_id == "test_auth_id"
    assert result.auth_method == "test_auth_method"
    assert result.auth_name == "Test User"
    assert result.enabled == True


def test_get_user_by_auth_id_and_auth_method(session, user1):
    result = crud_user.get_user_by_auth_id_and_auth_method(session, user1.auth_id, user1.auth_method)

    assert isinstance(result, User)
    assert result.id == user1.id
    assert result.auth_id == user1.auth_id
    assert result.auth_method == user1.auth_method


def test_get_user_by_auth_id_and_auth_method_not_found(session):
    with pytest.raises(errors.NoSuchEntityError):
        crud_user.get_user_by_auth_id_and_auth_method(session, "non_existent_id", "non_existent_method")


def test_get_user_by_id(session, user1):
    result = crud_user.get_user_by_id(session, str(user1.id))

    assert isinstance(result, User)
    assert result.id == user1.id


def test_get_user_by_id_not_found(session):
    with pytest.raises(errors.NoSuchEntityError):
        crud_user.get_user_by_id(session, "00000000-0000-0000-0000-000000000000")


def test_update_user(session, user1):
    new_auth_name = "Updated User Name"
    result = crud_user.update_user(session, user1.auth_id, user1.auth_method, new_auth_name)

    assert isinstance(result, User)
    assert result.auth_name == new_auth_name


def test_update_user_not_found(session):
    with pytest.raises(errors.NoSuchEntityError):
        crud_user.update_user(session, "non_existent_id", "non_existent_method", "New Name")


def test_disable_user(session, user1):
    result = crud_user.disable_user(session, str(user1.id))

    assert isinstance(result, user.UserGetUpdateOut)
    assert result.enabled == False
    assert result.auth_name.startswith("deleted-user-")
    assert result.auth_method == "deleted"


def test_disable_user_not_found(session):
    with pytest.raises(errors.NoSuchEntityError):
        crud_user.disable_user(session, "00000000-0000-0000-0000-000000000000")


def test_user_to_user_get_update_out(user1):
    result = crud_user._user_to_user_get_update_out(user1)

    assert isinstance(result, user.UserGetUpdateOut)
    assert str(result.id) == user1.id
    assert result.auth_id == user1.auth_id
    assert result.auth_method == user1.auth_method
    assert result.auth_name == user1.auth_name
    assert result.enabled == user1.enabled
