# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of users Create, and Update database operations.
"""
from uuid import uuid4

from sqlalchemy.orm import Session

from tft.nucleus.api.core import errors
from tft.nucleus.api.core.database import User
from tft.nucleus.api.core.schemes import user


def _user_to_user_get_update_out(user_db: User) -> user.UserGetUpdateOut:
    """
    Convert user database object to response schema.
    """
    return user.UserGetUpdateOut(
        id=user_db.id,
        auth_id=user_db.auth_id,
        auth_method=user_db.auth_method,
        auth_name=user_db.auth_name,
        enabled=user_db.enabled,
        created=user_db.created,
        updated=user_db.updated,
    )


def create_user(session: Session, auth_id: str, auth_method: str, auth_name: str) -> User:
    """
    Create a user in database.
    """

    user_db = User(id=str(uuid4()), auth_id=auth_id, auth_method=auth_method, auth_name=auth_name, enabled=True)
    session.add(user_db)
    session.commit()
    session.refresh(user_db)
    return user_db


def get_user_by_auth_id_and_auth_method(session: Session, auth_id: str, auth_method: str) -> User:
    """
    Retrieve user from database by name and auth method.
    """
    user_db = session.query(User).filter(User.auth_id == auth_id, User.auth_method == auth_method).first()
    if not user_db:
        raise errors.NoSuchEntityError()
    return user_db


def get_user_by_id(session: Session, user_id: str) -> User:
    """
    Retrieve user from database id.
    """
    user_db = session.query(User).filter(User.id == user_id).first()
    if not user_db:
        raise errors.NoSuchEntityError()
    return user_db


def update_user(session: Session, auth_id: str, auth_method: str, new_auth_name: str) -> User:
    """
    Update a user in database.
    """

    updated_user_db = (
        session.query(User)
        .filter(User.auth_id == auth_id, User.auth_method == auth_method)
        .update({'auth_name': new_auth_name})
    )

    if not updated_user_db:
        raise errors.NoSuchEntityError()

    session.commit()

    return get_user_by_auth_id_and_auth_method(session, auth_id, auth_method)


def disable_user(session: Session, user_id: str) -> user.UserGetUpdateOut:
    """
    Disable and anonymize a user in database.
    """

    updated_user_db = (
        session.query(User)
        .filter(User.id == user_id)
        .update(
            {
                'enabled': False,
                'auth_name': f'deleted-user-{user_id}',
                'auth_method': 'deleted',
            }
        )
    )

    if not updated_user_db:
        raise errors.NoSuchEntityError()

    session.commit()

    user_db = session.query(User).filter(User.id == user_id).first()
    assert user_db is not None

    return _user_to_user_get_update_out(user_db)
