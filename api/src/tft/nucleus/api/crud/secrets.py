# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0

from typing import NamedTuple

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from dynaconf import settings
from sqlalchemy.orm import Session

from tft.nucleus.api.core import errors
from tft.nucleus.api.core.database import RepoKeyPair

RSA_PUBLIC_EXPONENT = 65537


class RSAKeyPair(NamedTuple):
    public_key: str
    private_key: str


def _generate_rsa_key_pair() -> RSAKeyPair:
    """
    Generate a RSA public/private key pair in PEM representation.
    """
    private_key = rsa.generate_private_key(
        public_exponent=RSA_PUBLIC_EXPONENT,
        key_size=settings.RSA_KEY_SIZE,
        backend=default_backend(),  # type: ignore[no-untyped-call]
    )

    pem_private_key = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption(),
    )

    pem_public_key = private_key.public_key().public_bytes(
        encoding=serialization.Encoding.PEM, format=serialization.PublicFormat.SubjectPublicKeyInfo
    )

    return RSAKeyPair(pem_public_key.decode(), pem_private_key.decode())


def create_key_pair(session: Session, url: str, token_id: str) -> RepoKeyPair:
    """
    Creates a RepoKeyPair entry in the DB.
    """
    key_pair = _generate_rsa_key_pair()
    repo = RepoKeyPair(url=url, token_id=token_id, public_key=key_pair.public_key, private_key=key_pair.private_key)
    session.add(repo)
    session.commit()
    session.refresh(repo)
    return repo


def get_repo_key_pair_db(session: Session, url: str, token_id: str) -> RepoKeyPair:
    """
    Retrieve public/private key pair for a particular repository url.
    """
    repo = session.query(RepoKeyPair).filter(RepoKeyPair.url == url, RepoKeyPair.token_id == token_id).first()

    if not repo:
        raise errors.NoSuchEntityError()
    return repo
