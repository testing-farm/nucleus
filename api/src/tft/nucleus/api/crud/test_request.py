# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of test requests Create, Read, Update, and Delete database operations.
"""
from typing import Any, Dict, List, Optional
from uuid import UUID, uuid4

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from tft.nucleus.api.core import errors
from tft.nucleus.api.core.database import Request, RequestStateType, Token
from tft.nucleus.api.core.schemes import test_request


def _request_to_request_get_update_out(request: Request) -> test_request.RequestGetUpdateOut:
    """
    Transform test request database model to RequestGetUpdateOut response schema.
    """
    request_out = test_request.RequestGetUpdateOut(
        id=request.id,
        # NOTE: user_id is the same as token_id, it is preserved for backwards compatibility
        user_id=request.token_id,
        token_id=request.token_id,
        test=request.test,
        # TODO: change once database migrated to enums with string values
        state=str(request.state),
        environments_requested=request.environments_requested,
        notes=request.notes,
        result=request.result,
        run=request.run,
        settings=request.settings,
        user=test_request.RequestUser(webpage=None),
        queued_time=request.queued_time,
        run_time=request.run_time,
        created=request.created,
        updated=request.updated,
    )
    if request.user_webpage_url and request_out.user:
        request_out.user.webpage = test_request.RequestUserWebpage(
            url=request.user_webpage_url,
            icon=request.user_webpage_icon,
            name=request.user_webpage_name,
        )
    return request_out


def create_test_request(
    session: Session,
    test_request_in: test_request.RequestCreateIn,
    auth_token: Token,
) -> test_request.RequestCreateOut:
    """
    Create test request in database.
    """
    request_id = str(uuid4())
    db_test_request = Request(
        id=request_id,
        token_id=str(auth_token.id),
        test=jsonable_encoder(test_request_in.test),
        environments_requested=jsonable_encoder(test_request_in.environments),
        notification=jsonable_encoder(test_request_in.notification),
        settings=jsonable_encoder(test_request_in.settings),
    )
    if test_request_in.user and test_request_in.user.webpage:
        db_test_request.user_webpage_url = test_request_in.user.webpage.url
        db_test_request.user_webpage_icon = test_request_in.user.webpage.icon
        db_test_request.user_webpage_name = test_request_in.user.webpage.name

    session.add(db_test_request)
    session.commit()
    session.refresh(db_test_request)
    request_out = test_request.RequestCreateOut(
        id=request_id,
        test=test_request_in.test,
        notification=test_request_in.notification,
        environments=test_request_in.environments,
        settings=test_request_in.settings,
        user=test_request_in.user,
        created=db_test_request.created,
        updated=db_test_request.updated,
        # TODO: change once database migrated to enums with string values
        # state=db_test_request.state,
        state=str(db_test_request.state),
    )
    return request_out


def get_test_request_db(
    session: Session,
    request_id: UUID,
) -> Request:
    """
    Get test request from database.
    """
    test_request_db = session.query(Request).filter(Request.id == str(request_id)).first()
    if not test_request_db:
        raise errors.NoSuchEntityError()
    return test_request_db


def get_test_request(
    session: Session,
    request_id: UUID,
) -> test_request.RequestGetUpdateOut:
    """
    Get test request from database.
    """
    return _request_to_request_get_update_out(get_test_request_db(session, request_id))


def get_test_request_authenticated(
    session: Session,
    request_id: UUID,
) -> test_request.RequestGetAuthenticatedOut:
    """
    Get test request from database with authentication.
    """
    test_request_db = get_test_request_db(session, request_id)

    request_out = test_request.RequestGetAuthenticatedOut(
        id=test_request_db.id,
        # NOTE: user_id is the same as token_id, it is preserved for backwards compatibility
        token_id=test_request_db.token_id,
        user_id=test_request_db.token_id,
        test=test_request_db.test,
        # TODO: change once database migrated to enums with string values
        state=str(test_request_db.state),
        environments_requested=test_request_db.environments_requested,
        notes=test_request_db.notes,
        result=test_request_db.result,
        run=test_request_db.run,
        settings=test_request_db.settings,
        user=test_request.RequestUser(webpage=None),
        queued_time=test_request_db.queued_time,
        run_time=test_request_db.run_time,
        created=test_request_db.created,
        updated=test_request_db.updated,
        notification=test_request_db.notification,
    )
    if test_request_db.user_webpage_url and request_out.user:
        request_out.user.webpage = test_request.RequestUserWebpage(
            url=test_request_db.user_webpage_url,
            icon=test_request_db.user_webpage_icon,
            name=test_request_db.user_webpage_name,
        )

    return request_out


def get_test_requests(
    session: Session, filters: List[Any], join_token: bool = False
) -> List[test_request.RequestGetUpdateOut]:
    """
    Get test requests from database.
    """
    query = session.query(Request)

    if join_token:
        query = query.join(Token)  # type: ignore

    for request_filter in filters:
        query = query.filter(request_filter)

    test_requests_db = query.all()

    return [_request_to_request_get_update_out(request) for request in test_requests_db]


def update_test_request(
    session: Session,
    request_id: UUID,
    test_request_in: test_request.RequestUpdateIn,
    internal_fields: Optional[Dict[str, Any]] = None,
) -> test_request.RequestGetUpdateOut:
    """
    Update test request in database.
    """
    test_request_db = get_test_request_db(session, request_id)

    update_dict = {
        'notes': jsonable_encoder(test_request_in.notes) or test_request_db.notes,
        'environments_requested': jsonable_encoder(test_request_in.environments_requested)
        or test_request_db.environments_requested,
        'result': jsonable_encoder(test_request_in.result) or test_request_db.result,
        'run': jsonable_encoder(test_request_in.run) or test_request_db.run,
        'notification': jsonable_encoder(test_request_in.notification) or test_request_db.notification,
    }

    update_dict.update(internal_fields or {})

    updated_test_request_db = session.query(Request).filter(Request.id == str(request_id)).update(update_dict)

    if not updated_test_request_db:
        raise errors.NoSuchEntityError()

    session.commit()

    return get_test_request(session, request_id)


def delete_test_request(
    session: Session,
    request_id: UUID,
) -> None:
    """
    Cancel test request in database.
    """
    session.query(Request).filter(Request.id == str(request_id)).update({'state': RequestStateType.CANCEL_REQUESTED})
    session.commit()
