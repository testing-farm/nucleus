# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of tokens Create, Read, Update, and Delete database operations.
"""
from typing import Any, List, Optional
from uuid import UUID, uuid4

from sqlalchemy.orm import Session

from tft.nucleus.api.core import errors
from tft.nucleus.api.core.database import DEFAULT_TOKEN_ROLE, Token
from tft.nucleus.api.core.schemes import token


def _token_to_token_get_update_out(token_db: Token) -> token.TokenGetUpdateOut:
    """
    Transform token database model to TokenGetUpdateOut response schema.
    """
    token_out = token.TokenGetUpdateOut(
        id=token_db.id,
        user_id=token_db.user_id,
        name=token_db.name,
        enabled=token_db.enabled,
        ranch=token_db.ranch,
        role=token_db.role,
        created=token_db.created,
        updated=token_db.updated,
    )

    return token_out


def create_token(session: Session, token_in: token.TokenCreateIn, user_id: UUID) -> token.TokenCreateOut:
    """
    Create a token in database and transform it to response schema.
    """
    token_db = Token(
        id=str(uuid4()),
        user_id=str(user_id),
        name=token_in.name,
        api_key=str(uuid4()),
        enabled=True,
        role=token_in.role or DEFAULT_TOKEN_ROLE,
        ranch=token_in.ranch,
    )

    session.add(token_db)
    session.commit()
    session.refresh(token_db)

    token_out = token.TokenCreateOut(
        id=token_db.id,
        user_id=token_db.user_id,
        name=token_db.name,
        api_key=token_db.api_key,
        enabled=token_db.enabled,
        ranch=token_db.ranch,
        role=token_db.role,
        created=token_db.created,
        updated=token_db.updated,
    )
    return token_out


def get_token_db(
    session: Session, api_key: Optional[str] = None, token_id: Optional[UUID] = None, allow_disabled: bool = True
) -> 'Token':
    """
    Retrieve token from database by api key or token id.
    """
    query = session.query(Token)

    if api_key:
        token_db = query.filter(Token.api_key == api_key).first()
    elif token_id:
        token_db = query.filter(Token.id == str(token_id)).first()
    else:
        raise errors.BadRequestError(message='Either api_key or token_id must be provided.')

    if not token_db or (not allow_disabled and not token_db.enabled):
        raise errors.NotAuthorizedError()

    return token_db


def get_token(
    session: Session, api_key: Optional[str] = None, token_id: Optional[UUID] = None, allow_disabled: bool = True
) -> token.TokenGetUpdateOut:
    """
    Retrieve token from database by api key or token id and transform it to response schema.
    """
    try:
        token_db = get_token_db(session, api_key=api_key, token_id=token_id, allow_disabled=allow_disabled)
    except errors.NotAuthorizedError as not_authorized_error:
        raise errors.NoSuchEntityError from not_authorized_error

    return _token_to_token_get_update_out(token_db)


def get_tokens(session: Session, filters: Optional[List[Any]] = None) -> List[token.TokenGetUpdateOut]:
    """
    Retrieve tokens from database and transform them to response schema.
    """
    query = session.query(Token)

    filters = filters or []
    for token_filter in filters:
        query = query.filter(token_filter)

    tokens_db = query.all()

    return [_token_to_token_get_update_out(token_db) for token_db in tokens_db]


def update_token(session: Session, token_id: UUID, token_in: token.TokenUpdateIn) -> token.TokenGetUpdateOut:
    """
    Update token in database and transform it to response schema.
    """
    token_db = get_token_db(session, token_id=token_id, allow_disabled=False)

    token_db.name = token_in.name

    if not token_db:
        raise errors.NoSuchEntityError()

    session.commit()

    return get_token(session, token_id=token_id, allow_disabled=False)


def disable_token(session: Session, token_id: UUID) -> token.TokenGetUpdateOut:
    """
    Disable token in database and transform it to response schema.
    """
    token_db = get_token_db(session, token_id=token_id, allow_disabled=False)

    token_db.enabled = False

    if not token_db:
        raise errors.NoSuchEntityError()

    session.commit()

    return get_token(session, token_id=token_id, allow_disabled=True)
