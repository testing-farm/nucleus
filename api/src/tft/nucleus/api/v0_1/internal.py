# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides public implementation of testing farm API v0.1.
"""
from typing import Dict

from fastapi import APIRouter

from tft.nucleus.api.about import about_get
from tft.nucleus.api.v0_1.routers import secrets, test_request

internal_api_router_v0_1 = APIRouter()


@internal_api_router_v0_1.get('/about', summary='About Testing Farm')
def get_about() -> Dict[str, str]:
    """
    The function returns metadata about nucleus api package.
    """
    return about_get()


internal_api_router_v0_1.include_router(test_request.internal_router, prefix='/requests')
internal_api_router_v0_1.include_router(secrets.internal_router, prefix='/secrets')
