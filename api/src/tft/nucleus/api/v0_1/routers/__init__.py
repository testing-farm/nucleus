# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides routers for testing farm API v0.1.
Routers are meant to call middleware functions.
"""
from fastapi.security import OAuth2PasswordBearer

api_key_header = OAuth2PasswordBearer(
    tokenUrl="token",
    auto_error=False,
    description="The API key for authentication.",
)
