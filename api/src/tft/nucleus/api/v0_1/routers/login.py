# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of login router
"""
from fastapi import APIRouter, Depends
from fastapi.responses import RedirectResponse, Response
from sqlalchemy.orm import Session

from tft.nucleus.api.core.database import get_db
from tft.nucleus.api.middleware import login

router = APIRouter()


@router.get('/github')
def login_github() -> RedirectResponse:
    """
    Login endpoint redirecting to github authentication page.
    """
    return login.login_github()


@router.get("/github/callback", include_in_schema=False)
def login_github_callback(code: str, session: Session = Depends(get_db)) -> Response:  # pylint: disable=too-many-locals
    """
    Login callback after authenticating to github.
    """
    return login.login_github_callback(code, session)


@router.get('/fedora')
def login_fedora() -> RedirectResponse:
    """
    Login endpoint redirecting to fedora authentication page.
    """
    return login.login_fedora()


@router.get("/fedora/callback", include_in_schema=False)
def login_fedora_callback(code: str, session: Session = Depends(get_db)) -> Response:
    """
    Login callback after authenticating to fedora.
    """
    return login.login_fedora_callback(code, session)


@router.get('/redhat')
def login_redhat() -> RedirectResponse:
    """
    Login endpoint redirecting to redhat authentication page.
    """
    return login.login_redhat()


@router.get("/redhat/callback", include_in_schema=False)
def login_redhat_callback(code: str, session: Session = Depends(get_db)) -> Response:
    """
    Login callback after authenticating to redhat.
    """
    return login.login_redhat_callback(code, session)
