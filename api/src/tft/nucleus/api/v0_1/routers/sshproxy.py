# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of ssh proxy router
"""
from typing import Any, Dict

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from tft.nucleus.api.config import settings
from tft.nucleus.api.core import errors
from tft.nucleus.api.core.database import get_db
from tft.nucleus.api.crud.token import get_token_db

router = APIRouter()


@router.get('', response_model=None)
def get_sshproxy(api_key: str, session: Session = Depends(get_db)) -> Dict[str, Any]:
    """
    Get ssh proxy.
    """
    token = get_token_db(session, api_key=api_key)
    if not token.enabled or not token.ranch:
        raise errors.ForbiddenError

    proxy = settings.get("SSH_PROXY", {}).get(token.ranch.upper())
    ssh_private_key_base_64 = settings.get("SSH_PRIVATE_KEY_BASE_64", {}).get(token.ranch.upper())
    content = {
        'ssh_proxy': proxy,
        'ssh_private_key_base_64': ssh_private_key_base_64,
    }
    return content
