# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of test requests router
"""
from typing import List, Optional
from uuid import UUID

from fastapi import APIRouter, Depends, Request, Response
from sqlalchemy.orm import Session

from tft.nucleus.api.core.database import get_db
from tft.nucleus.api.core.schemes import test_request
from tft.nucleus.api.middleware import test_request as mw_test_request
from tft.nucleus.api.v0_1.routers import api_key_header

internal_router = APIRouter()
public_router = APIRouter()


@internal_router.post('', response_model=test_request.RequestCreateOut)
@public_router.post('', response_model=test_request.RequestCreateOut)
def request_a_new_test(
    request: Request,
    request_in: test_request.RequestCreateIn,
    session: Session = Depends(get_db),
    api_key: Optional[str] = Depends(api_key_header),
) -> test_request.RequestCreateOut:
    """
    Create new test request handler
    """
    return mw_test_request.create_test_request(request, session, request_in, api_key)


@public_router.get('/{request_id}', response_model=test_request.RequestGetUpdateOut)
def test_request_details(
    request: Request, request_id: UUID, session: Session = Depends(get_db)
) -> test_request.RequestGetUpdateOut:
    """
    Get test request handler
    """
    return mw_test_request.get_test_request(request, session, request_id)


@public_router.get('', response_model=Optional[List[test_request.RequestGetUpdateOut]])
def get_test_requests(
    request: Request,
    state: Optional[str] = None,
    # NOTE: user_id is the same as token_id, it is preserved for backwards compatibility
    token_id: Optional[UUID] = None,
    user_id: Optional[UUID] = None,
    ranch: Optional[str] = None,
    created_before: Optional[str] = None,
    created_after: Optional[str] = None,
    session: Session = Depends(get_db),
) -> Optional[List[test_request.RequestGetUpdateOut]]:
    """
    Get all test requests with the given state handler
    """
    return mw_test_request.get_test_requests(
        request, session, state, token_id or user_id, ranch, created_before, created_after
    )


@internal_router.get('/{request_id}', response_model=test_request.RequestGetAuthenticatedOut)
def test_request_details_authenticated(
    request: Request,
    request_id: UUID,
    api_key: Optional[str] = None,
    session: Session = Depends(get_db),
    api_key_from_header: Optional[str] = Depends(api_key_header),
) -> test_request.RequestGetUpdateOut:
    """
    Get test request handler
    """
    return mw_test_request.get_test_request_authenticated(request, session, request_id, api_key, api_key_from_header)


@internal_router.put('/{request_id}', response_model=test_request.RequestGetUpdateOut)
def update_test_request(
    request: Request,
    request_id: UUID,
    request_in: test_request.RequestUpdateIn,
    session: Session = Depends(get_db),
    api_key: Optional[str] = Depends(api_key_header),
) -> test_request.RequestGetUpdateOut:
    """
    Update test request handler
    """
    return mw_test_request.update_test_request(request, session, request_id, request_in, api_key)


@public_router.delete('/{request_id}', response_model=test_request.RequestGetUpdateOut)
def delete_test_request(
    request: Request,
    request_id: UUID,
    response: Response,
    request_in: Optional[test_request.RequestDeleteIn] = None,
    session: Session = Depends(get_db),
    api_key: Optional[str] = Depends(api_key_header),
) -> test_request.RequestGetUpdateOut:
    """
    Delete test request handler
    """
    response.status_code, request_update = mw_test_request.delete_test_request(
        request, session, request_id, request_in, api_key
    )

    return request_update
