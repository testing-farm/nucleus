# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of secrets router
"""
from typing import Optional

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from tft.nucleus.api.core.database import get_db
from tft.nucleus.api.core.schemes import secrets
from tft.nucleus.api.middleware import secrets as mw_secrets
from tft.nucleus.api.v0_1.routers import api_key_header

public_router = APIRouter()
internal_router = APIRouter()


@public_router.post('/public-key')
def public_key(
    repo_in: secrets.RepoIn, session: Session = Depends(get_db), api_key: Optional[str] = Depends(api_key_header)
) -> str:
    """
    Retrieve a public key for a particular repo.
    """
    return mw_secrets.public_key(repo_in, session, api_key)


@public_router.post('/encrypt')
def encrypt(
    encrypt_in: secrets.EncryptIn, session: Session = Depends(get_db), api_key: Optional[str] = Depends(api_key_header)
) -> str:
    """
    Encrypt a message for a particular repo.
    """
    return mw_secrets.encrypt(encrypt_in, session, api_key)


@internal_router.post('/decrypt')
def decrypt(
    decrypt_in: secrets.DecryptIn, session: Session = Depends(get_db), api_key: Optional[str] = Depends(api_key_header)
) -> str:
    """
    Decrypt a message for a particular repo.
    """
    return mw_secrets.decrypt(decrypt_in, session, api_key)
