# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of whoami router
"""
from typing import Optional

from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session

from tft.nucleus.api.core.database import get_db
from tft.nucleus.api.core.schemes import whoami as schemes_whoami
from tft.nucleus.api.middleware import whoami as mw_whoami
from tft.nucleus.api.v0_1.routers import api_key_header

router = APIRouter()


@router.get('', response_model=schemes_whoami.WhoamiGetOut)
def whoami(
    request: Request, session: Session = Depends(get_db), api_key: Optional[str] = Depends(api_key_header)
) -> schemes_whoami.WhoamiGetOut:
    """
    Get information about token and user.
    """
    return mw_whoami.get_whoami(request, session, api_key)
