# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of tokens router
"""
from typing import List, Union

from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session

from tft.nucleus.api.core.database import Token, get_db
from tft.nucleus.api.core.schemes import token
from tft.nucleus.api.login import AuthorizedUser, parse_bearer_token
from tft.nucleus.api.middleware import token as mw_token

router = APIRouter()


@router.post('', response_model=token.TokenCreateOut)
def create_token(
    request: Request,
    token_in: token.TokenCreateIn,
    session: Session = Depends(get_db),
    user: AuthorizedUser = Depends(parse_bearer_token),
) -> token.TokenCreateOut:
    """
    Create new token handler
    """
    return mw_token.create_token(request, session, token_in, user)


@router.put('/{token_id}', response_model=token.TokenGetUpdateOut)
def update_token(
    request: Request,
    token_id: str,
    token_in: token.TokenUpdateIn,
    session: Session = Depends(get_db),
    user: AuthorizedUser = Depends(parse_bearer_token),
) -> token.TokenGetUpdateOut:
    """
    Update new token handler
    """
    return mw_token.update_token(request, session, token_id, token_in, user)


@router.get('/{token_id}', response_model=token.TokenGetUpdateOut)
def get_token(
    request: Request,
    token_id: str,
    session: Session = Depends(get_db),
    auth: Union[AuthorizedUser, Token] = Depends(parse_bearer_token),
) -> token.TokenGetUpdateOut:
    """
    Get token handler
    """
    return mw_token.get_token(request, session, token_id, auth)


@router.get('', response_model=List[token.TokenGetUpdateOut])
def get_tokens(
    request: Request,
    session: Session = Depends(get_db),
    auth: Union[AuthorizedUser, Token] = Depends(parse_bearer_token),
) -> List[token.TokenGetUpdateOut]:
    """
    Get tokens handler
    """
    return mw_token.get_tokens(request, session, auth)


@router.delete('/{token_id}', response_model=token.TokenGetUpdateOut)
def delete_token(
    request: Request,
    token_id: str,
    session: Session = Depends(get_db),
    auth: AuthorizedUser = Depends(parse_bearer_token),
) -> token.TokenGetUpdateOut:
    """
    Delete token handler
    """
    return mw_token.disable_token(request, session, token_id, auth)
