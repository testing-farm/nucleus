# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of user routers
"""

from typing import Optional

from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session

from tft.nucleus.api.core import errors
from tft.nucleus.api.core.database import get_db
from tft.nucleus.api.core.schemes import user
from tft.nucleus.api.middleware import user as mw_user
from tft.nucleus.api.v0_1.routers import api_key_header

router = APIRouter()


@router.delete('/{user_id}', response_model=user.UserGetUpdateOut)
def disable_user(
    request: Request,
    user_id: str,
    api_key: Optional[str] = None,
    session: Session = Depends(get_db),
    api_key_from_header: Optional[str] = Depends(api_key_header),
) -> user.UserGetUpdateOut:
    """
    Disable user and anonymize its name
    """
    api_key = api_key or api_key_from_header
    if api_key is None:
        raise errors.UnprocessableEntityError(message='API key is required')

    return mw_user.disable_user(request, session, api_key, user_id)
