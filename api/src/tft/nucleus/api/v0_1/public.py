# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides public implementation of testing farm API v0.1.
"""
from typing import Dict

from fastapi import APIRouter

from tft.nucleus.api.about import about_get
from tft.nucleus.api.v0_1.routers import (
    compose,
    login,
    secrets,
    sshproxy,
    test_request,
    token,
    user,
    whoami,
)

public_api_router_v0_1 = APIRouter()


@public_api_router_v0_1.get('/about', summary='About Testing Farm')
def get_about() -> Dict[str, str]:
    """
    The function returns metadata about nucleus api package.
    """
    return about_get()


public_api_router_v0_1.include_router(test_request.public_router, prefix='/requests')
public_api_router_v0_1.include_router(compose.router, prefix='/composes')
public_api_router_v0_1.include_router(login.router, prefix='/login')
public_api_router_v0_1.include_router(token.router, prefix='/tokens')
public_api_router_v0_1.include_router(user.router, prefix='/users')
public_api_router_v0_1.include_router(sshproxy.router, prefix='/sshproxy')
public_api_router_v0_1.include_router(secrets.public_router, prefix='/secrets')
public_api_router_v0_1.include_router(whoami.router, prefix='/whoami')
