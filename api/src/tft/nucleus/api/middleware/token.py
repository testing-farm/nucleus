# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of tokens middleware operations.
"""
import enum
from typing import Any, Dict, List, Optional, Union, cast
from uuid import UUID

from fastapi import Request
from sqlalchemy.orm import Session

from tft.nucleus.api.core import errors
from tft.nucleus.api.core.access_control import (
    AccessAction,
    AccessObject,
    access_logger,
    check_access_api_key,
)
from tft.nucleus.api.core.database import Token, User
from tft.nucleus.api.core.log import get_extra_from_user, log_access_attempt
from tft.nucleus.api.core.schemes import token
from tft.nucleus.api.crud import token as crud_token
from tft.nucleus.api.crud.user import get_user_by_auth_id_and_auth_method
from tft.nucleus.api.login import AuthorizedUser


class TokenRole(enum.IntEnum):
    """
    Enum representing possible roles of a token.
    The order is critical here as it is used to determine which token role is higher.
    """

    USER = 1
    POWERUSER = 2
    WORKER = 3
    ADMIN = 4
    FEDORA_ADMIN = 5


def _log_access_token_as_user(
    session: Session,
    user_db: User,
    obj: AccessObject,
    action: AccessAction,
    access: bool,
    logging_extra: Optional[Dict[str, Any]] = None,
) -> None:
    """
    Log access attempt as user.
    """
    logging_extra = logging_extra or {}

    logging_extra.update(get_extra_from_user(session, user_db))

    log_access_attempt(
        access_logger,
        None,
        obj,
        action,
        access,
        extra=logging_extra,
    )


def create_token(
    request: Request, session: Session, token_in: token.TokenCreateIn, auth: Union[AuthorizedUser, Token]
) -> token.TokenCreateOut:
    """
    Create a token in database and transform it to response schema.
    """

    try:
        TokenRole[token_in.role.upper()]
    except KeyError:
        raise errors.BadRequestError(  # pylint: disable=raise-missing-from
            message=f'Invalid token role {token_in.role}.'
        )

    # Both API Tokens and User JWTs are allowed on this endpoint
    if isinstance(auth, Token):
        check_access_api_key(
            session,
            auth.api_key,
            AccessObject.TOKEN,
            AccessAction.CREATE,
            logging_extra={"client_ip": request.client.host if request.client else ""},
        )
        # NOTE: at the moment, users of any role can create tokens only for themselves
        token_db = crud_token.get_token_db(session, api_key=auth.api_key)
        user_id = token_db.user_id
    else:
        user_db = get_user_by_auth_id_and_auth_method(session, auth.auth_id, auth.auth_method)

        user_id = user_db.id
        if token_in.ranch not in cast(AuthorizedUser, auth).ranch:
            _log_access_token_as_user(
                session,
                user_db,
                AccessObject.TOKEN,
                AccessAction.CREATE,
                False,
                logging_extra={"client_ip": request.client.host if request.client else ""},
            )
            raise errors.ForbiddenError(message=f'User not authorized to create token in ranch {token_in.ranch}.')

        if TokenRole[token_in.role.upper()] > TokenRole[auth.role.upper()]:
            _log_access_token_as_user(
                session,
                user_db,
                AccessObject.TOKEN,
                AccessAction.CREATE,
                False,
                logging_extra={"client_ip": request.client.host if request.client else ""},
            )
            raise errors.ForbiddenError(message=f'User not authorized to create token with role {token_in.role}.')

        _log_access_token_as_user(
            session,
            user_db,
            AccessObject.TOKEN,
            AccessAction.CREATE,
            True,
            logging_extra={"client_ip": request.client.host if request.client else ""},
        )

    return crud_token.create_token(session, token_in, UUID(user_id))


def get_token(
    request: Request, session: Session, token_id: str, auth: Union[AuthorizedUser, Token]
) -> token.TokenGetUpdateOut:
    """
    Retrieve token from database and transform to response schema.
    """

    try:
        token_id_uuid = UUID(token_id)
    except ValueError:
        raise errors.BadRequestError(message='Token is not a valid UUID string.')  # pylint: disable=raise-missing-from

    token_out = crud_token.get_token(session, token_id=token_id_uuid, allow_disabled=False)

    # Both API Tokens and User JWTs are allowed on this endpoint
    if isinstance(auth, Token):
        if auth.id == str(token_id):
            check_access_api_key(
                session,
                auth.api_key,
                AccessObject.TOKEN_SELF,
                AccessAction.GET,
                logging_extra={"client_ip": request.client.host if request.client else "", "object_id": token_id},
            )
        else:
            check_access_api_key(
                session,
                auth.api_key,
                AccessObject.TOKEN,
                AccessAction.GET,
                logging_extra={"client_ip": request.client.host if request.client else "", "object_id": token_id},
            )
    else:
        user_db = get_user_by_auth_id_and_auth_method(session, auth.auth_id, auth.auth_method)

        if token_out.user_id != user_db.id and TokenRole[auth.role.upper()] == TokenRole.USER:
            _log_access_token_as_user(
                session,
                user_db,
                AccessObject.TOKEN,
                AccessAction.GET,
                False,
                logging_extra={"client_ip": request.client.host if request.client else "", "object_id": token_id},
            )
            raise errors.ForbiddenError(message='User not authorized to fetch tokens of other users.')

        _log_access_token_as_user(
            session,
            user_db,
            AccessObject.TOKEN_SELF,
            AccessAction.GET,
            True,
            logging_extra={"client_ip": request.client.host if request.client else "", "object_id": token_id},
        )

    return token_out


def get_tokens(request: Request, session: Session, auth: Union[AuthorizedUser, Token]) -> List[token.TokenGetUpdateOut]:
    """
    Retrieve tokens from database and transform to response schemes list.
    """

    filters = []
    # Both API Tokens and User JWTs are allowed on this endpoint
    if isinstance(auth, Token):
        # API token fetches ALL tokens
        check_access_api_key(
            session,
            auth.api_key,
            AccessObject.TOKENS_LIST,
            AccessAction.GET,
            logging_extra={"client_ip": request.client.host if request.client else ""},
        )
    else:
        # User's JWT token fetches only user's enabled tokens
        user_db = get_user_by_auth_id_and_auth_method(session, auth.auth_id, auth.auth_method)

        _log_access_token_as_user(
            session,
            user_db,
            AccessObject.TOKENS_LIST,
            AccessAction.GET,
            True,
            logging_extra={"client_ip": request.client.host if request.client else ""},
        )

        filters.append(Token.user_id == user_db.id)
        filters.append(Token.enabled)

    tokens_out = crud_token.get_tokens(session, filters)
    return tokens_out


def update_token(
    request: Request, session: Session, token_id: str, token_in: token.TokenUpdateIn, auth: Union[AuthorizedUser, Token]
) -> token.TokenGetUpdateOut:
    """
    Update token in database and return response schema.
    """

    try:
        token_id_uuid = UUID(token_id)
    except ValueError:
        raise errors.BadRequestError(message='Token is not a valid UUID string.')  # pylint: disable=raise-missing-from

    token_db = crud_token.get_token_db(session, token_id=token_id_uuid)

    # Both API Tokens and User JWTs are allowed on this endpoint
    if isinstance(auth, Token):
        check_access_api_key(
            session,
            auth.api_key,
            AccessObject.TOKEN,
            AccessAction.UPDATE,
            logging_extra={"client_ip": request.client.host if request.client else "", "object_id": token_id},
        )
    else:
        user_db = get_user_by_auth_id_and_auth_method(session, auth.auth_id, auth.auth_method)

        if token_db.user_id != user_db.id:
            _log_access_token_as_user(
                session,
                user_db,
                AccessObject.TOKEN,
                AccessAction.UPDATE,
                False,
                logging_extra={"client_ip": request.client.host if request.client else "", "object_id": token_id},
            )
            raise errors.ForbiddenError(message='User not authorized to change tokens of other users.')

        _log_access_token_as_user(
            session,
            user_db,
            AccessObject.TOKEN_SELF,
            AccessAction.UPDATE,
            True,
            logging_extra={"client_ip": request.client.host if request.client else "", "object_id": token_id},
        )

    return crud_token.update_token(session, token_id_uuid, token_in)


def disable_token(
    request: Request, session: Session, token_id: str, auth: Union[AuthorizedUser, Token]
) -> token.TokenGetUpdateOut:
    """
    Disable the token.
    """

    try:
        token_id_uuid = UUID(token_id)
    except ValueError:
        raise errors.BadRequestError()  # pylint: disable=raise-missing-from

    token_db = crud_token.get_token_db(session, token_id=token_id_uuid)

    # Both API Tokens and User JWTs are allowed on this endpoint
    if isinstance(auth, Token):
        check_access_api_key(
            session,
            auth.api_key,
            AccessObject.TOKEN,
            AccessAction.DELETE,
            logging_extra={"client_ip": request.client.host if request.client else "", "object_id": token_id},
        )
    else:
        user_db = get_user_by_auth_id_and_auth_method(session, auth.auth_id, auth.auth_method)

        if token_db.user_id != user_db.id:
            _log_access_token_as_user(
                session,
                user_db,
                AccessObject.TOKEN,
                AccessAction.DELETE,
                False,
                logging_extra={"client_ip": request.client.host if request.client else "", "object_id": token_id},
            )
            raise errors.ForbiddenError(message='User not authorized to delete tokens of other users.')

        _log_access_token_as_user(
            session,
            user_db,
            AccessObject.TOKEN_SELF,
            AccessAction.DELETE,
            True,
            logging_extra={"client_ip": request.client.host if request.client else "", "object_id": token_id},
        )

    return crud_token.disable_token(session, token_id_uuid)
