# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of whoami middleware operations.
"""
from typing import Optional

from fastapi import Request as fastapi_request
from sqlalchemy.orm import Session

from tft.nucleus.api.core import errors
from tft.nucleus.api.core.access_control import (
    AccessAction,
    AccessObject,
    check_access_api_key,
)
from tft.nucleus.api.core.schemes import whoami
from tft.nucleus.api.crud.token import get_token
from tft.nucleus.api.crud.user import _user_to_user_get_update_out, get_user_by_id


def get_whoami(fastapi_request: fastapi_request, session: Session, api_key: Optional[str]) -> whoami.WhoamiGetOut:
    if api_key is None:
        raise errors.UnprocessableEntityError(message='API key is required')

    check_access_api_key(
        session,
        api_key,
        AccessObject.TOKEN_SELF,
        AccessAction.GET,
        logging_extra={'client_ip': fastapi_request.client.host if fastapi_request.client else ""},
    )

    token = get_token(session, api_key=api_key, allow_disabled=False)
    user_db = get_user_by_id(session, token.user_id)
    return whoami.WhoamiGetOut(token=token, user=_user_to_user_get_update_out(user_db))
