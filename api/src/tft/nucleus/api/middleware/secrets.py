# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of secrets router
"""
import base64
import binascii
import re
from typing import Optional, cast
from uuid import UUID

from cryptography.hazmat.backends.openssl.rsa import _RSAPrivateKey, _RSAPublicKey
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import padding
from fastapi import Depends
from sqlalchemy.orm import Session

from tft.nucleus.api.core import errors
from tft.nucleus.api.core.access_control import (
    AccessAction,
    AccessObject,
    check_access_api_key,
)
from tft.nucleus.api.core.database import get_db
from tft.nucleus.api.core.schemes import secrets
from tft.nucleus.api.crud import secrets as crud_secrets
from tft.nucleus.api.crud import token
from tft.nucleus.api.v0_1.routers import api_key_header


def normalize_git_url(url: str) -> str:
    """
    Normalize various git url formats into `https://FORGE/NAMESPACE/REPO` format.
    """
    url = re.sub(r'^ssh://', '', url)
    url = re.sub(r'^git@(.*?):', r'https://\1/', url)
    url = re.sub(r'^http://', 'https://', url)
    url = re.sub(r'.git$', '', url)
    return url


def parse_rsa_public_key(public_key: str) -> _RSAPublicKey:
    """
    Parse RSA public key in PEM representation into an object.
    """
    return cast(_RSAPublicKey, serialization.load_pem_public_key(public_key.encode()))


def parse_rsa_private_key(private_key: str) -> _RSAPrivateKey:
    """
    Parse RSA private key in PEM representation into an object.
    """
    return cast(_RSAPrivateKey, serialization.load_pem_private_key(private_key.encode(), password=None))


def encrypt_message(public_key: _RSAPublicKey, message: str) -> str:
    """
    Encrypt a message using a public key.
    """
    encrypted_message = public_key.encrypt(
        message.encode(),
        padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA256()), algorithm=hashes.SHA256(), label=None),
    )
    return base64.b64encode(encrypted_message).decode()


def decrypt_message(private_key: _RSAPrivateKey, message: str) -> str:
    """
    Encrypt a message using a private key.
    """
    decrypted_message = private_key.decrypt(
        base64.b64decode(message),
        padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA256()), algorithm=hashes.SHA256(), label=None),
    )
    return decrypted_message.decode()


def public_key(
    repo_in: secrets.RepoIn, session: Session = Depends(get_db), api_key: Optional[str] = Depends(api_key_header)
) -> str:
    """
    Retrieve a public key for a particular repo.
    """
    if api_key is None:
        raise errors.UnprocessableEntityError(message='API key is required')
    check_access_api_key(session, api_key, AccessObject.SECRET, AccessAction.CREATE)

    if repo_in.token_id:
        token_id = token.get_token_db(session, token_id=repo_in.token_id, allow_disabled=False).id
    else:
        token_id = token.get_token_db(session, api_key=api_key, allow_disabled=False).id

    git_url = normalize_git_url(repo_in.url)
    try:
        return crud_secrets.get_repo_key_pair_db(session, git_url, token_id).public_key
    except errors.NoSuchEntityError:
        return crud_secrets.create_key_pair(session, git_url, token_id).public_key


def encrypt(
    encrypt_in: secrets.EncryptIn, session: Session = Depends(get_db), api_key: Optional[str] = Depends(api_key_header)
) -> str:
    """
    Encrypt a message for a particular repo.
    """
    if api_key is None:
        raise errors.UnprocessableEntityError(message='API key is required')
    check_access_api_key(session, api_key, AccessObject.SECRET, AccessAction.CREATE)

    if encrypt_in.token_id:
        token_id = token.get_token_db(session, token_id=encrypt_in.token_id, allow_disabled=False).id
    else:
        token_id = token.get_token_db(session, api_key=api_key, allow_disabled=False).id

    git_url = normalize_git_url(encrypt_in.url)

    try:
        repo = crud_secrets.get_repo_key_pair_db(session, git_url, token_id)
    except errors.NoSuchEntityError:
        repo = crud_secrets.create_key_pair(session, git_url, token_id)

    public_key_obj = parse_rsa_public_key(repo.public_key)

    return f'{token_id},{encrypt_message(public_key_obj, encrypt_in.message)}'


def decrypt(
    decrypt_in: secrets.DecryptIn, session: Session = Depends(get_db), api_key: Optional[str] = Depends(api_key_header)
) -> str:
    """
    Decrypt a message for a particular repo.
    """
    if api_key is None:
        raise errors.UnprocessableEntityError(message='API key is required')
    check_access_api_key(session, api_key, AccessObject.SECRET, AccessAction.GET)

    token_id, message = decrypt_in.message.split(',', 1)

    try:
        UUID(token_id)
    except ValueError:
        raise errors.BadRequestError(message='Token is not a valid UUID string.')  # pylint: disable=raise-missing-from

    git_url = normalize_git_url(decrypt_in.url)

    repo = crud_secrets.get_repo_key_pair_db(session, git_url, token_id)
    private_key_obj = parse_rsa_private_key(repo.private_key)

    try:
        return decrypt_message(private_key_obj, message)
    except binascii.Error:
        raise errors.BadRequestError(message='Encrypted message does not have a valid base64 format.')
    except ValueError:
        raise errors.BadRequestError(message='Unable to decrypt message.')
