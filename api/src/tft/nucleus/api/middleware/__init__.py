# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of middleware operations.

Middleware is a software component that is inserted between routers and crud operations.
The purpose of middleware is to perform some actions before and after the crud operation
such as logging, authentication, authorization, data transformation, etc.
"""
