# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of test requests middleware operations.
"""
import re
from datetime import datetime
from typing import Dict, List, Optional, Tuple
from uuid import UUID

from fastapi import Request as fastapi_request
from sqlalchemy.orm import Session

from tft.nucleus.api.config import settings
from tft.nucleus.api.core import errors, helpers
from tft.nucleus.api.core.access_control import (
    AccessAction,
    AccessObject,
    check_access_api_key,
    is_test_request_owned_by_token,
)
from tft.nucleus.api.core.database import Request, RequestStateType, Token
from tft.nucleus.api.core.schemes import test_request
from tft.nucleus.api.crud import composes as crud_composes
from tft.nucleus.api.crud import test_request as crud_test_request
from tft.nucleus.api.crud import token as crud_token

STATES_EXCEPT_CANCELED: List[RequestStateType] = [
    state for state in RequestStateType if state != RequestStateType.CANCELED
]

# Allowed state transitions from current state to future states
ALLOWED_STATE_TRANSITIONS: Dict[RequestStateType, List[RequestStateType]] = {
    RequestStateType.NEW: STATES_EXCEPT_CANCELED,
    RequestStateType.QUEUED: STATES_EXCEPT_CANCELED,
    RequestStateType.RUNNING: STATES_EXCEPT_CANCELED,
    RequestStateType.COMPLETE: STATES_EXCEPT_CANCELED,
    RequestStateType.ERROR: STATES_EXCEPT_CANCELED,
    RequestStateType.CANCEL_REQUESTED: [
        RequestStateType.CANCEL_REQUESTED,
        RequestStateType.CANCELED,
    ],
    RequestStateType.CANCELED: [
        RequestStateType.CANCELED,
    ],
}


def _remove_secrets(request: test_request.RequestGetUpdateOut) -> test_request.RequestGetUpdateOut:
    # hide possible sensitive tmt environment variables
    if request.environments_requested:
        for environment in request.environments_requested:
            environment.secrets = None
            if environment.tmt:
                environment.tmt.environment = None

    # git url can contain secrets
    if request.test:
        test = request.test.fmf or request.test.sti
        # test must exist
        assert test is not None
        test.url = helpers.hide_cred_in_url(test.url)

    return request


def get_test_request(
    fastapi_request: fastapi_request, session: Session, request_id: UUID
) -> test_request.RequestGetUpdateOut:
    """
    Retrieve request from database and transform to response schema.
    """
    check_access_api_key(
        session,
        None,
        AccessObject.REQUEST,
        AccessAction.GET,
        logging_extra={
            'client_ip': fastapi_request.client.host if fastapi_request.client else "",
            "object_id": request_id,
        },
    )

    request_out = crud_test_request.get_test_request(session, request_id)
    return _remove_secrets(request_out)


def get_test_requests(
    fastapi_request: fastapi_request,
    session: Session,
    state: Optional[str],
    token_id: Optional[UUID],
    ranch: Optional[str],
    created_before: Optional[str],
    created_after: Optional[str],
) -> Optional[List[test_request.RequestGetUpdateOut]]:
    """
    Get all test requests with the given state
    """
    check_access_api_key(
        session,
        None,
        AccessObject.REQUESTS_LIST,
        AccessAction.GET,
        logging_extra={'client_ip': fastapi_request.client.host if fastapi_request.client else ""},
    )

    if not any([state, token_id]):
        return None

    if state and state not in [str(state) for state in RequestStateType]:
        return None

    filters = []
    join_token = False

    if state:
        filters.append(Request.state == RequestStateType[state.upper()].value)

    if token_id:
        filters.append(Request.token_id == str(token_id))

    if ranch:
        filters.append(Token.ranch == ranch)
        join_token = True

    if created_before:
        filters.append(Request.created < datetime.fromisoformat(created_before))

    if created_after:
        filters.append(Request.created > datetime.fromisoformat(created_after))

    requests = crud_test_request.get_test_requests(session, filters, join_token)

    if not requests:
        return None

    requests_out = []
    for request in requests:
        requests_out.append(_remove_secrets(request))

    return requests_out


def get_test_request_authenticated(
    fastapi_request: fastapi_request,
    session: Session,
    request_id: UUID,
    api_key: Optional[str],
    api_key_from_header: Optional[str],
) -> test_request.RequestGetAuthenticatedOut:
    """
    Authenticate and retrieve request from database and transform to response schema.
    """
    api_key = api_key or api_key_from_header
    if api_key is None:
        raise errors.UnprocessableEntityError(message='API key is required')

    if is_test_request_owned_by_token(session, api_key, request_id):
        check_access_api_key(
            session,
            api_key,
            AccessObject.REQUEST_OWNED,
            AccessAction.GET_SECRETS,
            logging_extra={
                'client_ip': fastapi_request.client.host if fastapi_request.client else "",
                "object_id": request_id,
            },
        )
    else:
        check_access_api_key(
            session,
            api_key,
            AccessObject.REQUEST,
            AccessAction.GET_SECRETS,
            logging_extra={
                'client_ip': fastapi_request.client.host if fastapi_request.client else "",
                "object_id": request_id,
            },
        )

    return crud_test_request.get_test_request_authenticated(session, request_id)


def create_test_request(
    fastapi_request: fastapi_request,
    session: Session,
    test_request_create_in: test_request.RequestCreateIn,
    api_key: Optional[str],
) -> test_request.RequestCreateOut:
    """
    Create test request in database and transform it to response schema.
    """
    api_key = api_key or test_request_create_in.api_key
    if not api_key:
        raise errors.UnprocessableEntityError(message="API key is required")

    check_access_api_key(
        session,
        api_key,
        AccessObject.REQUEST,
        AccessAction.CREATE,
        logging_extra={'client_ip': fastapi_request.client.host if fastapi_request.client else ""},
    )

    auth_token_db = crud_token.get_token_db(session, api_key=api_key)

    # If tmt alias is used, move test to fmf
    if hasattr(test_request_create_in.test, "tmt") and test_request_create_in.test.tmt is not None:
        test_request_create_in.test.fmf = test_request_create_in.test.tmt
        test_request_create_in.test.tmt = None

    if auth_token_db.ranch is None:
        raise errors.GenericError("The token's ranch is not defined")
    composes = crud_composes.get_composes_from_file(settings.COMPOSES[auth_token_db.ranch.upper()])

    if test_request_create_in.environments is not None:
        for environment in test_request_create_in.environments:

            if environment.os is not None:
                if not any(re.match(compose.name, environment.os.compose) for compose in composes):

                    raise errors.BadRequestError(
                        message=f"Compose {environment.os.compose} does not exist. "
                        + f"Go to {settings.COMPOSE_DOCS} to find all available composes."
                    )

    if (
        test_request_create_in.test.sti
        and test_request_create_in.settings
        and test_request_create_in.settings.pipeline
        and test_request_create_in.settings.pipeline.type == test_request.SettingsPipelineNames.TMT_MULTIHOST
    ):
        raise errors.BadRequestError(
            message=f'The value `{test_request.SettingsPipelineNames.TMT_MULTIHOST}` of option `settings.pipeline.type`'
            ' is incompatible with STI test types.'
        )

    return crud_test_request.create_test_request(session, test_request_create_in, auth_token_db)


def update_test_request(  # pylint: disable=too-many-locals
    fastapi_request: fastapi_request,
    session: Session,
    request_id: UUID,
    test_request_in: test_request.RequestUpdateIn,
    api_key: Optional[str],
) -> test_request.RequestGetUpdateOut:
    """
    Update test request in database and return response schema.
    """
    api_key = api_key or test_request_in.api_key
    if not api_key:
        raise errors.UnprocessableEntityError(message="API key is required")

    if is_test_request_owned_by_token(session, api_key, request_id):
        check_access_api_key(
            session,
            api_key,
            AccessObject.REQUEST_OWNED,
            AccessAction.UPDATE,
            logging_extra={
                'client_ip': fastapi_request.client.host if fastapi_request.client else "",
                "object_id": request_id,
            },
        )
    else:
        check_access_api_key(
            session,
            api_key,
            AccessObject.REQUEST,
            AccessAction.UPDATE,
            logging_extra={
                'client_ip': fastapi_request.client.host if fastapi_request.client else "",
                "object_id": request_id,
            },
        )

    update_test_request_db = crud_test_request.get_test_request_db(session, request_id)

    internal_fields = {}

    if test_request_in.state == str(RequestStateType.RUNNING):
        internal_fields['queued_time'] = datetime.utcnow() - update_test_request_db.created

    if test_request_in.state in [
        str(RequestStateType.COMPLETE),
        str(RequestStateType.ERROR),
        str(RequestStateType.CANCELED),
    ]:
        internal_fields['run_time'] = datetime.utcnow() - update_test_request_db.created

    # TODO: change getting state once database migrated to enums with string values
    if test_request_in.state:

        current_state = update_test_request_db.state
        future_state = RequestStateType.from_string(test_request_in.state)

        if future_state not in ALLOWED_STATE_TRANSITIONS[current_state]:
            raise errors.ConflictError(
                # TODO: find better way to map states
                message=f"Request in state '{str(current_state)}' cannot be changed to '{str(future_state)}'."
            )

        internal_fields['state'] = future_state

    return crud_test_request.update_test_request(session, request_id, test_request_in, internal_fields=internal_fields)


def delete_test_request(
    fastapi_request: fastapi_request,
    session: Session,
    request_id: UUID,
    test_request_delete_in: Optional[test_request.RequestDeleteIn],
    api_key: Optional[str],
) -> Tuple[int, test_request.RequestGetUpdateOut]:
    """
    Cancel test request in database and return response schema.
    """
    if test_request_delete_in is not None and test_request_delete_in.api_key is not None:
        api_key = test_request_delete_in.api_key
    if not api_key:
        raise errors.UnprocessableEntityError(message="API key is required")

    if is_test_request_owned_by_token(session, api_key, request_id):
        check_access_api_key(
            session,
            api_key,
            AccessObject.REQUEST_OWNED,
            AccessAction.DELETE,
            logging_extra={
                'client_ip': fastapi_request.client.host if fastapi_request.client else "",
                "object_id": request_id,
            },
        )
    else:
        check_access_api_key(
            session,
            api_key,
            AccessObject.REQUEST,
            AccessAction.DELETE,
            logging_extra={
                'client_ip': fastapi_request.client.host if fastapi_request.client else "",
                "object_id": request_id,
            },
        )

    update_test_request_db = crud_test_request.get_test_request_db(session, request_id)

    state = update_test_request_db.state

    if state in [RequestStateType.COMPLETE, RequestStateType.ERROR]:
        raise errors.ConflictError(
            # TODO: find better way to map states
            message=f"Request in state '{str(state)}' cannot be canceled"
        )

    status_code = 200

    if state in [RequestStateType.CANCEL_REQUESTED, RequestStateType.CANCELED]:
        status_code = 204

    else:
        crud_test_request.delete_test_request(session, request_id)

    request_out = crud_test_request.get_test_request(session, request_id)
    return status_code, _remove_secrets(request_out)
