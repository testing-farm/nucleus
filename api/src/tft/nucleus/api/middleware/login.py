# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides the implementation of login middleware operations.
"""
from typing import Any, Dict, cast

import requests
from fastapi.responses import RedirectResponse, Response
from sqlalchemy.orm import Session

from tft.nucleus.api.config import settings
from tft.nucleus.api.core import errors
from tft.nucleus.api.core.log import logger
from tft.nucleus.api.crud.user import (
    create_user,
    get_user_by_auth_id_and_auth_method,
    update_user,
)
from tft.nucleus.api.login import create_jwt

GITHUB_AUTHORIZE_URL = "https://github.com/login/oauth/authorize"
GITHUB_TOKEN_URL = "https://github.com/login/oauth/access_token"
GITHUB_USER_URL = "https://api.github.com/user"
GITHUB_ORGS_URL = f"https://api.github.com/orgs/{settings.GITHUB_ORG}/memberships"  # noqa: E231

FEDORA_AUTHORIZE_URL = (
    f"https://id.fedoraproject.org/openidc/Authorization?redirect_uri={settings.FEDORA_REDIRECT_URL}&"  # noqa: E231
    f"scope=openid%20https://{settings.FEDORA_SSO_SERVER}"
    f"/scope/groups&response_type=code&client_id={settings.FEDORA_CLIENT_ID}"
)
FEDORA_TOKEN_URL = f"https://{settings.FEDORA_SSO_SERVER}/openidc/Token"
FEDORA_USER_URL = f"https://{settings.FEDORA_SSO_SERVER}/openidc/UserInfo"

REDHAT_AUTHORIZE_URL = (
    f"https://{settings.REDHAT_SSO_SERVER}/auth/realms/EmployeeIDP/protocol/openid-connect/auth?"
    f"redirect_uri={settings.REDHAT_REDIRECT_URL}&"
    "scope=openid&"
    "response_type=code&"
    f"client_id={settings.REDHAT_CLIENT_ID}"
)
REDHAT_TOKEN_URL = f"https://{settings.REDHAT_SSO_SERVER}/auth/realms/EmployeeIDP/protocol/openid-connect/token"
REDHAT_USER_URL = f"https://{settings.REDHAT_SSO_SERVER}/auth/realms/EmployeeIDP/protocol/openid-connect/userinfo"


def login_github() -> RedirectResponse:
    """
    Login endpoint redirecting to github authentication page.
    """
    return RedirectResponse(
        url=f'{GITHUB_AUTHORIZE_URL}?client_id={settings.GITHUB_CLIENT_ID}&scope=read:org'  # noqa: E231
    )


def login_fedora() -> RedirectResponse:
    """
    Login endpoint redirecting to fedora authentication page.
    """
    return RedirectResponse(url=FEDORA_AUTHORIZE_URL)


def login_redhat() -> RedirectResponse:
    """
    Login endpoint redirecting to redhat authentication page.
    """
    return RedirectResponse(url=REDHAT_AUTHORIZE_URL)


def login_github_callback(code: str, session: Session) -> Response:  # pylint: disable=too-many-locals
    """
    Login callback after authenticating to github.
    """
    # Fetch user's access token using provided code
    token_response = requests.post(
        GITHUB_TOKEN_URL,
        headers={'Accept': 'application/json'},
        data={'client_id': settings.GITHUB_CLIENT_ID, 'client_secret': settings.GITHUB_CLIENT_SECRET, 'code': code},
    )

    if token_response.status_code != 200:
        raise errors.GenericError(message='Error: Failed to retrieve access token.')

    token_json = token_response.json()
    access_token = token_json.get('access_token')

    if not access_token:
        raise errors.GenericError(message='Error: Failed to retrieve access token.')

    # Fetch github user info
    user_response = requests.get(GITHUB_USER_URL, headers={'Authorization': f'token {access_token}'})

    if user_response.status_code != 200:
        raise errors.GenericError(message=f'Error: Failed to retrieve data from {GITHUB_USER_URL}.')

    user_json = user_response.json()

    org_member_response = requests.get(
        f'{GITHUB_ORGS_URL}/{user_json["login"]}', headers={'Authorization': f'token {access_token}'}
    )

    if org_member_response.status_code != 200:
        raise errors.ForbiddenError(message=f'Error: User is not a member of the {settings.GITHUB_ORG} organization.')

    org_member_response_json = org_member_response.json()

    # Authorize user to Testing Farm
    tf_role = None
    tf_ranches = []

    for auth_rule in cast(Dict[str, Any], settings.authentication)['github']:
        if org_member_response_json['role'] == auth_rule['github-role']:
            tf_role = auth_rule['testing-farm-role']
            tf_ranches = auth_rule['ranch']

    if not tf_role:
        raise errors.ForbiddenError(
            message=f'Error: User does not have expected role in the {settings.GITHUB_ORG} organization.'
        )

    # Construct JWT
    user_auth_id = str(user_json['id'])
    user_auth_method = 'github'
    user_auth_name = user_json['login']
    jwt_token = create_jwt(user_auth_id, user_auth_method, user_auth_name, tf_role, tf_ranches)

    # Check whether the user exists in the DB, if not, add him
    try:
        user_db = get_user_by_auth_id_and_auth_method(session, user_auth_id, user_auth_method)
    except errors.NoSuchEntityError:
        logger.info(f'Adding a new user ({user_auth_id}, {user_auth_method}, {user_auth_name}).')
        user_db = create_user(session, user_auth_id, user_auth_method, user_auth_name)

    # Check whether the user has changed their username, if so, update it in the DB
    if user_db.auth_name != user_auth_name:
        logger.info(f'Renaming user ({user_auth_id}, {user_auth_method}, {user_db.auth_name}) to {user_auth_name}.')
        update_user(session, user_auth_id, user_auth_method, user_auth_name)

    logger.info(f'User ({user_auth_id}, {user_auth_method}, {user_auth_name}) successfully logged in.')
    return Response(jwt_token)


def login_fedora_callback(code: str, session: Session) -> Response:
    """
    Login callback after authenticating to fedora.
    """
    # Fetch user's access token using provided code
    token_response = requests.post(
        f'{FEDORA_TOKEN_URL}?client_id={settings.FEDORA_CLIENT_ID}&client_secret={settings.FEDORA_CLIENT_SECRET}&'
        f'code={code}&grant_type=authorization_code&redirect_uri={settings.FEDORA_REDIRECT_URL}'
    )

    if token_response.status_code != 200:
        raise errors.GenericError(message='Error: Failed to retrieve access token.')

    token_json = token_response.json()
    access_token = token_json.get('access_token')

    if not access_token:
        raise errors.GenericError(message='Error: Failed to retrieve access token.')

    # Fetch fedora user info
    user_response = requests.get(FEDORA_USER_URL, headers={'Authorization': f'Bearer {access_token}'})

    if user_response.status_code != 200:
        raise errors.GenericError(message=f'Error: Failed to retrieve data from {FEDORA_USER_URL}.')

    user_json = user_response.json()

    user_auth_id = user_auth_name = user_json['sub']
    user_auth_method = 'fedora'
    user_groups = user_json['groups']

    # Authorize user to Testing Farm
    tf_role = None
    tf_ranches = []

    for auth_rule in cast(Dict[str, Any], settings.authentication)['fedora']:
        if auth_rule['fedora-group'] in user_groups:
            tf_role = auth_rule['testing-farm-role']
            tf_ranches = auth_rule['ranch']

    if not tf_role:
        raise errors.ForbiddenError(message='Error: User is not a member of the organization.')

    # Construct JWT
    jwt_token = create_jwt(user_auth_id, user_auth_method, user_auth_name, tf_role, tf_ranches)

    # Check whether the user exists in the DB, if not, add him
    try:
        get_user_by_auth_id_and_auth_method(session, user_auth_id, user_auth_method)
    except errors.NoSuchEntityError:
        logger.info(f'Adding a new user ({user_auth_id}, {user_auth_method}, {user_auth_name}).')
        create_user(session, user_auth_id, user_auth_method, user_auth_name)

    logger.info(f'User ({user_auth_id}, {user_auth_method}, {user_auth_name}) successfully logged in.')
    # NOTE: We count on `auth_id == auth_name` - we don't expect it is possible to change username in fedora
    return Response(jwt_token)


def login_redhat_callback(code: str, session: Session) -> Response:
    """
    Login callback after authenticating to redhat.
    """
    # Fetch user's access token using provided code
    token_response = requests.post(
        REDHAT_TOKEN_URL,
        data={
            "client_id": settings.REDHAT_CLIENT_ID,
            "client_secret": settings.REDHAT_CLIENT_SECRET,
            "code": code,
            "grant_type": "authorization_code",
            "redirect_uri": settings.REDHAT_REDIRECT_URL,
        },
    )
    if token_response.status_code != 200:
        raise errors.GenericError(message='Error: Failed to retrieve access token.')

    token_json = token_response.json()
    access_token = token_json.get('access_token')

    if not access_token:
        raise errors.GenericError(message='Error: Failed to retrieve access token.')

    # Fetch redhat user info
    user_response = requests.get(REDHAT_USER_URL, headers={'Authorization': f'Bearer {access_token}'})

    if user_response.status_code != 200:
        raise errors.GenericError(message=f'Error: Failed to retrieve data from {REDHAT_USER_URL}.')

    user_json = user_response.json()

    user_auth_id = user_json['sub']
    user_auth_name = user_json['preferred_username']
    user_auth_method = 'redhat'

    # Construct JWT
    jwt_token = create_jwt(user_auth_id, user_auth_method, user_auth_name, 'user', ['public', 'redhat'])

    # Check whether the user exists in the DB, if not, add him
    try:
        user_db = get_user_by_auth_id_and_auth_method(session, user_auth_id, user_auth_method)
    except errors.NoSuchEntityError:
        logger.info(f'Adding a new user ({user_auth_id}, {user_auth_method}, {user_auth_name}).')
        user_db = create_user(session, user_auth_id, user_auth_method, user_auth_name)

    # Check whether the user has changed their username, if so, update it in the DB
    if user_db.auth_name != user_auth_name:
        logger.info(f'Renaming user ({user_auth_id}, {user_auth_method}, {user_db.auth_name}) to {user_auth_name}.')
        update_user(session, user_auth_id, user_auth_method, user_auth_name)

    logger.info(f'User ({user_auth_id}, {user_auth_method}, {user_auth_name}) successfully logged in.')
    return Response(jwt_token)
