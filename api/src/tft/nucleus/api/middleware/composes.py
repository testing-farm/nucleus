# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides the implementation of composes middleware operations.
"""
from tft.nucleus.api.core.schemes.compose import SupportedComposesOut
from tft.nucleus.api.crud import composes


def get_ranch_composes(ranch: str) -> SupportedComposesOut:
    """
    Return supported composes in a ranch
    """
    return composes.get_ranch_composes(ranch)


def get_composes() -> SupportedComposesOut:
    """
    Return supported composes in the public ranch
    """
    return composes.get_ranch_composes('public')
