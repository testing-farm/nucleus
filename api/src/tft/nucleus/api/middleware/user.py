# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of users middleware operations.
"""
from fastapi import Request
from sqlalchemy.orm import Session

from tft.nucleus.api.core import errors
from tft.nucleus.api.core.access_control import (
    AccessAction,
    AccessObject,
    check_access_api_key,
)
from tft.nucleus.api.core.database import User
from tft.nucleus.api.core.schemes import user
from tft.nucleus.api.crud import user as crud_user


def create_user(session: Session, auth_id: str, auth_method: str, auth_name: str) -> User:
    """
    Create a user in database.
    """

    return crud_user.create_user(session, auth_id, auth_method, auth_name)


def update_user(session: Session, auth_id: str, auth_method: str, new_auth_name: str) -> User:
    """
    Update a user in database.
    """

    return update_user(session, auth_id, auth_method, new_auth_name)


def disable_user(request: Request, session: Session, api_key: str, user_id: str) -> user.UserGetUpdateOut:
    """
    Disable and anonymize a user in database.
    """

    if not api_key:
        raise errors.UnprocessableEntityError(message="API key is required")

    check_access_api_key(
        session,
        api_key,
        AccessObject.USER,
        AccessAction.DELETE,
        logging_extra={"client_ip": request.client.host if request.client else "", "object_id": user_id},
    )

    return crud_user.disable_user(session, user_id)
