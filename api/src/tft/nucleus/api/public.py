# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides public implementation of testing farm API.
"""
from fastapi import FastAPI
from sentry_sdk import set_tag
from starlette.responses import RedirectResponse

from tft.nucleus.api.config import settings
from tft.nucleus.api.core.errors import NucleusException, nucleus_exception_handler
from tft.nucleus.api.v0_1.public import public_api_router_v0_1

set_tag('api', 'public')

api = FastAPI(
    title='Testing Farm API',
)
api.include_router(public_api_router_v0_1, prefix=settings.VERSIONS.V0_1.PREFIX)

api.add_exception_handler(NucleusException, nucleus_exception_handler)


@api.get('/', include_in_schema=False)
def redirect_to_docs() -> RedirectResponse:
    """
    The function redirects root endpoint to docs.
    """
    return RedirectResponse(url='/redoc')


# NOTE: In fedora-infrastructure, the OIDC callback URL is set to this endpoint. This endpoint is a configurable
# redirect which redirects to some other endpoint which processes the generated code. In production, this will redirect
# to UI.
@api.get('/login/fedora/callback', include_in_schema=False)
def redirect_to_login_fedora_callback(code: str) -> RedirectResponse:
    """
    The function redirects fedora login callback to proper version of the API.
    """
    return RedirectResponse(url=f'{settings.FEDORA_CALLBACK_REDIRECT_URL}?code={code}')


@api.get('/login/redhat/callback', include_in_schema=False)
def redirect_to_login_redhat_callback(code: str) -> RedirectResponse:
    """
    The function redirects redhat login callback to proper version of the API.
    """
    return RedirectResponse(url=f'{settings.REDHAT_CALLBACK_REDIRECT_URL}?code={code}')
