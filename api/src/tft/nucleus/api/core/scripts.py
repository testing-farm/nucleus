# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides a core helpers methods of Testing Farm API.
"""
import sys
from uuid import uuid4

from sqlalchemy.exc import OperationalError

from tft.nucleus.api.core import errors
from tft.nucleus.api.core.database import SessionLocal, Token, User
from tft.nucleus.api.crud.user import get_user_by_auth_id_and_auth_method

DEFAULT_USER_AUTH_ID = 'developer'
DEFAULT_USER_AUTH_METHOD = 'developer'
DEFAULT_USER_AUTH_NAME = 'developer'


def add_token() -> None:
    """
    Entrypoint for the `tft-add-token` helper script for adding a new token.
    Useful for bootstrapping the first token.
    """
    if len(sys.argv) != 5:
        print("[E] Script takes 4 arguments: name, api_key, ranch and role")
        sys.exit(1)

    name = sys.argv[1]
    api_key = sys.argv[2]
    ranch = sys.argv[3]
    role = sys.argv[4]
    token_id = str(uuid4())

    session = SessionLocal()
    if session.query(Token).filter(Token.api_key == api_key).first():
        print("[+] Token with the same api key already exists, skipping.")
        return

    try:
        user_db = get_user_by_auth_id_and_auth_method(session, DEFAULT_USER_AUTH_ID, DEFAULT_USER_AUTH_METHOD)
    except errors.NoSuchEntityError:
        user_id = str(uuid4())
        user_db = User(
            id=user_id,
            auth_id=DEFAULT_USER_AUTH_ID,
            auth_method=DEFAULT_USER_AUTH_METHOD,
            auth_name=DEFAULT_USER_AUTH_NAME,
            enabled=True,
        )
        session.add(user_db)

    token_db = Token(id=token_id, name=name, api_key=api_key, enabled=True, ranch=ranch, role=role, user_id=user_db.id)
    session.add(token_db)

    try:
        session.commit()
        print(f"[+] Adding token '{name}' to ranch '{ranch}'")
    except OperationalError as error:
        print("[E] Could not add token, is database running?")
        print(error)
        sys.exit(1)
