# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0

"""
The module provides logging functions.
"""
import logging
from typing import Any, Dict, Optional

from dynaconf import settings
from logfmter import Logfmter  # type: ignore
from sqlalchemy.orm import Session

from tft.nucleus.api.core.database import Token, User
from tft.nucleus.api.crud import user


def setup_logger(name: str, log_filepath: Optional[str] = None, log_level: int = logging.DEBUG) -> logging.Logger:
    """
    Returns logger which logs to file or console.
    """

    logger = logging.getLogger(name)
    logger.setLevel(log_level)

    formatter = Logfmter(
        keys=["level", "when"],
        mapping={"level": "levelname", "when": "asctime"},
        datefmt="%Y-%m-%dT%H:%M:%S%z",
    )

    if log_filepath:
        file_handler = logging.FileHandler(log_filepath)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
    else:
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(formatter)
        logger.addHandler(console_handler)

    return logger


logger = setup_logger('nucleus', log_filepath=settings.get('LOG_FILEPATH', None), log_level=logging.DEBUG)


def get_extra_from_token(session: Session, token: Token) -> Dict[str, Any]:
    """
    Returns dictionary with token and its user data.
    """
    extra = {
        "token_id": token.id,
        "token_name": token.name,
        "token_enabled": token.enabled,
    }

    user_db = user.get_user_by_id(session, token.user_id)
    extra.update(get_extra_from_user(session, user_db))

    return extra


def get_extra_from_user(session: Session, user: User) -> Dict[str, Any]:
    """
    Returns dictionary with user data.
    """
    return {
        "user_id": user.id,
        "user_name": user.auth_name,
    }


def log_access_attempt(
    logger: logging.Logger,
    role: Optional[str],
    obj: Any,  # There should be AccessObject here, but the type leads to circular import.
    action: Any,  # There should be AccessAction here, but the type leads to circular import.
    access: bool,
    extra: Optional[Dict[str, Any]] = None,
) -> None:
    """
    Format and log access attempt.
    """

    extra = extra or {}

    extra.update(
        {
            "what": "access_control",
            "role": role,
            "object": obj.name.lower(),
            "action": action.name.lower(),
            "access": access,
        }
    )

    if access:
        logger.info("Access attempt", extra=extra)
    else:
        logger.warning("Access attempt", extra=extra)
