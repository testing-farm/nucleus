# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0

"""
Testing Farm API - database
"""

import enum
from typing import Generator

import sqlalchemy
import sqlalchemy.ext.declarative
from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Integer,
    Interval,
    String,
    UniqueConstraint,
    create_engine,
)
from sqlalchemy.dialects.postgresql import JSONB, UUID
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy_utils import ChoiceType, EncryptedType, Timestamp
from sqlalchemy_utils.types.encrypted.encrypted_type import AesEngine

from tft.nucleus.api.config import settings

DEFAULT_TOKEN_ROLE = settings.get('DEFAULT_TOKEN_ROLE', 'user')

# This secret key is used for sqlalchemy_utils.EncryptedType to encrypt secrets in DB
ENCRYPTION_SECRET = settings.get('DATABASE_SECRET_KEY', 'nucleus')

DATABASE_NAME = settings.DATABASE_NAME
DATABASE_URL = (
    f"{settings.DATABASE_URL_PREFIX}://{settings.DATABASE_USER}@"  # noqa: E231
    f"{settings.DATABASE_HOST}:{settings.DATABASE_PORT}"  # noqa: E231
)

engine = create_engine(f'{DATABASE_URL}/{DATABASE_NAME}', connect_args={'sslmode': 'disable'})
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


# We are using declarative class definitions
Base = sqlalchemy.ext.declarative.declarative_base()


def get_db() -> Generator[sessionmaker, None, None]:
    """
    Retrieve database session.
    """

    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()


class User(Base, Timestamp):  # type: ignore
    """
    Testing Farm User

    auth_id: Unique and unchangeable identifier of the user within the particular auth_method
    auth_method: Name of the authentication method which the user used to log in
    enabled: Flag indicating if the user is enabled
    """

    __tablename__ = 'users'
    __table_args__ = (UniqueConstraint('auth_id', 'auth_method'),)

    id = Column(UUID, primary_key=True)
    auth_id = Column(String, nullable=False)
    auth_method = Column(String, nullable=False)
    auth_name = Column(String, nullable=False)
    # TODO: seems like `default=True` doesn't actually work in the DB (it's not present in the alembic migration)
    enabled = Column(Boolean, default=True, nullable=False)

    tokens = relationship('Token')


class Token(Base, Timestamp):  # type: ignore
    """
    Testing Farm Token

    name: Name of the token
    api_key: Secret key used for authentication to the API endpoints
    enabled: Flag indicating if the user is enabled
    """

    __tablename__ = 'tokens'

    id = Column(UUID, primary_key=True)
    user_id = Column(UUID, ForeignKey('users.id'), nullable=False)

    name = Column(String, nullable=False)
    api_key = Column(EncryptedType(String, ENCRYPTION_SECRET, AesEngine), nullable=False)
    # TODO: seems like `default=True` doesn't actually work in the DB (it's not present in the alembic migration)
    enabled = Column(Boolean, default=True, nullable=False)
    ranch = Column(String, default=None, nullable=True)
    role = Column(String, default=DEFAULT_TOKEN_ROLE, nullable=False)

    requests = relationship('Request')


class RequestStateType(enum.Enum):
    """
    Enum for state column in Request table.
    """

    NEW = enum.auto()
    QUEUED = enum.auto()
    RUNNING = enum.auto()
    COMPLETE = enum.auto()
    ERROR = enum.auto()
    CANCEL_REQUESTED = enum.auto()
    CANCELED = enum.auto()

    def __str__(self) -> str:
        return self.name.lower().replace('_', '-')

    @classmethod
    def from_string(cls, state: str) -> 'RequestStateType':
        """
        Initialize from a string representation of the state.
        """
        return cls[state.upper().replace('-', '_')]


class Request(Base, Timestamp):  # type: ignore
    """
    Testing Farm Test Request.
    """

    __tablename__ = 'requests'

    id = Column(UUID, primary_key=True)
    token_id = Column(UUID, ForeignKey('tokens.id'), nullable=False)

    generation = Column(Integer, nullable=False, default=0)

    state = Column(ChoiceType(RequestStateType, impl=Integer()), default=RequestStateType.NEW.value, nullable=False)
    notes = Column(JSONB)

    environments_requested = Column(JSONB, nullable=False)
    environments_provisioned = Column(JSONB)

    test = Column(JSONB, nullable=False)

    result = Column(JSONB)

    run = Column(JSONB)

    settings = Column(JSONB)

    user_webpage_url = Column(String)
    user_webpage_icon = Column(String)
    user_webpage_name = Column(String)

    notification = Column(JSONB)

    queued_time = Column(Interval)
    run_time = Column(Interval)


class RepoKeyPair(Base, Timestamp):  # type: ignore
    """
    Git repository and its corresponding public/private key pair.
    """

    __tablename__ = 'repo_key_pairs'

    id = Column(Integer, primary_key=True)
    url = Column(String, nullable=False)
    token_id = Column(UUID, ForeignKey('tokens.id'), nullable=False)
    public_key = Column(String, nullable=False)
    private_key = Column(EncryptedType(String, ENCRYPTION_SECRET, AesEngine), nullable=False)
