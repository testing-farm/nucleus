# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides a core helpers methods of Testing Farm API.
"""
import re
from typing import cast

from pydantic import AnyUrl, parse_obj_as  # pylint: disable=no-name-in-module


def hide_cred_in_url(url: AnyUrl) -> AnyUrl:
    """
    Hide possible credentials in URL
    """
    trimmed_url = parse_obj_as(AnyUrl, re.sub(r"(https?:\/\/).+:.+@(.+)", r"\1*****@\2", url))
    return cast(AnyUrl, trimmed_url)
