# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides schemes for secrets endpoints.
"""

from typing import Optional
from uuid import UUID

from pydantic import AnyUrl, Field

from tft.nucleus.api.core.schemes import NucleusBaseModel


class RepoIn(NucleusBaseModel):
    url: AnyUrl = Field(..., description=('Git url for which a RSA key pair will be generated.'))
    token_id: Optional[UUID] = Field(None, description='API Token which will be paired with key pair.')


class EncryptIn(RepoIn):
    message: str = Field(..., max_length=448, description=('Message to encrypt.'))


class DecryptIn(NucleusBaseModel):
    url: AnyUrl = Field(..., description=('Git url for which a RSA key pair will be generated.'))
    message: str = Field(..., max_length=900, description=('Secret message to decrypt.'))
