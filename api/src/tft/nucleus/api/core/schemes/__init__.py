# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides commonly used schemes across different endpoints
"""
from pydantic import BaseModel, Extra  # pylint: disable=no-name-in-module


class NucleusBaseModel(BaseModel):
    """
    The base model for all models in the Nucleus API.
    """

    class Config:
        """
        https://fastapi.tiangolo.com/tutorial/sql-databases/#use-pydantics-orm_mode
        """

        orm_mode = True
        extra = Extra.forbid
