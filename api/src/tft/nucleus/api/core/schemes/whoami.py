# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides schema of whoami endpoint
"""
from pydantic import Field  # pylint: disable=no-name-in-module

from tft.nucleus.api.core.schemes import NucleusBaseModel
from tft.nucleus.api.core.schemes.token import TokenGetUpdateOut
from tft.nucleus.api.core.schemes.user import UserGetUpdateOut


class WhoamiGetOut(NucleusBaseModel):
    token: TokenGetUpdateOut = Field(
        ..., description=('Information about the token that authenticated against this endpoint.')
    )
    user: UserGetUpdateOut = Field(
        ..., description=('Information about the user that authenticated against this endpoint.')
    )
