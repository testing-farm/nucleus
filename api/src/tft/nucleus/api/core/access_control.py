# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0

"""
The module provides functions for access control.
"""

import enum
import logging
from typing import Any, Dict, Optional, cast
from uuid import UUID

import casbin
from sqlalchemy.orm import Session

from tft.nucleus.api.config import settings
from tft.nucleus.api.core.errors import ForbiddenError
from tft.nucleus.api.core.log import (
    get_extra_from_token,
    log_access_attempt,
    setup_logger,
)
from tft.nucleus.api.crud import test_request
from tft.nucleus.api.crud import token as crud_token


class AccessObject(enum.Enum):
    """
    The class represents objects which can be accessed.
    """

    # Requests
    REQUEST = enum.auto()
    REQUEST_OWNED = enum.auto()
    REQUESTS_LIST = enum.auto()
    # Tokens
    TOKEN = enum.auto()
    TOKEN_SELF = enum.auto()
    TOKENS_LIST = enum.auto()
    # Users
    USER = enum.auto()
    # Secrets
    SECRET = enum.auto()


class AccessAction(enum.Enum):
    """
    The class represents actions which can be performed on objects.
    """

    GET = enum.auto()
    GET_SECRETS = enum.auto()
    CREATE = enum.auto()
    UPDATE = enum.auto()
    DELETE = enum.auto()
    RESTART = enum.auto()


enforcer = casbin.Enforcer(settings.ACCESS_CONTROL_MODEL_PATH, settings.ACCESS_CONTROL_POLICY_PATH)

access_logger = setup_logger('access_control', settings.get('ACCESS_CONTROL_LOG_FILEPATH'), log_level=logging.INFO)


def check_role_access(
    role: str,
    obj: AccessObject,
    action: AccessAction,
    no_error: bool = False,
    logging_extra: Optional[Dict[str, Any]] = None,
) -> bool:
    """
    Check if a user with provided role has access to the object with the action.
    """
    access = cast(bool, enforcer.enforce(role, obj.name.lower(), action.name.lower()))

    log_access_attempt(access_logger, role, obj, action, access, extra=logging_extra)

    if access:
        return True

    if no_error:
        return False

    raise ForbiddenError


def check_access_api_key(
    session: Session,
    api_key: Optional[str],
    obj: AccessObject,
    action: AccessAction,
    no_error: bool = False,
    logging_extra: Optional[Dict[str, Any]] = None,
) -> bool:
    """
    Check if the provided api_key has access to the object with the action.
    """
    logging_extra = logging_extra or {}

    if api_key is None:
        return check_role_access("anonymous", obj, action, no_error=no_error, logging_extra=logging_extra)

    token_db = crud_token.get_token_db(session, api_key=api_key)

    if token_db:
        logging_extra.update(get_extra_from_token(session, token_db))

    if not token_db.enabled:
        return check_role_access("disabled", obj, action, no_error=no_error, logging_extra=logging_extra)

    return check_role_access(token_db.role, obj, action, no_error=no_error, logging_extra=logging_extra)


def is_test_request_owned_by_token(session: Session, api_key: str, request_id: UUID) -> bool:
    """
    Check if a token with provided api_key has access to the object with the action.
    """
    token_db = crud_token.get_token_db(session, api_key=api_key)
    test_request_db = test_request.get_test_request_db(session, request_id)

    return token_db.id == test_request_db.token_id
