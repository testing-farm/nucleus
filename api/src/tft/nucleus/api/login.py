# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of login logic
"""
from datetime import datetime, timedelta, timezone
from typing import List, Union, cast

import jwt
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jwt.exceptions import InvalidTokenError
from pydantic import BaseModel  # pylint: disable=no-name-in-module
from sqlalchemy.orm import Session

from tft.nucleus.api.config import settings
from tft.nucleus.api.core import errors
from tft.nucleus.api.core.database import Token, get_db
from tft.nucleus.api.crud.token import get_token_db

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login")


class AuthorizedUser(BaseModel):
    """
    Parsed payload of JWT.
    """

    auth_id: str
    auth_method: str
    auth_name: str
    role: str
    ranch: List[str]


def create_jwt(
    auth_id: str,
    auth_method: str,
    auth_name: str,
    role: str,
    ranch: List[str],
    expires_delta: timedelta = timedelta(minutes=300),
) -> str:
    """
    Generate JWT with a payload.
    """
    to_encode = {
        'auth_id': auth_id,
        'auth_method': auth_method,
        'auth_name': auth_name,
        'role': role,
        'ranch': ranch,
        'exp': datetime.now(timezone.utc) + expires_delta,
    }
    encoded_jwt = jwt.encode(to_encode, cast(str, settings.JWT_SECRET), algorithm="HS256")
    return encoded_jwt


def get_authorized_user(token: str = Depends(oauth2_scheme)) -> AuthorizedUser:
    """
    Parse user from provided JWT.
    """
    try:
        payload = jwt.decode(token, cast(str, settings.JWT_SECRET), algorithms=["HS256"])
    except InvalidTokenError:
        raise HTTPException(  # pylint: disable=raise-missing-from
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return AuthorizedUser(**payload)


def parse_bearer_token(
    token: str = Depends(oauth2_scheme), session: Session = Depends(get_db)
) -> Union[AuthorizedUser, Token]:
    """
    Parse either user from JWT or API token.
    """
    try:
        return AuthorizedUser(**jwt.decode(token, cast(str, settings.JWT_SECRET), algorithms=["HS256"]))
    except InvalidTokenError:
        pass

    try:
        return get_token_db(session, api_key=token)
    except errors.NotAuthorizedError:
        raise HTTPException(  # pylint: disable=raise-missing-from
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
